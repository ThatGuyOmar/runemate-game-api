package com.runemate.game.cache.item;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.exception.*;

@Log4j2
public abstract class FileLoader<V> extends DataLoader {

    public FileLoader(int archive) {
        super(archive);
    }

    public V form(int group, int file, byte[] data) throws IOException {
        return form(group, file, data, Collections.emptyMap());
    }

    public abstract V form(int group, int file, byte[] data, Map<String, Object> arguments) throws IOException;

    public V load(ConfigType type, int file) throws IOException {
        return load(JS5CacheController.getLargestJS5CacheController(), type, file);
    }

    public V load(JS5CacheController cache, ConfigType type, int file) throws IOException {
        return load(cache, type.getId(), file);
    }

    public V load(JS5CacheController cache, int entry, int file) throws IOException {
        return load(cache, entry, file, Collections.emptyMap());
    }

    public V load(JS5CacheController cache, int group, int file, Map<String, Object> arguments) throws IOException {
        byte[] data = retrieve(cache, group, file);
        return data == null ? null : form(group, file, data, arguments);
    }

    public List<V> load(JS5CacheController cache, int group) {
        return load(cache, group, Collections.emptyMap());
    }

    public List<V> load(JS5CacheController cache, int group, Map<String, Object> arguments) {
        int[] files = files(cache, group);
        List<V> loaded = new ArrayList<>(files.length);
        Map<Integer, IOException> exceptions = null;
        for (int file = 0; file < files.length; ++file) {
            byte[] data = retrieve(cache, group, files[file]);
            if (data != null) {
                try {
                    loaded.add(form(group, files[file], data, arguments));
                } catch (IOException ioe) {
                    if (exceptions == null) {
                        exceptions = new HashMap<>();
                    }
                    exceptions.put(file, ioe);
                }
            }
        }
        if (exceptions != null) {
            log.warn(exceptions.size() + " IOExceptions occurred while loading files "
                + Arrays.toString(exceptions.keySet().toArray()) + " from archive " + archive + " of entry " + group + " of the Js5Cache.");
            exceptions.forEach((file, e) -> log.warn("File:" + file + ' ' + ExceptionUtils.getStackTrace(e)));
        }
        return loaded;
    }

    public List<V> load(JS5CacheController cache, String group) {
        return load(
            cache,
            DJB2Hasher.getGroupId(cache.getIndexFile(archive).getReferenceTable(), group),
            Collections.emptyMap()
        );
    }

    public V load(JS5CacheController cache, String group, int file) throws IOException {
        return load(cache, group, file, Collections.emptyMap());
    }

    public V load(JS5CacheController cache, String group, int file, Map<String, Object> arguments)
        throws IOException {
        byte[] data = retrieve(cache, group, file);
        return data == null ? null :
            form(DJB2Hasher.getGroupId(cache.getIndexFile(archive).getReferenceTable(), group), file, data, arguments);
    }

    public int[] getGroups(JS5CacheController cache) {
        return groups(cache);
    }

    public int[] getFiles(JS5CacheController cache, int group) {
        return files(cache, group);
    }

    public int[] getFiles(ConfigType type) {
        return getFiles(JS5CacheController.getLargestJS5CacheController(), type);
    }

    public int[] getFiles(JS5CacheController controller, ConfigType type) {
        return getFiles(controller, type.getId());
    }
}
