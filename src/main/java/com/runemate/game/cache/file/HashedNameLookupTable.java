package com.runemate.game.cache.file;

public class HashedNameLookupTable {
    private final int[] table;

    public HashedNameLookupTable(int[] hashedNames) {
        int size;
        for (size = 1; size <= (hashedNames.length >> 1) + hashedNames.length; size <<= 1) {
        }
        table = new int[size + size];
        for (int i = 0; i < size + size; i++) {
            table[i] = -1;
        }
        for (int i = 0; i < hashedNames.length; i++) {
            int pos;
            for (pos = hashedNames[i] & size - 1; table[pos + pos + 1] != -1;
                 pos = pos + 1 & size - 1) {
            }
            table[pos + pos] = hashedNames[i];
            table[pos + pos + 1] = i;
        }
    }

    public int getIndex(int hashedName) {
        int size = (table.length >> 1) - 1;
        int pos = hashedName & size;
        while (true) {
            int idx = table[pos + 1 + pos];
            if (idx == -1) {
                return -1;
            }
            if (hashedName == table[pos + pos]) {
                return idx;
            }
            pos = pos + 1 & size;
        }
    }
}
