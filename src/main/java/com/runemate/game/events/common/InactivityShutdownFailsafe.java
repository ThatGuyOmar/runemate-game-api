package com.runemate.game.events.common;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.events.*;

public abstract class InactivityShutdownFailsafe extends GameEventHandler {

    public InactivityShutdownFailsafe() {
        super(PRIORITY_LOW);
    }


    @Override
    public GameEvents.GameEvent getAPIEventInstance() {
        return null;
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public void run() {

    }
}
