package com.runemate.game.events.common;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.logger.*;
import com.runemate.game.events.*;
import java.awt.event.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import lombok.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public abstract class InterfaceCloser extends GameEventHandler {
    protected UnwantedInterface unwanted;

    public InterfaceCloser() {
        super(PRIORITY_NORMAL);
    }

    @Override
    public boolean isValid() {
        return !Bank.isOpen() && (unwanted = getPriorityInterface()) != null;
    }

    @Override
    public void run() {
        final SpriteItem selected = Inventory.getSelectedItem();
        if (selected != null) {
            log.debug("Deselecting " + selected);
            selected.click();
            return;
        }
        if (Environment.isOSRS()) {
            final Magic magic = Magic.getSelected();
            if (magic != null) {
                log.debug("Deselecting " + magic);
                magic.deactivate();
            }
        }
        final ChatDialog.Continue cdContinue = ChatDialog.getContinue();
        if (cdContinue != null) {
            log.debug("Progressing the interfering dialog by selecting continue.");
            cdContinue.select();
        } else if (!ChatDialog.getOptions().isEmpty()) {
            log.debug("Closing the open ChatDialog option.");
            if (Keyboard.typeKey(KeyEvent.VK_ESCAPE)) {
                Execution.delayUntil(() -> ChatDialog.getOptions().isEmpty(), 1500, 3000);
            }
        } else {
            InterfaceComponent component = unwanted.getComponent(null);
            if (component != null) {
                if (PlayerSense.getAsBoolean(
                    PlayerSense.Key.USE_UNWANTED_INTERFACE_CLOSER_HOTKEYS)) {
                    try {
                        if (unwanted.hasHotkey.call()) {
                            log.debug(
                                "Pressing escape on the unwanted component \""
                                    + unwanted.comment + '"');
                            if (Keyboard.typeKey(KeyEvent.VK_ESCAPE)) {
                                Execution.delayUntil(() -> !component.isVisible(), 1500, 3000);
                                return;
                            }
                        }
                    } catch (Exception e) {
                        log.warn("Failed to determine if the unwanted component '{}' could be closed with a hotkey", component, e);
                    }
                }
                log.debug("Clicking the unwanted component \"" + unwanted.comment + '"');
                List<String> actions = component.getActions();
                if (actions.isEmpty() ? component.click() : component.interact(actions.get(0))) {
                    Execution.delayUntil(() -> !component.isVisible(), 1500, 3000);
                }
            }
        }
    }

    @Override
    public String getActivationText() {
        return super.getActivationText() + " - " + unwanted.comment;
    }

    @NonNull
    @Override
    public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
        return Arrays.asList(getUnwantedInterfaces());
    }

    protected UnwantedInterface getPriorityInterface() {
        final AbstractBot bot = Environment.getBot();
        Set<Integer> loadedContainers =
            InterfaceContainers.getLoaded().stream().map(InterfaceContainer::getIndex)
                .collect(Collectors.toSet());
        for (final UnwantedInterface widget : getUnwantedInterfaces()) {
            if (!widget.isEnabled(bot)) {
                continue;
            }
            InterfaceComponent c = widget.getComponent(loadedContainers);
            if (c != null) {
                if (!BotControl.isRunningFromSource() && (
                    widget.query == null
                        || widget.query.types == null
                )) {
                    log.info(
                        "[Owner Debug] Interface Closer \"" + widget.comment + "\": " + c
                            + " should declare a type of " + c.getType()
                            + " in it's query.\n\r[Owner Debug] Additional Data: Text: "
                            + c.getText() + ", Sprite: " + c.getSpriteId() + ", Width: "
                            + c.getWidth() + ", Height: " + c.getHeight() + ", Name: " + c.getName()
                            + ", Actions: " + Arrays.toString(c.getActions().toArray()));
                }
                if (!BotControl.isRunningFromSource() && (
                    widget.query == null
                        || widget.query.scanGrandchildren
                ) && c.getParentComponent() == null) {
                    log.info(
                        "[Owner Debug] Interface Closer \"" + widget.comment + "\": " + c
                            + " should include grandchildren(false) in it's query.\n\r[Owner Debug] Additional Data: Text: "
                            + c.getText() + ", Sprite: " + c.getSpriteId() + ", Width: "
                            + c.getWidth() + ", Height: " + c.getHeight() + ", Name: " + c.getName()
                            + ", Actions: " + Arrays.toString(c.getActions().toArray()));
                }
                return widget;
            }
        }
        return null;
    }

    public abstract UnwantedInterface[] getUnwantedInterfaces();

    public static class UnwantedInterface implements GameEvents.GameEvent {
        //private static Callable<List<? extends InterfaceComponent>> DEFAULT_PROVIDER = Interfaces.newQuery().getDefaultProvider();
        private final String comment;
        private final Callable<Boolean> hasHotkey;
        private String name;
        private final InterfaceComponentQueryBuilder query;
        private final List<InterfaceComponentQueryBuilder> validationQueries;

        public UnwantedInterface(String comment, InterfaceComponentQueryBuilder query) {
            this(comment, query, Collections.emptyList());
        }

        public UnwantedInterface(
            String comment, InterfaceComponentQueryBuilder query,
            List<InterfaceComponentQueryBuilder> validationQueries
        ) {
            this(comment, query, validationQueries, false);
        }

        public UnwantedInterface(
            String comment, InterfaceComponentQueryBuilder query,
            boolean hasHotkey
        ) {
            this(comment, query, Collections.emptyList(), hasHotkey);
        }

        public UnwantedInterface(
            String comment, InterfaceComponentQueryBuilder query,
            List<InterfaceComponentQueryBuilder> validationQueries,
            boolean hasHotkey
        ) {
            this(comment, query, validationQueries, () -> hasHotkey);
        }

        public UnwantedInterface(
            String comment, InterfaceComponentQueryBuilder query,
            Callable<Boolean> hasHotkey
        ) {
            this(comment, query, Collections.emptyList(), hasHotkey);
        }

        public UnwantedInterface(
            String comment, InterfaceComponentQueryBuilder query,
            List<InterfaceComponentQueryBuilder> validationQueries,
            Callable<Boolean> hasHotkey
        ) {
            this.comment = comment;
            this.query = query;
            this.validationQueries = validationQueries;
            this.hasHotkey = hasHotkey;
        }

        public InterfaceComponent getComponent(Collection<Integer> loadedContainers) {
            InterfaceComponentQueryResults components;
            if (query != null) {
                if (loadedContainers != null && query.containers != null && Collections.disjoint(
                    query.containers, loadedContainers)) {
                    return null;
                }
                components = query.results();
                if (!components.isEmpty()) {
                    for (InterfaceComponentQueryBuilder validationQuery : validationQueries) {
                        if (loadedContainers != null && validationQuery.containers != null
                            && Collections.disjoint(validationQuery.containers, loadedContainers)) {
                            return null;
                        }
                        InterfaceComponent validator = validationQuery.results().first();
                        if (validator == null || !validator.isVisible()) {
                            return null;
                        }
                    }
                }
            } else {
                throw new UnsupportedOperationException("Unable to locate a query for " + comment);
            }
            return components.stream().filter(InterfaceComponent::isVisible).findFirst()
                .orElse(null);
        }

        public String getComment() {
            return comment;
        }

        @Override
        public String toString() {
            return "UnwantedInterface{name='" + getName() + "'}";
        }

        @Override
        public String getName() {
            if (name == null) {
                name = "interface." + getComment().toLowerCase().replaceAll(" ", "_")
                    .replaceAll("[^a-zA-Z_]", "");
            }
            return name;
        }

        @NonNull
        @Override
        public List<GameEvents.GameEvent> getChildren(AbstractBot bot) {
            return Collections.emptyList();
        }
    }
}
