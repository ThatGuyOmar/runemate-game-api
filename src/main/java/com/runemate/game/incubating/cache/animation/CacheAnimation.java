package com.runemate.game.incubating.cache.animation;

import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;

public class CacheAnimation extends IncrementallyDecodedItem implements Animation {
    private final boolean rs3;
    private final int id;

    public CacheAnimation(boolean rs3, int id) {
        this.rs3 = rs3;
        this.id = id;
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
    }
}
