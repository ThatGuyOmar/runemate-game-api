package com.runemate.game.internal;

import java.lang.annotation.*;

/**
 * Elements marked @InternalAPI may not be used on the bot store, and bots attempting to use these elements
 * will be automatically rejected
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
public @interface InternalAPI {
}
