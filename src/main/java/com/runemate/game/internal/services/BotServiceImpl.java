package com.runemate.game.internal.services;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.util.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.task.*;
import com.runemate.game.cache.*;
import com.runemate.game.internal.events.*;
import com.runemate.game.internal.input.*;
import com.runemate.game.internal.rmi.*;
import java.util.*;
import java.util.concurrent.*;

public class BotServiceImpl implements BotService {

    private final AbstractBot bot;

    private final Task gameEventController;
    private final EventDispatcher eventDispatcher;
    private final Runnable framerateRegulator;
    private final BotInputService inputManager;
    private final CacheController cacheController;
    private final BridgeService bridgeService;
    private final ReducedCPUListener reducedCpuListener;

    public BotServiceImpl(AbstractBot bot) {
        this.bot = bot;

        this.gameEventController = new GameEventControllerImpl();
        this.eventDispatcher = new EventDispatcherImpl(bot);
        this.framerateRegulator = new OSRSFramerateRegulator();
        this.inputManager = new BotInputServiceImpl();
        this.cacheController = JS5CacheController.newJs5CacheController();
        this.bridgeService = new BridgeServiceImpl(bot);
        this.reducedCpuListener = new ReducedCPUListener();
    }

    @Override
    public Task getEventController() {
        return gameEventController;
    }

    @Override
    public EventDispatcher getEventDispatcher() {
        return eventDispatcher;
    }

    @Override
    public Runnable getFramerateRegulator() {
        return framerateRegulator;
    }

    @Override
    public BotInputService getInputManager() {
        return inputManager;
    }

    @Override
    public CacheController getCacheController() {
        return cacheController;
    }

    @Override
    public BridgeService getBridgeService() {
        return bridgeService;
    }

    @Override
    public EventListener getReducedCpuListener() {
        return reducedCpuListener;
    }

    @Override
    public void close() {
        final ForkJoinPool fjp = Parallelize.BOT_FORK_JOIN_POOLS.remove(bot);
        if (fjp != null) {
            fjp.shutdownNow();
        }
        final ExecutorService cameraExecutor = Camera.executorMap.remove(bot);
        if (cameraExecutor != null) {
            cameraExecutor.shutdownNow();
        }
        for (final EventListener listener : eventDispatcher.getListeners()) {
            eventDispatcher.removeListener(listener);
        }
        eventDispatcher.shutdown();
    }
}
