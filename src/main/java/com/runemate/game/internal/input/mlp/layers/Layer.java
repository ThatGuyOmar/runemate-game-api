package com.runemate.game.internal.input.mlp.layers;

import com.runemate.game.internal.input.mlp.activation.ActivationFunction;
import org.jblas.DoubleMatrix;

/**
 * Base class for creating new layers
 * Only used to create Dense right now therefore limited functionality
 */
public abstract class Layer {

    private final DoubleMatrix weights;
    private final DoubleMatrix biases;
    private Layer outputLayer, inputLayer;
    private ActivationFunction activationFunction;

    public Layer(DoubleMatrix weights, DoubleMatrix biases) {
        this.weights = weights;
        this.biases = biases;
    }

    public void setInputLayer(Layer inputLayer) {
        this.inputLayer = inputLayer;
    }

    public ActivationFunction getActivationFunction() {
        return activationFunction;
    }

    protected void setActivationFunction(ActivationFunction activationFunction) {
        this.activationFunction = activationFunction;
    }

    public DoubleMatrix getWeights() {
        return weights;
    }

    public DoubleMatrix getBiases() {
        return biases;
    }

    public Layer getOutputLayer() {
        return outputLayer;
    }

    public void setOutputLayer(Layer outputLayer) {
        this.outputLayer = outputLayer;
    }

    @Override
    public String toString() {
        return "Dense[" + this.activationFunction + "]";
    }

}
