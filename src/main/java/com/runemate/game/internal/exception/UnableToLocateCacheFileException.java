package com.runemate.game.internal.exception;

public class UnableToLocateCacheFileException extends RuntimeException {
    public UnableToLocateCacheFileException(String explanation) {
        super(explanation);
    }

    public UnableToLocateCacheFileException(String explanation, Exception cause) {
        super(explanation, cause);
    }
}
