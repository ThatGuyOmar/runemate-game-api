package com.runemate.game.internal.events;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.dispatchers.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.rmi.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

/**
 * Processes and dispatches events to the registered listeners.
 */
@Log4j2
public class EventDispatcherImpl extends EventDispatcher {

    private final HashSet<Class<? extends EventListener>> using = new HashSet<>();
    private final AbstractBot bot;

    private final ScheduledExecutorService poller = Executors.newSingleThreadScheduledExecutor();
    private ExecutorService executor = new DelegatingExecutorService();

    public EventDispatcherImpl(AbstractBot bot) {
        super(bot);
        this.bot = bot;
    }

    private <T> void forEach(Class<T> type, Consumer<T> consumer) {
        try {
            for (final var listener : listeners) {
                if (type.isAssignableFrom(listener.getClass())) {
                    //noinspection unchecked
                    executor.submit(() -> consumer.accept((T) listener));
                }
            }

        } catch (RejectedExecutionException e) {
            //Happens occasionally if an event is dispatched when bot is shutting down
            log.warn("Failed to dispatch event as executor is shutting down");
        } catch (final Throwable t) {
            ClientAlarms.handle(bot, t);
            log.warn("Failed to dispatch event", t);
            if (!bot.isStopped()) {
                bot.stop(t.getMessage());
            }
        }
    }

    /**
     * Forwards an event to the registered listeners of the correct type.
     */

    @Override
    public void dispatchLater(Event event) {
        if (event == null) {
            return;
        }

        forEach(GlobalListener.class, l -> l.onEvent(event));
        if (event instanceof EntityEvent) {
            switch (((EntityEvent) event).getEntityType()) {
                case PLAYER: {
                    if (event instanceof DeathEvent) {
                        forEach(PlayerListener.class, l -> l.onPlayerDeath((DeathEvent) event));
                    } else if (event instanceof AnimationEvent) {
                        forEach(PlayerListener.class, l -> l.onPlayerAnimationChanged((AnimationEvent) event));
                    } else if (event instanceof TargetEvent) {
                        forEach(PlayerListener.class, l -> l.onPlayerTargetChanged((TargetEvent) event));
                    } else if (event instanceof PlayerMovementEvent) {
                        forEach(PlayerListener.class, l -> l.onPlayerMoved((PlayerMovementEvent) event));
                    } else if (event instanceof HitsplatEvent) {
                        forEach(PlayerListener.class, l -> l.onPlayerHitsplat((HitsplatEvent) event));
                    }
                    break;
                }
                case NPC: {
                    if (event instanceof DeathEvent) {
                        forEach(NpcListener.class, l -> l.onNpcDeath((DeathEvent) event));
                    } else if (event instanceof AnimationEvent) {
                        forEach(NpcListener.class, l -> l.onNpcAnimationChanged((AnimationEvent) event));
                    } else if (event instanceof TargetEvent) {
                        forEach(NpcListener.class, l -> l.onNpcTargetChanged((TargetEvent) event));
                    } else if (event instanceof NpcSpawnedEvent) {
                        forEach(NpcListener.class, l -> l.onNpcSpawned((NpcSpawnedEvent) event));
                    } else if (event instanceof HitsplatEvent) {
                        forEach(NpcListener.class, l -> l.onNpcHitsplat((HitsplatEvent) event));
                    }
                    break;
                }
                case PROJECTILE: {
                    if (event instanceof ProjectileLaunchEvent) {
                        forEach(ProjectileListener.class, l -> l.onProjectileLaunched((ProjectileLaunchEvent) event));
                    } else if (event instanceof ProjectileMovedEvent) {
                        forEach(ProjectileListener.class, l -> l.onProjectileMoved((ProjectileMovedEvent) event));
                    }
                    break;
                }
                case GROUNDITEM: {
                    if (event instanceof GroundItemSpawnedEvent) {
                        forEach(GroundItemListener.class, l -> l.onGroundItemSpawned((GroundItemSpawnedEvent) event));
                    }
                    break;
                }
                case GAMEOBJECT: {
                    if (event instanceof GameObjectSpawnEvent) {
                        forEach(GameObjectListener.class, l -> l.onGameObjectSpawned((GameObjectSpawnEvent) event));
                    }
                    break;
                }
                case SPOTANIMATION: {
                    if (event instanceof SpotAnimationSpawnEvent) {
                        forEach(SpotAnimationListener.class, l -> l.onSpotAnimationSpawned((SpotAnimationSpawnEvent) event));
                    }
                    break;
                }
                default:
                    //Simply serves as a reminder in case we ever add other EntityType listeners
                    throw new IllegalStateException("Unsupported EntityEvent: " + event.getClass().getSimpleName());
            }
        }

        if (event instanceof EngineEvent) {
            EngineEvent.Type type = ((EngineEvent) event).getType();
            switch (type) {
                case CLIENT_CYCLE:
                    forEach(EngineListener.class, EngineListener::onCycleStart);
                    break;
                case SERVER_TICK:
                    forEach(EngineListener.class, EngineListener::onTickStart);
                    break;
                default:
                    log.warn("Unsupported engine event type of {}", type);
            }
        } else if (event instanceof CS2ScriptEvent) {
            final CS2ScriptEvent sEvent = ((CS2ScriptEvent) event);
            forEach(CS2ScriptEventListener.class, l -> {
                if (CS2ScriptEvent.Type.STARTED.equals(sEvent.getType())) {
                    l.onScriptExecutionStarted(sEvent);
                }
            });
        } else if (event instanceof EngineStateEvent) {
            forEach(EngineListener.class, l -> l.onEngineStateChanged((EngineStateEvent) event));
        } else if (event instanceof MenuInteractionEvent) {
            forEach(MenuInteractionListener.class, l -> l.onInteraction((MenuInteractionEvent) event));
        } else if (event instanceof ItemEvent) {
            final var ie = (ItemEvent) event;
            final var item = ie.getItem();
            if (item.getOrigin() == SpriteItem.Origin.INVENTORY) {
                forEach(InventoryListener.class, l -> {
                    if (ie.getType() == ItemEvent.Type.ADDITION) {
                        l.onItemAdded(ie);
                    } else {
                        l.onItemRemoved(ie);
                    }
                });
            } else if (SpriteItem.Origin.EQUIPMENT.equals(item.getOrigin())) {
                forEach(EquipmentListener.class, l -> {
                    if (ie.getType() == ItemEvent.Type.ADDITION) {
                        l.onItemEquipped(ie);
                    } else {
                        l.onItemUnequipped(ie);
                    }
                });
            }
        } else if (event instanceof SkillEvent) {
            forEach(SkillListener.class, l -> {
                final var s = (SkillEvent) event;
                if (s.getType() == SkillEvent.Type.LEVEL_GAINED) {
                    l.onLevelUp(s);
                } else if (s.getType() == SkillEvent.Type.EXPERIENCE_GAINED) {
                    l.onExperienceGained(s);
                } else if (s.getType() == SkillEvent.Type.CURRENT_LEVEL_CHANGED) {
                    l.onCurrentLevelChanged(s);
                }
            });
        } else if (event instanceof MessageEvent) {
            forEach(ChatboxListener.class, l -> l.onMessageReceived((MessageEvent) event));
        } else if (event instanceof VarpEvent) {
            forEach(VarpListener.class, l -> l.onValueChanged((VarpEvent) event));
        } else if (event instanceof VarcEvent) {
            final var v = (VarcEvent) event;
            forEach(VarcListener.class, l -> {
                if (v.isString()) {
                    l.onStringChanged(v);
                } else {
                    l.onIntChanged(v);
                }
            });
        } else if (event instanceof VarbitEvent) {
            forEach(VarbitListener.class, l -> l.onValueChanged((VarbitEvent) event));
        } else if (event instanceof GrandExchangeEvent) {
            forEach(GrandExchangeListener.class, l -> l.onSlotUpdated((GrandExchangeEvent) event));
        } else if (event instanceof RegionLoadedEvent) {
            forEach(RegionListener.class, l -> l.onRegionLoaded((RegionLoadedEvent) event));
        } else if (event instanceof SettingChangedEvent) {
            forEach(SettingsListener.class, l -> l.onSettingChanged((SettingChangedEvent) event));
        } else if (event instanceof SettingsConfirmedEvent) {
            forEach(SettingsListener.class, SettingsListener::onSettingsConfirmed);

            //TODO Handled by EntityEvent branch - remove this at a later date
        } else if (event instanceof ProjectileLaunchEvent) {
            forEach(ProjectileLaunchListener.class, l -> l.onProjectileLaunched((ProjectileLaunchEvent) event));
        } else if (event instanceof HitsplatEvent) {
            forEach(HitsplatListener.class, l -> l.onHitsplatAdded((HitsplatEvent) event));
        } else if (event instanceof TargetEvent) {
            forEach(TargetListener.class, l -> l.onTargetChanged((TargetEvent) event));
        } else if (event instanceof PlayerMovementEvent) {
            forEach(PlayerMovementListener.class, l -> l.onPlayerMoved((PlayerMovementEvent) event));
        } else if (event instanceof DeathEvent) {
            forEach(DeathListener.class, l -> l.onDeath((DeathEvent) event));
        } else if (event instanceof AnimationEvent) {
            forEach(AnimationListener.class, l -> l.onAnimationChanged((AnimationEvent) event));
        }
    }

    /**
     * Registers a listener to dispatch in-game events to
     *
     * @param listener a listener (PaintListener, MouseListener, KeyListener, etc)
     */
    public void addListener(final EventListener listener) {
        listeners.add(listener);
        //The GrandExchangeListener is the only listeners that still rely on polling.
        if (listener instanceof GrandExchangeListener && !using.contains(GrandExchangeListener.class)) {
            GrandExchangeDispatcher dispatcher = new GrandExchangeDispatcher();
            poller.scheduleWithFixedDelay(dispatcher, 100, dispatcher.getIterationRateInMilliseconds(), TimeUnit.MILLISECONDS);
            using.add(GrandExchangeListener.class);
        }

        if (listener instanceof GlobalListener) {
            registerCallback(0xFFFFFFFF);
        } else {
            if (listener instanceof ChatboxListener) {
                registerCallback(Callback.CHATBOX);
            }
            if (listener instanceof CS2ScriptEventListener) {
                registerCallback(Callback.CS2);
            }
            if (listener instanceof EngineListener) {
                registerCallback(Callback.ENGINE);
            }
            if (listener instanceof GroundItemListener) {
                registerCallback(Callback.GROUND_ITEM);
            }
            if (listener instanceof GameObjectListener) {
                registerCallback(Callback.OBJECT);
            }
            if (listener instanceof InventoryListener) {
                registerCallback(Callback.INVENTORY);
            }
            if (listener instanceof MenuInteractionListener) {
                registerCallback(Callback.MENU_ACTION);
            }
            if (listener instanceof NpcListener) {
                registerCallback(Callback.NPC);
            }
            if (listener instanceof PlayerListener) {
                registerCallback(Callback.PLAYER);
            }
            if (listener instanceof ProjectileListener) {
                registerCallback(Callback.PROJECTILE);
            }
            if (listener instanceof RegionListener) {
                registerCallback(Callback.REGION);
            }
            if (listener instanceof VarpListener) {
                registerCallback(Callback.VARP);
            }
            if (listener instanceof VarbitListener) {
                registerCallback(Callback.VARBIT);
            }
            if (listener instanceof VarcListener) {
                registerCallback(Callback.VARC);
            }
            if (listener instanceof SpotAnimationListener) {
                registerCallback(Callback.SPOT_ANIMATION);
            }
        }
    }

    @Override
    public void createAnimationEvent(@NonNull String type, long characterUid, int animationId) {
        final var entityType = EntityEvent.resolveType(type);
        dispatchLater(new AnimationEvent(entityType, createActor(entityType, characterUid), animationId));
    }

    @Override
    public void createChatboxEvent(int type, @NonNull String sender, @NonNull String message) {
        dispatchLater(new MessageEvent(Chatbox.Message.Type.resolve(type, message, sender), sender, message));
    }

    @Override
    public void createConnectionStateEvent(int old, int current) {
        dispatchLater(new EngineStateEvent(old, current));
    }

    @Override
    @SneakyThrows({ ExecutionException.class, InterruptedException.class })
    public void createDeathEvent(@NonNull String type, long actorUid, int gameCycle) {
        final var entityType = EntityEvent.resolveType(type);
        final Actor actor = createActor(entityType, actorUid);
        if (actor == null) //Shouldn't happen but just to be safe
        {
            return;
        }
        final Coordinate deathPos = bot.getPlatform().invokeAndWait(() -> actor.getPosition());
        final Area.Rectangular deathArea = bot.getPlatform().invokeAndWait(() -> actor.getArea());
        dispatchLater(new DeathEvent(entityType, actor, deathPos, deathArea, gameCycle));
    }

    @Override
    public void createEngineCycleEvent() {
        dispatchLater(new EngineEvent(EngineEvent.Type.CLIENT_CYCLE));
    }

    @Override
    //TODO cleanup parameter types (long endCycle)
    public void createHitsplatEvent(
        @NonNull String type, long actorUid, int typeId, int damage, int specialTypeId, int startCyle, int endCycle
    ) {
        final var hitsplat = new Hitsplat(typeId, damage, specialTypeId, startCyle, endCycle);
        final var entityType = EntityEvent.resolveType(type);
        final var source = createActor(entityType, actorUid);
        dispatchLater(new HitsplatEvent(entityType, source, hitsplat));
    }

    @Override
    public void createMenuInteractionEvent(
        int arg0, int arg1, int opcode, int identifier, @NonNull String action, @NonNull String target, int mx, int my
    ) {
        dispatchLater(new MenuInteractionEvent(arg0, arg1, opcode, identifier, JagTags.remove(action), JagTags.remove(target), mx, my));
    }

    @Override
    public void createPlayerMovedEvent(long playerUid) {
        dispatchLater(new PlayerMovementEvent(new OSRSPlayer(playerUid)));
    }

    @Override
    public void createProjectileLaunchEvent(long uid) {
        dispatchLater(new ProjectileLaunchEvent(new OSRSProjectile(uid)));
    }

    @Override
    public void createRegionLoadedEvent(int x, int y) {
        final Coordinate previous = (Coordinate) bot.getCache().get("RegionBase");
        final Coordinate base = new Coordinate(x, y, 0);
        bot.getCache().put("RegionBase", base);
        dispatchLater(new RegionLoadedEvent(previous, base));

        //Force the pathfinder to download the current subregion by building a path to our current position
        bot.getPlatform().invokeLater(() -> {
            if (!Region.isInstanced() && !House.isInside()) {
                WebPath.buildTo(Players.getLocal());
            }
        });
    }

    @Override
    public void createScriptStartEvent(int scriptId, @Nullable Object[] scriptArgs) {
        dispatchLater(new CS2ScriptEvent(scriptId, CS2ScriptEvent.Type.STARTED, scriptArgs));
    }

    @Override
    public void createServerTickEvent() {
        dispatchLater(new EngineEvent(EngineEvent.Type.SERVER_TICK));
    }

    @Override
    @SneakyThrows({ ExecutionException.class, InterruptedException.class })
    public void createTargetEvent(final @NonNull String type, long actorUid, int targetIndex) {
        final var entityType = EntityEvent.resolveType(type);
        dispatchLater(new TargetEvent(
            entityType,
            createActor(entityType, actorUid),
            bot.getPlatform().invokeAndWait(() -> getCharacterTarget(targetIndex))
        ));
    }

    @Override
    public void createVarbitEvent(final int index, final int old, final int value) {
        //handled by 'createVarpEvent'
    }

    @Override
    public void createVarcIntEvent(final int index, final int old, final int value) {
        if (old != value) {
            dispatchLater(new VarcEvent(false, index, old, value));
        }
    }

    @Override
    public void createVarcStringEvent(final int index, final String old, final String value) {
        if (!Objects.equals(old, value)) {
            dispatchLater(new VarcEvent(true, index, old, value));
        }
    }

    @Override
    @SneakyThrows({ ExecutionException.class, InterruptedException.class })
    public void createVarpEvent(final int index, final int old, final int value) {
        var varp = bot.getPlatform().invokeAndWait(() -> Varps.getAt(index));
        dispatchLater(new VarpEvent(varp, old, value));

        var varbits = bot.getPlatform().invokeAndWait(() -> Varbits.forVarp(index));
        varbits.forEach(varbit -> {
            final var oldValue = varbit.getValue(old);
            final var newValue = varbit.getValue(value);
            if (oldValue != newValue) {
                dispatchLater(new VarbitEvent(varbit, oldValue, newValue));
            }
        });
    }

    @Override
    public void createNpcSpawnEvent(final long uid) {
        final Npc npc = new OSRSNpc(uid);
        dispatchLater(new NpcSpawnedEvent(npc));
    }

    private Actor createActor(EntityEvent.EntityType type, long uid) {
        if (type == EntityEvent.EntityType.PLAYER) {
            return new OSRSPlayer(uid);
        } else if (type == EntityEvent.EntityType.NPC) {
            return new OSRSNpc(uid);
        }
        return null;
    }

    @Nullable
    private static Actor getCharacterTarget(int targetIndex) {
        if (targetIndex == -1) {
            return null;
        }
        if (targetIndex < 0x10000) {
            return OSRSNpcs.getByIndex(targetIndex);
        } else {
            return OSRSPlayers.getAt(targetIndex - 0x10000);
        }
    }

    @Override
    public void createEventObjectSpawnEvent(int x, int y, int plane, long uid, String type, int id) {
        final var pos = new Coordinate(x, y, plane);
        dispatchLater(new GameObjectSpawnEvent(new OSRSGameObject(uid, type, id, pos), pos));
    }

    @Override
    @SneakyThrows({ ExecutionException.class, InterruptedException.class })
    public void createProjectileMovedEvent(final long uid, final int x, final int y) {
        final var plane = bot.getPlatform().invokeAndWait(Region::getCurrentPlane);
        dispatchLater(new ProjectileMovedEvent(new OSRSProjectile(uid), new Coordinate(x, y, plane)));
    }

    @Override
    public void createSpotAnimationEvent(int x, int y, final int plane, final long uid) {
        dispatchLater(new SpotAnimationSpawnEvent(new OSRSSpotAnimation(uid), new Coordinate(x, y, plane)));
    }

    @Override
    public void createGroundItemEvent(int x, int y, int plane, long uid) {
        final var c = new Coordinate(x, y, plane);
        final var item = new OSRSGroundItem(uid, c);
        dispatchLater(new GroundItemSpawnedEvent(item, c));
    }

    @Override
    @SneakyThrows({ ExecutionException.class, InterruptedException.class })
    public synchronized void forceSequentialDispatching(boolean enable) {
        var previous = executor;
        if (enable) {
            if (executor instanceof DelegatingExecutorService) {
                executor = bot.getPlatform().invokeAndWait(() -> Executors.newSingleThreadExecutor());
            }
        } else if (!(executor instanceof DelegatingExecutorService)) {
            try {
                executor = new DelegatingExecutorService();
            } finally {
                previous.shutdown();
            }
        }
    }

    @Override
    public void shutdown() {
        poller.shutdown();
        if (!(executor instanceof DelegatingExecutorService)) {
            executor.shutdown();
        }
    }

    private class DelegatingExecutorService extends AbstractExecutorService {

        @Override
        public void execute(@NonNull final Runnable command) {
            bot.getPlatform().invokeLater(command);
        }

        @Override
        public void shutdown() {
            bot.getPlatform().shutdown();
        }

        @NonNull
        @Override
        public List<Runnable> shutdownNow() {
            return Collections.emptyList();
        }

        @Override
        public boolean isShutdown() {
            return false;
        }

        @Override
        public boolean isTerminated() {
            return false;
        }

        @Override
        public boolean awaitTermination(final long timeout, @NonNull final TimeUnit unit) throws InterruptedException {
            return false;
        }
    }
}
