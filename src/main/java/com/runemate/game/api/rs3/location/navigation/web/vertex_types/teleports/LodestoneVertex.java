package com.runemate.game.api.rs3.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import java.io.*;
import java.util.*;
import lombok.*;

@Deprecated
public class LodestoneVertex extends TeleportVertex implements SerializableVertex {
    private Lodestone lodestone;

    public LodestoneVertex(final Lodestone lodestone, Collection<WebRequirement> requirements) {
        super(lodestone.getPosition(), requirements);
        this.lodestone = lodestone;
    }

    public LodestoneVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public Lodestone getLodestone() {
        return lodestone;
    }

    @Override
    public boolean step() {
        return lodestone.teleport();
    }

    @Override
    public int hashCode() {
        return lodestone.hashCode();
    }

    @Override
    public String toString() {
        Coordinate pos = getPosition();
        return "LodestoneVertex(lodestone=" + lodestone.name() +
            ", x=" + pos.getX() + ", y=" + pos.getY() + ", plane=" + pos.getPlane() + ')';
    }

    @Override
    public int getOpcode() {
        return 2;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(lodestone.name());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.lodestone = Lodestone.valueOf(stream.readUTF());
        return true;
    }
}
