package com.runemate.game.api.rs3.local.hud;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.rs3.local.*;
import com.runemate.game.api.rs3.local.hud.eoc.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import org.jetbrains.annotations.*;

@Deprecated
public final class Powers {
    public static int getAdrenalinePercentage() {
        return 0;
    }

    public enum Ranged {
        ;
        private final int spriteId;
        private final int levelRequired;
        private final int adrenalineConsumption;
        private final int activationVarbit;

        Ranged(int spriteId, int levelRequired, int adrenalineConsumption, int activationVarbit) {
            this.spriteId = spriteId;
            this.levelRequired = levelRequired;
            this.adrenalineConsumption = adrenalineConsumption;
            this.activationVarbit = activationVarbit;
        }

        public boolean isActivated() {
            return false;
        }
    }

    @Deprecated
    public enum Prayer {
        THICK_SKIN,
        BURST_OF_STRENGTH,
        CLARITY_OF_THOUGHT,
        SHARP_EYE,
        MYSTIC_WILL,
        UNSTOPPABLE_FORCE,
        CHARGE,
        ROCK_SKIN,
        SUPERHUMAN_STRENGTH,
        IMPROVED_REFLEXES,
        UNRELENTING_FORCE,
        HAWK_EYE,
        SUPER_CHARGE,
        MYSTIC_LORE,
        STEEL_SKIN,
        ULTIMATE_STRENGTH,
        INCREDIBLE_REFLEXES,
        EAGLE_EYE,
        OVERPOWERING_FORCE,
        MYSTIC_MIGHT,
        OVERCHARGE,
        RAPID_RESTORE,
        RAPID_HEAL,
        PROTECT_ITEM,
        PROTECT_FROM_SUMMONING,
        PROTECT_FROM_MAGIC,
        PROTECT_FROM_MISSILES,
        PROTECT_FROM_MELEE,
        RETRIBUTION,
        REDEMPTION,
        SMITE,
        CHIVALRY,
        RAPID_RENEWAL,
        PIETY,
        RIGOUR,
        AUGURY;

        Prayer() {
            // no-op
        }

        public static int getPoints() {
            return 0;
        }

        public static int getMaximumPoints() {
            return 0;
        }

        public static boolean isQuickPraying() {
            return false;
        }

        public static boolean toggleQuickPrayers() {
            return false;
        }

        public String getName() {
            return name();
        }

        public int getRequiredLevel() {
            return 0;
        }

        public boolean isActivated() {
            return false;
        }

        public boolean activate() {
            return false;
        }

        public boolean deactivate() {
            return false;
        }

        public InterfaceComponent getComponent() {
           return null;
        }

        @Override
        public String toString() {
            return "Prayer{name=" + name() + '}';
        }

        @Deprecated
        public enum Curse {
            PROTECT_ITEM,
            SAP_WARRIOR,
            SAP_RANGER,
            SAP_RANGER_STRENGTH,
            SAP_MAGE,
            SAP_MAGIC_STRENGTH,
            SAP_SPIRIT,
            SAP_DEFENCE,
            SAP_STRENGTH,
            BERSERKER,
            DEFLECT_SUMMONING,
            DEFLECT_MAGIC,
            DEFLECT_MISSILES,
            DEFLECT_MELEE,
            LEECH_ATTACK,
            LEECH_RANGED,
            LEECH_RANGE_STRENGTH,
            LEECH_MAGIC,
            LEECH_MAGIC_STRENGTH,
            LEECH_DEFENCE,
            LIGHT_FORM,
            DARK_FORM,
            LEECH_STRENGTH,
            LEECH_ENERGY,
            LEECH_ADRENALINE,
            CHRONICLE_ABSORBTION,
            SOUL_LINK,
            WRATH,
            TEAMWORK_PROTECTION,
            SUPERHEAT_FORM,
            SOUL_SPLIT,
            FORTITUDE,
            TURMOIL,
            ANGUISH,
            TORMENT;

            Curse() {
                // no-op
            }

            public String getName() {
                return name();
            }

            public int getRequiredLevel() {
                return 0;
            }

            public boolean isActivated() {
                return false;
            }

            public boolean toggle() {
                return false;
            }

            public InterfaceComponent getComponent() {
                return null;
            }

            @Override
            public String toString() {
                return "Curse{name='" + name() + "'}";
            }
        }

        public enum Book {
            STANDARD,
            ANCIENT_CURSES;

            public static Book getCurrent() {
                return null;
            }

            public static Book get(int id) {
                return null;
            }

            public int getId() {
                return ordinal();
            }

            public boolean isCurrent() {
                return false;
            }
        }
    }

    @Deprecated
    public enum Magic implements Spell {
        ENCHANT_CROSSBOW_BOLT,
        LVL_1_ENCHANT,
        BONES_TO_BANANAS,
        LOW_LEVEL_ALCHEMY,
        LVL_2_ENCHANT,
        TELEKINETIC_GRAB,
        SUPERHEAT_ITEM,
        LVL_3_ENCHANT,
        HIGH_LEVEL_ALCHEMY,
        CHARGE_WATER_ORB,
        LVL_4_ENCHANT,
        CHARGE_EARTH_ORB,
        BONES_TO_PEACHES,
        CHARGE_FIRE_ORB,
        CHARGE_AIR_ORB,
        LVL_5_ENCHANT,
        LVL_6_ENCHANT,
        HOME_TELEPORT,
        MOBILISING_ARMIES_TELEPORT,
        VARROCK_TELEPORT,
        LUMBRIDGE_TELEPORT,
        FALADOR_TELEPORT,
        HOUSE_TELEPORT,
        CAMELOT_TELEPORT,
        ARDOUGNE_TELEPORT,
        WATCHTOWER_TELEPORT,
        TROLLHEIM_TELEPORT,
        GOD_WARS_DUNGEON_TELEPORT,
        APE_ATOLL_TELEPORT,
        TELE_OTHER_LUMBRIDGE,
        TELE_OTHER_FALADOR,
        TELE_OTHER_CAMELOT,
        AIR_STRIKE,
        CONFUSE,
        WATER_STRIKE,
        EARTH_STRIKE,
        WEAKEN,
        FIRE_STRIKE,
        AIR_BOLT,
        CURSE,
        BIND,
        WATER_BOLT,
        EARTH_BOLT,
        FIRE_BOLT,
        AIR_BLAST,
        WATER_BLAST,
        SNARE,
        SLAYER_DART,
        EARTH_BLAST,
        FIRE_BLAST,
        DIVINE_STORM,
        AIR_WAVE,
        WATER_WAVE,
        VULNERABILITY,
        EARTH_WAVE,
        ENFEEBLE,
        FIRE_WAVE,
        STORM_OF_ARMADYL,
        ENTANGLE,
        STAGGER,
        AIR_SURGE,
        TELEPORT_BLOCK,
        EARTH_SURGE,
        WATER_SURGE,
        FIRE_SURGE,
        POLYPORE_STRIKE;

        Magic() {
            // no-op
        }

        public String getName() {
            return name();
        }

        public boolean activate() {
            return false;
        }

        public InterfaceComponent getComponent() {
            return null;
        }

        public boolean isSelected() {
            return false;
        }

        @Override
        public boolean isAutocasting() {
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.STANDARD;
        }

        @Deprecated
        public enum Ancient implements Spell {
            HOME_TELEPORT,
            PADDEWWA_TELEPORT,
            SENNTISTEN_TELEPORT,
            KHARYRLL_TELEPORT,
            LASSAR_TELEPORT,
            DAREEYAK_TELEPORT,
            CARRALLANGER_TELEPORT,
            ANNAKARL_TELEPORT,
            GHORROCK_TELEPORT,
            SMOKE_RUSH,
            SMOKE_BURST,
            SMOKE_BLITZ,
            SMOKE_BARRAGE,
            SHADOW_RUSH,
            SHADOW_BURST,
            SHADOW_BLITZ,
            SHADOW_BARRAGE,
            BLOOD_RUSH,
            BLOOD_BURST,
            BLOOD_BLITZ,
            BLOOD_BARRAGE,
            ICE_RUSH,
            ICE_BURST,
            ICE_BLITZ,
            ICE_BARRAGE,
            SHIELD_DOME,
            INTERCEPT,
            EMERALD_AURORA,
            SAPPHIRE_AURORA,
            OPAL_AURORA,
            RUBY_AURORA,
            POLYPORE_STRIKE,
            PRISM_OF_DOWSING,
            CRYSTAL_MASK,
            CRYSTALLISE,
            PRISM_OF_RESTORATION,
            PRISM_OF_LOYALTY,
            PRISM_OF_SALVATION,
            RAPID_GROWTH,
            SPELLBOOK_SWAP;

            Ancient() {
                // no-op
            }

            public String getName() {
                return name();
            }

            public boolean activate() {
                return false;
            }

            public InterfaceComponent getComponent() {
                return null;
            }

            public boolean isSelected() {
                return false;
            }

            @Override
            public boolean isAutocasting() {
                return false;
            }

            @Override
            public SpellBook getSpellBook() {
                return Book.ANCIENT;
            }
        }

        @Deprecated
        public enum Lunar implements Spell {
            BAKE_PIE,
            CURE_PLANT,
            NPC_CONTACT,
            HUMIDIFY,
            HUNTER_KIT,
            REPAIR_RUNE_POUCH,
            SUPERGLASS_MAKE,
            REMOTE_FARM,
            STRING_JEWELLERY,
            MAGIC_IMBUE,
            FERTILE_SOIL,
            MAKE_LEATHER,
            PLANK_MAKE,
            TUNE_BANE_ORE,
            SPELLBOOK_SWAP, //Borrowed power cannot be placed on actionbar
            BORROWED_POWER,
            HOME_TELEPORT,
            MOONCLAN_TELEPORT,
            TELE_GROUP_MOONCLAN,
            OURANIA_TELEPORT,
            SOUTH_FALADOR_TELEPORT,
            WATERBIRTH_TELEPORT,
            TELE_GROUP_WATERBIRTH,
            BARBARIAN_TELEPORT,
            NORTH_ARDOUGNE_TELEPORT,
            TELE_GROUP_BARBARIAN,
            KHAZARD_TELEPORT,
            TELEGROUP_KHAZARD,
            FISHING_GUILD_TELEPORT,
            TELE_GROUP_FISHING_GUILD,
            CATHERBY_TELEPORT,
            TELE_GROUP_CATHERBY,
            ICE_PLATEAU_TELEPORT,
            TELE_GROUP_ICE_PLATEAU,
            TROLLHEIM_TELEPORT,
            TELE_GROUP_TROLLHEIM,
            MONSTER_EXAMINE,
            CURE_OTHER,
            CURE_ME,
            CURE_GROUP,
            STAT_SPY,
            DREAM,
            SPIRITUALISE_FOOD,
            STAT_RESTORE_POT_SHARE,
            BOOST_POTION_SHARE,
            DISRUPTION_SHIELD,
            HEAL_OTHER,
            VENGEANCE_OTHER,
            VENGEANCE,
            VENGEANCE_GROUP,
            HEAL_GROUP,
            POLYPORE_STRIKE;

            Lunar() {
                // no-op
            }

            public String getName() {
                return name();
            }

            public boolean activate() {
                return false;
            }

            public InterfaceComponent getComponent() {
                return null;
            }

            public boolean isSelected() {
                return false;
            }

            @Override
            public boolean isAutocasting() {
                return false;
            }

            @Override
            public SpellBook getSpellBook() {
                return Book.LUNAR;
            }
        }

        @Deprecated
        public enum Dungeoneering implements Spell {
            DUNGEON_HOME_TELEPORT,
            CREATE_GATESTONE,
            GATESTONE_TELEPORT,
            GROUP_GATESTONE_TELEPORT,
            CREATE_GATESTONE_2,
            GATESTONE_2_TELEPORT;

            Dungeoneering() {
                // no-op
            }

            public String getName() {
                return name();
            }

            public boolean activate() {
                return false;
            }

            public InterfaceComponent getComponent() {
                return null;
            }

            public boolean isSelected() {
                return false;
            }

            @Override
            public boolean isAutocasting() {
                return false;
            }

            @Override
            public SpellBook getSpellBook() {
                return Book.DUNGEONEERING;
            }
        }


        @Deprecated
        public enum Book implements SpellBook {
            STANDARD,
            ANCIENT,
            LUNAR,
            DUNGEONEERING;

            public static Book getCurrent() {
                return Book.STANDARD;
            }

            public static Book get(int id) {
                return null;
            }

            @Override
            public int getId() {
                return 0;
            }

            @Override
            public boolean isCurrent() {
                return false;
            }
        }

        @Deprecated
        public enum Category {
            ABILITIES,
            COMBAT,
            TELEPORT,
            SKILLING;

            Category() {
                // no-op
            }

            public InterfaceComponent getComponent() {
                return null;
            }

            public boolean isOpen() {
                return false;
            }

            public boolean open() {
                return false;
            }
        }
    }

    @Deprecated
    public static class Defensive {
        @Deprecated
        public enum Category {
            DEFENCE,
            CONSTITUTION
        }
    }

    @Deprecated
    public static class Melee {
        @Deprecated
        public enum Category {
            ATTACK,
            STRENGTH
        }
    }
}
