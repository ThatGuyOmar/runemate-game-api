package com.runemate.game.api.rs3.local;

import com.runemate.game.api.hybrid.local.*;

@Deprecated
public enum InterfaceMode {
    MODERN,
    LEGACY;

    public static InterfaceMode getCurrent() {
        return null;
    }

    public boolean isCurrent() {
        return false;
    }

    @Override
    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase().replace("_", " ");
    }
}
