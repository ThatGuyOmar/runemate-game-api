package com.runemate.game.api.script.framework.listeners.events;

@Deprecated
public class MoneyPouchEvent implements Event {
    private final int contents, change;

    public MoneyPouchEvent(int contents, int change) {
        this.contents = contents;
        this.change = change;
    }

    public int getContents() {
        return contents;
    }

    public int getChange() {
        return change;
    }

    @Override
    public String toString() {
        return "MoneyPouchEvent(contents: " + contents + ", change: " + change + ")";
    }
}
