package com.runemate.game.api.script.framework.logger;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

@Deprecated
public class LoggedMessageEvent implements Event {
    private final BotLogger.Message message;

    public LoggedMessageEvent(BotLogger.Message message) {
        this.message = message;
    }

    public BotLogger.Message getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoggedMessageEvent that = (LoggedMessageEvent) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }

    @Override
    public String toString() {
        return "LoggedMessageEvent{message=" + message + '}';
    }
}
