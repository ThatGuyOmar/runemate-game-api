package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class ProjectileMovedEvent implements EntityEvent {

    Projectile projectile;
    Coordinate destination;

    @Override
    public EntityType getEntityType() {
        return EntityType.PROJECTILE;
    }
}
