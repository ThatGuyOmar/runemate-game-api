package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import lombok.*;

@Value
@ToString(onlyExplicitlyIncluded = true)
public class MenuInteractionEvent implements Event {

    @ToString.Include(rank = 10)
    int param0;
    @ToString.Include(rank = 9)
    int param1;
    @ToString.Include(rank = 8)
    int opcode;
    @ToString.Include(rank = 7)
    int identifier;
    @ToString.Include(rank = 6)
    String action;
    @ToString.Include(rank = 5)
    String target;
    @ToString.Include(rank = 4)
    int mouseX;
    @ToString.Include(rank = 3)
    int mouseY;
    @Deprecated
    @Getter(lazy = true)
    int param2 = identifier;

    @Getter(lazy = true)
    MenuItem.Type targetType = MenuItem.Type.getByOpcode(opcode);

    @Getter(lazy = true)
    Interactable targetEntity = getTargetType().resolve(opcode, identifier, param0, param1);

    @Getter(lazy = true)
    @ToString.Include(rank = -1)
    boolean directInputEvent = "Automated".equals(getAction());

    @Deprecated
    public MenuItem.Type getType() {
        return getTargetType();
    }

    @Deprecated
    public String getOption() {
        return getAction();
    }

    @Deprecated
    public String getTitle() {
        return getTarget();
    }
}
