package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import lombok.*;

@Value
public class NpcSpawnedEvent implements EntityEvent {

    Npc npc;

    @Override
    public EntityType getEntityType() {
        return EntityType.NPC;
    }
}
