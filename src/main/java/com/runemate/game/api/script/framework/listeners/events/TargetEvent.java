package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.details.*;
import java.util.concurrent.*;
import lombok.*;

@Value
public class TargetEvent implements EntityEvent {

    EntityType entityType;
    Animable source;
    Animable target;
}
