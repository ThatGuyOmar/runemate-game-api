package com.runemate.game.api.hybrid.entities.definitions;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.internal.exception.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.*;
import org.jetbrains.annotations.*;
import javax.annotation.Nullable;
import org.jetbrains.annotations.*;

/**
 * The definition of an npc
 */
public abstract class NpcDefinition implements Onymous, Identifiable {

    private static final Cache<Integer, NpcDefinition> OSRS_CACHE = CacheBuilder.newBuilder()
        .expireAfterAccess(1, TimeUnit.MINUTES)
        .build();
    private static final NpcDefinitionLoader osrsDefLoader = new NpcDefinitionLoader(CacheIndex.CONFIGS.getId());

    /**
     * Gets a list of definitions within the range of [first, last]
     */
    public static List<NpcDefinition> get(final int first, final int last) {
        return get(first, last, null);
    }

    /**
     * Gets a list of definitions within the range of [first, last] that are accepted by the filter
     */
    public static List<NpcDefinition> get(final int first, final int last, final Predicate<NpcDefinition> filter) {
        ArrayList<NpcDefinition> definitions = new ArrayList<>(last - first + 1);
        for (int id = first; id <= last; ++id) {
            final NpcDefinition definition = get(id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }

    /**
     * Gets the NpcDefinition for the npc with the specified id
     *
     * @return The definition if available, otherwise null
     */

    @Nullable
    public static NpcDefinition get(final int id) {
        if (id >= 0) {
            try {
                NpcDefinition def = OSRS_CACHE.getIfPresent(id);
                if (def != null) {
                    return def;
                }
                CacheNpcDefinition bdef = osrsDefLoader.load(ConfigType.NPC, id);
                if (bdef != null) {
                    def = bdef.toNpcDefinition();
                    OSRS_CACHE.put(id, def);
                    return def;
                }
            } catch (final IOException ioe) {
                throw new UnableToParseBufferException(
                    "Unable to load npc definition for " + id + ": \"" + ioe.getMessage() + '"',
                    ioe
                );
            }
        }
        return null;
    }

    @NonNull
    @Override
    public abstract String getName();

    public abstract int getOverheadGaugeWidth();

    public abstract boolean appearsOnMinimap();

    /**
     * Gets the powers of this npc
     */
    @NonNull
    public abstract List<String> getActions();

    @Nullable
    public abstract String[] getRawActions();

    /**
     * Gets a list of integers that are used to represent this entities appearance.
     * Internally these are used to generate an Npc's Model.
     */
    @NonNull
    public abstract List<Integer> getAppearance();

    @NonNull
    public abstract List<Integer> getChatHeadAppearance();

    public abstract Attribute getAttribute(final long id);

    @NonNull
    public abstract List<Attribute> getAttributes();

    /**
     * Gets a mapping of colors that are to be substituted in the base model.
     */
    @NonNull
    public abstract Map<Color, Color> getColorSubstitutions();

    /**
     * An id that can be used to identify this npc
     */
    public abstract int getId();

    /**
     * Gets this npc's level
     */
    public abstract int getLevel();

    @Nullable
    public abstract NpcDefinition getLocalState();

    @NonNull
    public abstract Map<Material, Material> getMaterialSubstitutions();

    public abstract int getMovementCapabilitiesFlag();

    /**
     * The icons above this npc
     *
     * @return a list of overhead icons
     */
    @NonNull
    public abstract List<OverheadIcon> getOverheadIcons();

    @NonNull
    public abstract Collection<NpcDefinition> getTransformations();

    public abstract boolean hasPrioritizedRendering();

    /**
     * Gets whether or not the npc with this definition can be interacted with.
     */
    public abstract boolean isClickable();

    public abstract boolean isFollower();

    @Override
    public String toString() {
        return "NpcDefinition(id: " + getId() + ", name: " + getName() + ", actions: " +
            Arrays.toString(getActions().toArray()) + ')';
    }

    //public abstract boolean isHalfStepping();

    public abstract int getAreaEdgeLength();

    /**
     * The {@link Varp} responsible for controlling the state (NpcDefinition) of the Npc, if present.
     */
    @Nullable
    public abstract Varp getStateVarp();


    /**
     * The {@link Varbit} responsible for controlling the state (NpcDefinition) of the Npc, if present.
     */
    @Nullable
    public abstract Varbit getStateVarbit();
}
