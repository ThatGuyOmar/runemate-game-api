package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.concurrent.*;

public class ChatboxQueryBuilder
    extends QueryBuilder<Chatbox.Message, ChatboxQueryBuilder, ChatboxQueryResults> {

    private Collection<String> senders, texts, subtexts;
    private Collection<Chatbox.Message.Type> types;

    @Override
    public boolean accepts(Chatbox.Message argument) {
        if (senders != null && !senders.contains(argument.getSpeaker())) {
            return false;
        }
        if (types != null && !types.contains(argument.getType())) {
            return false;
        }
        if (texts != null) {
            boolean condition = false;
            for (String arg : texts) {
                if (arg.equals(argument.getMessage())) {
                    condition = true;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (subtexts != null) {
            boolean condition = false;
            for (String arg : subtexts) {
                if (argument.getMessage().contains(arg)) {
                    condition = true;
                }
            }
            if (!condition) {
                return false;
            }
        }

        return super.accepts(argument);
    }

    public ChatboxQueryBuilder senders(Collection<String> senders) {
        this.senders = senders;
        return get();
    }

    public ChatboxQueryBuilder texts(Collection<String> texts) {
        this.texts = texts;
        return get();
    }

    public ChatboxQueryBuilder textContains(Collection<String> texts) {
        this.subtexts = texts;
        return get();
    }

    public ChatboxQueryBuilder types(Collection<Chatbox.Message.Type> types) {
        this.types = types;
        return get();
    }

    public ChatboxQueryBuilder senders(String... senders) {
        return senders(Arrays.asList(senders));
    }

    public ChatboxQueryBuilder texts(String... texts) {
        return texts(Arrays.asList(texts));
    }

    public ChatboxQueryBuilder textContains(String... texts) {
        return textContains(Arrays.asList(texts));
    }

    public ChatboxQueryBuilder types(Chatbox.Message.Type... types) {
        return types(Arrays.asList(types));
    }

    @Override
    public ChatboxQueryBuilder get() {
        return this;
    }

    @Override
    protected ChatboxQueryResults results(
        Collection<? extends Chatbox.Message> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new ChatboxQueryResults(entries, cache);
    }

    @Override
    public Callable<List<? extends Chatbox.Message>> getDefaultProvider() {
        return Chatbox::getMessages;
    }
}
