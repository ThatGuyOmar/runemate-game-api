package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public abstract class NpcVertex extends WebVertex implements FilterableVertex<Npc> {

    protected Area walkingBounds;

    protected List<Npc> provider;

    public NpcVertex(final Area walkingBounds, final Collection<WebRequirement> requirements) {
        super(walkingBounds.getCenter(), requirements);
        this.walkingBounds = walkingBounds;
    }

    public NpcVertex(final Coordinate position, final Collection<WebRequirement> requirements) {
        super(position, requirements);
        this.walkingBounds = position.getArea();
    }

    public NpcVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    /**
     * Gets the bounds of the Area that the npc can be within
     */
    public Area getWalkingBounds() {
        return walkingBounds;
    }

    @Override
    public void setProvider(List<Npc> provider) {
        this.provider = provider;
    }

    public abstract Npc getNpc();

    @NonNull
    @Override
    protected Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        if (args.containsKey(NPC_CACHE)) {
            setProvider((List<Npc>) args.get(NPC_CACHE));
        }
        final Npc npc = getNpc();
        if (npc != null) {
            final Area.Rectangular region = (Area.Rectangular) args.get(REGION);
            if (npc.isVisible()) {
                return new Pair<>(this, WebPath.VertexSearchAction.STOP);
            }
            Coordinate npcPos = npc.getPosition();
            if (npcPos != null && npcPos.minimap().isVisible(region, args)) {
                return new Pair<>(
                    new WebPath.PseudoCoordinateVertex(npcPos, Collections.emptyList()),
                    WebPath.VertexSearchAction.STOP
                );
            }
        }
        return new Pair<>(null, WebPath.VertexSearchAction.CONTINUE);
    }

    @Override
    public boolean step(boolean prefersViewport) {
        return step();
    }
}
