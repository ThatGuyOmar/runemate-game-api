package com.runemate.game.api.hybrid.web;

public interface WebPathBuilder {

    WebPath build(WebPathRequest request);

}
