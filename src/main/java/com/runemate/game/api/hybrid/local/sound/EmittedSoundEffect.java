package com.runemate.game.api.hybrid.local.sound;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.script.annotations.*;

public interface EmittedSoundEffect {
    int getAmbientSoundId();

    int[] getShufflingSoundIds();

    int getAudibleRadius();

    GameObjectDefinition getEmittingObject();

    @RS3Only
    Npc getEmittingNpc();

    @RS3Only
    Player getEmittingPlayer();

    int getMinimumCyclesBeforeShuffle();

    int getMaximumCyclesBeforeShuffle();
}
