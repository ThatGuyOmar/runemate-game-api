package com.runemate.game.api.hybrid.local;

import java.util.*;
import lombok.*;

@Getter
@AllArgsConstructor
public enum WorldType {
    MEMBERS(1),
    PVP(1 << 2),
    BOUNTY(1 << 5),
    PVP_ARENA(1 << 6),
    SKILL_TOTAL(1 << 7),
    QUEST_SPEEDRUNNING(1 << 8),
    HIGH_RISK(1 << 10),
    LAST_MAN_STANDING(1 << 14),
    BETA(1 << 16),
    NOSAVE_MODE(1 << 25),
    TOURNAMENT_WORLD(1 << 26),
    FRESH_START_WORLD(1 << 27),
    CASTLE_WARS(1 << 28),
    DEADMAN(1 << 29),
    SEASONAL(1 << 30);

    private final int mask;

    private static final EnumSet<WorldType> PVP_TYPES = EnumSet.of(PVP, DEADMAN);

    public static EnumSet<WorldType> of(final int mask) {
        final EnumSet<WorldType> types = EnumSet.noneOf(WorldType.class);
        for (final var type : values()) {
            if ((mask & type.getMask()) != 0) {
                types.add(type);
            }
        }
        return types;
    }

    public static boolean isPvp(Collection<WorldType> types) {
        return types.stream().anyMatch(PVP_TYPES::contains);
    }

}
