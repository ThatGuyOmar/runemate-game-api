package com.runemate.game.api.hybrid.location;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import javafx.scene.canvas.*;
import javax.annotation.*;
import lombok.*;

public abstract class Area implements Locatable, Interactable, Renderable, Serializable {
    public static Area.Absolute absolute(Collection<Coordinate> coordinates) {
        return new Area.Absolute(coordinates);
    }

    public static Area.Circular circular(Coordinate center, double radius) {
        return new Area.Circular(center, radius);
    }

    public static Area.Polygonal polygonal(Coordinate... vertices) {
        return new Area.Polygonal(vertices);
    }

    public static Area.Rectangular rectangular(
        final Coordinate bottomLeft,
        final Coordinate topRight
    ) {
        return new Area.Rectangular(bottomLeft, topRight);
    }

    public static Area.Rectangular singular(final Coordinate coordinate) {
        return new Area.Rectangular(coordinate);
    }

    /**
     * Returns true if a given locatable is within this Area
     *
     * @return if Area contains the locatable
     */
    public final boolean contains(Locatable locatable) {
        return contains(locatable, false);
    }

    public boolean contains(Locatable locatable, boolean ignorePlane) {
        if (locatable != null) {
            final Coordinate position = locatable.getPosition();
            if (position != null) {
                if (ignorePlane) {
                    for (Coordinate coordinate : getCoordinates()) {
                        if (position.getX() == coordinate.getX()
                            && position.getY() == coordinate.getY()) {
                            return true;
                        }
                    }
                    return false;
                }
                return getCoordinates().contains(position);
            }
        }
        return false;
    }

    public final boolean containsAllOf(Locatable... coordinates) {
        return containsAllOf(coordinates, false);
    }

    public boolean containsAllOf(Locatable[] coordinates, boolean ignorePlane) {
        for (Locatable coordinate : coordinates) {
            if (!contains(coordinate, ignorePlane)) {
                return false;
            }
        }
        return true;
    }

    public final boolean containsAnyOf(Locatable... coordinates) {
        return containsAnyOf(coordinates, false);
    }

    public boolean containsAnyOf(Locatable[] coordinates, boolean ignorePlane) {
        for (Locatable coordinate : coordinates) {
            if (contains(coordinate, ignorePlane)) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    public Coordinate getCenter() {
        List<Coordinate> coordinates = getCoordinates();
        int amount = coordinates.size();
        int xTotal = 0, yTotal = 0, planeTotal = 0;
        for (Coordinate coordinate : coordinates) {
            xTotal += coordinate.getX();
            yTotal += coordinate.getY();
            planeTotal += coordinate.getPlane();
        }
        return new Coordinate(xTotal / amount, yTotal / amount, planeTotal / amount);
    }

    public final <T extends Area> T derive(int x, int y) {
        return derive(x, y, 0);
    }

    public abstract <T extends Area> T derive(int x, int y, int plane);

    /**
     * Gets a list of all coordinates within the area
     */
    public abstract List<Coordinate> getCoordinates();

    public final Set<Coordinate> getOverlappingCoordinates(Area area) {
        return getOverlappingCoordinates(area, false);
    }

    public Set<Coordinate> getOverlappingCoordinates(Area area, boolean ignorePlane) {
        List<Coordinate> coordinates = area.getCoordinates();
        if (coordinates.isEmpty()) {
            return Collections.emptySet();
        }
        Set<Coordinate> overlapping = new HashSet<>();
        for (Coordinate coordinate1 : getCoordinates()) {
            for (Coordinate coordinate2 : coordinates) {
                if (coordinate1.getX() == coordinate2.getX()
                    && coordinate1.getY() == coordinate2.getY()
                    && (ignorePlane || coordinate1.getPlane() == coordinate2.getPlane())) {
                    overlapping.add(coordinate1);
                }
            }
        }
        return overlapping;
    }

    public final boolean overlaps(Area area) {
        return overlaps(area, false);
    }

    public boolean overlaps(Area area, boolean ignorePlane) {
        List<Coordinate> coordinates = area.getCoordinates();
        if (coordinates.isEmpty()) {
            return false;
        }
        for (Coordinate coordinate1 : getCoordinates()) {
            for (Coordinate coordinate2 : coordinates) {
                if (coordinate1.getX() == coordinate2.getX()
                    && coordinate1.getY() == coordinate2.getY()
                    && (ignorePlane || coordinate1.getPlane() == coordinate2.getPlane())) {
                    return true;
                }
            }
        }
        return false;
    }


    @NonNull
    @Override
    public Coordinate getPosition() {
        return getCenter();
    }

    @NonNull
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition() {
        return getPosition().getHighPrecisionPosition();
    }

    /**
     * Calls toRectangular
     */
    @Nullable
    @Override
    public Rectangular getArea() {
        return toRectangular();
    }

    public Coordinate getRandomCoordinate() {
        return Random.nextElement(getCoordinates());
    }

    public boolean isLoaded() {
        List<Coordinate> coordinates = getCoordinates();
        if (!coordinates.isEmpty()) {
            Coordinate region_base = Region.getBase();
            for (Coordinate c : coordinates) {
                if (c.isLoaded(region_base)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isReachable() {
        List<Coordinate> coordinates =
            getCoordinates().stream().filter(Coordinate::isLoaded).collect(Collectors.toList());
        if (!coordinates.isEmpty()) {
            Player local = Players.getLocal();
            if (local == null) {
                return false;
            }
            Set<Coordinate> reachable = local.getPosition().getReachableCoordinates();
            for (final Coordinate coord : coordinates) {
                if (reachable.contains(coord)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isVisible() {
        List<Coordinate> coordinates =
            getCoordinates().stream().filter(Coordinate::isLoaded).collect(Collectors.toList());
        if (!coordinates.isEmpty()) {
            Shape viewport = Projection.getViewport();
            for (final Coordinate c : coordinates) {
                if (c.isVisible(viewport)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public double getVisibility() {
        double total = 0;
        int count = 0;
        List<Coordinate> coordinates = getCoordinates();
        if (!coordinates.isEmpty()) {
            Shape viewport = Projection.getViewport();
            for (Coordinate c : coordinates) {
                total += c.getVisibility(viewport);
                ++count;
            }
        }
        if (count == 0) {
            return 0;
        }
        return total / count;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        List<Coordinate> coordinates = getCoordinates();
        ArrayList<Coordinate> visible = new ArrayList<>(coordinates.size());
        for (final Coordinate c : coordinates) {
            if (c.isVisible()) {
                visible.add(c);
            }
        }
        Coordinate next = Random.nextElement(visible);
        return next != null ? next.getInteractionPoint() : null;
    }

    @Override
    public boolean contains(Point point) {
        List<Coordinate> coordinates = getCoordinates();
        if (!coordinates.isEmpty()) {
            Shape viewport = Projection.getViewport();
            for (final Coordinate c : coordinates) {
                if (c.contains(point, viewport)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean click() {
        List<Coordinate> visible = new ArrayList<>();
        for (final Coordinate c : getCoordinates()) {
            if (c.isVisible()) {
                visible.add(c);
            }
        }
        return !visible.isEmpty() && Random.nextElement(visible).click();
    }

    @Override
    public boolean hover() {
        List<Coordinate> visible = new ArrayList<>();
        for (final Coordinate c : getCoordinates()) {
            if (c.isVisible()) {
                visible.add(c);
            }
        }
        return !visible.isEmpty() && Random.nextElement(visible).hover();
    }

    @Override
    public boolean interact(Pattern action, Pattern target) {
        List<Coordinate> coordinates = getCoordinates();
        List<Coordinate> visible = new ArrayList<>(coordinates.size());
        for (final Coordinate c : coordinates) {
            if (c.isVisible()) {
                visible.add(c);
            }
        }
        Coordinate next = Random.nextElement(visible);
        return next != null && next.interact(action, target);
    }

    @Override
    public boolean interact(String action) {
        List<Coordinate> visible = new ArrayList<>();
        for (final Coordinate c : getCoordinates()) {
            if (c.isVisible()) {
                visible.add(c);
            }
        }
        return !visible.isEmpty() && Random.nextElement(visible).interact(action);
    }

    @Override
    public boolean interact(Pattern action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public boolean interact(String action, String target) {
        List<Coordinate> visible = new ArrayList<>();
        for (final Coordinate c : getCoordinates()) {
            if (c.isVisible()) {
                visible.add(c);
            }
        }
        return !visible.isEmpty() && Random.nextElement(visible).interact(action, target);
    }

    @Override
    public void render(Graphics2D g2d) {
        List<Coordinate> coordinates = getCoordinates();
        if (!coordinates.isEmpty()) {
            Shape viewport = Projection.getViewport();
            coordinates.forEach(c -> c.render(g2d, viewport));
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        List<Coordinate> coordinates = getCoordinates();
        if (!coordinates.isEmpty()) {
            Shape viewport = Projection.getViewport();
            coordinates.stream().filter(c -> c.minimap().isVisible())
                .forEach(c -> c.render(gc, viewport));
        }
    }

    /**
     * Converts an Area into a Area.Rectangular that contains the entire Area
     */
    public abstract Rectangular toRectangular();

    public abstract boolean equals(Object obj);

    public abstract int hashCode();

    public abstract String toString();

    /**
     * An area that has every contained coordinate specified in the constructor
     */
    public static class Absolute extends Area {
        private final List<Coordinate> coordinates;

        public Absolute(Collection<Coordinate> coordinates) {
            this.coordinates = new ArrayList<>(coordinates);
        }

        public Absolute(Coordinate... coordinates) {
            this(Arrays.asList(coordinates));
        }

        @Override
        public Area.Absolute derive(int x, int y, int plane) {
            return new Absolute(
                coordinates.stream().map(c -> c.derive(x, y, plane)).collect(Collectors.toList()));
        }

        @Override
        public List<Coordinate> getCoordinates() {
            return coordinates;
        }

        @Override
        public Rectangular toRectangular() {
            int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
            int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
            int plane = -1;
            for (final Coordinate coordinate : coordinates) {
                if (plane == -1) {
                    plane = coordinate.getPlane();
                }
                if (minX > coordinate.getX()) {
                    minX = coordinate.getX();
                }
                if (maxX < coordinate.getX()) {
                    maxX = coordinate.getX();
                }
                if (minY > coordinate.getY()) {
                    minY = coordinate.getY();
                }
                if (maxY < coordinate.getY()) {
                    maxY = coordinate.getY();
                }
            }
            return new Rectangular(
                new Coordinate(minX, minY, plane),
                new Coordinate(maxX, maxY, plane)
            );
        }

        @Override
        public int hashCode() {
            return coordinates.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Absolute) {
                Absolute that = (Absolute) obj;
                return Objects.equals(this.coordinates, that.coordinates);
            }
            return false;
        }

        @Override
        public String toString() {
            return "Area.Absolute[" + Arrays.toString(coordinates.toArray()) + ']';
        }
    }

    public static class Circular extends Area {
        private final Coordinate center;
        private final double radius;
        private List<Coordinate> coordinates;

        public Circular(final Coordinate center, final double radius) {
            if (center == null) {
                throw new IllegalArgumentException(
                    "The center of a circular area cannot be a null Coordinate.");
            }
            this.center = center;
            this.radius = radius;
        }

        @Override
        public Area.Circular derive(int x, int y, int plane) {
            return new Circular(center.derive(x, y, plane), radius);
        }

        @Override
        public final boolean contains(Locatable locatable, boolean ignorePlane) {
            if (locatable != null) {
                Coordinate position = locatable.getPosition();
                if (position != null) {
                    if (ignorePlane || center.getPlane() == position.getPlane()) {
                        return Distance.between(center, position, Distance.Algorithm.EUCLIDEAN) <=
                            radius;
                    }
                }
            }
            return false;
        }

        public double getRadius() {
            return radius;
        }

        @Override
        public List<Coordinate> getCoordinates() {
            if (this.coordinates == null) {
                final List<Coordinate> coordinates = new ArrayList<>();
                for (double x = center.getX() - radius; x <= center.getX() + radius; ++x) {
                    for (double y = center.getY() - radius; y <= center.getY() + radius; ++y) {
                        final Coordinate coordinate =
                            new Coordinate((int) x, (int) y, center.getPlane());
                        if (contains(coordinate, false)) {
                            coordinates.add(coordinate);
                        }
                    }
                }
                this.coordinates = coordinates;
            }
            return coordinates;
        }

        @Override
        public Rectangular toRectangular() {
            int radius = (int) this.radius;
            return new Rectangular(center.derive(-radius, -radius), center.derive(radius, radius));
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Circular) {
                Circular that = (Circular) obj;
                return Objects.equals(this.center, that.center) &&
                    Double.compare(this.radius, that.radius) == 0;
            }
            return false;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = center.hashCode();
            temp = Double.doubleToLongBits(radius);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return "Area[center=" + center + ",radius=" + radius + ']';
        }

        @NonNull
        @Override
        public Coordinate getCenter() {
            return center;
        }
    }

    public static class Polygonal extends Area {
        private final Polygon bounds;
        private final int plane;
        private List<Coordinate> coordinates;

        public Polygonal(final Coordinate... vertices) {
            if (vertices == null || vertices.length == 0) {
                throw new IllegalArgumentException(
                    "The coordinates that are provided to the constructor of an Area.Polygonal must not be null and must have a length that's greater than 0.");
            }
            int[] xPoints = new int[vertices.length],
                yPoints = new int[vertices.length],
                planes = new int[vertices.length];
            for (int i = 0; i < xPoints.length; ++i) {
                final Coordinate vertex = vertices[i];
                planes[i] = vertex.getPlane();
                xPoints[i] = vertex.getX();
                yPoints[i] = vertex.getY();
            }
            int lastPlane = -1;
            for (int plane : planes) {
                if (lastPlane == -1) {
                    lastPlane = plane;
                } else if (lastPlane != plane) {
                    throw new IllegalArgumentException("The coordinates that are used to build an Area.Polygonal must all exist on the same plane.");
                }
            }
            this.plane = lastPlane;
            this.bounds = new Polygon(xPoints, yPoints, xPoints.length);
        }

        @Override
        public Area.Polygonal derive(int x, int y, int plane) {
            final Coordinate[] coordinates = new Coordinate[bounds.xpoints.length];
            for (int i = 0; i < bounds.xpoints.length; i++) {
                coordinates[i] = new Coordinate(bounds.xpoints[i] + x, bounds.ypoints[i] + y, this.plane + plane);
            }
            return new Polygonal(coordinates);
        }

        /**
         * Gets a list of the vertices that were used to build this Area.
         */
        public List<Coordinate> getVertices() {
            ArrayList<Coordinate> res = new ArrayList<>(bounds.npoints);
            for (int i = 0; i < bounds.npoints; ++i) {
                res.add(new Coordinate(bounds.xpoints[i], bounds.ypoints[i], plane));
            }
            return res;
        }

        @Override
        public List<Coordinate> getCoordinates() {
            if (this.coordinates == null) {
                final List<Coordinate> coordinates = new ArrayList<>();
                final Rectangle boundingBox = bounds.getBounds();
                for (int x = boundingBox.x; x < boundingBox.x + boundingBox.width; ++x) {
                    for (int y = boundingBox.y; y < boundingBox.y + boundingBox.height; ++y) {
                        if (bounds.contains(x, y)) {
                            coordinates.add(new Coordinate(x, y, plane));
                        }
                    }
                }
                this.coordinates = coordinates;
            }
            return this.coordinates;
        }

        @Override
        public Rectangular toRectangular() {
            int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
            int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
            for (final Coordinate coordinate : getCoordinates()) {
                if (minX > coordinate.getX()) {
                    minX = coordinate.getX();
                }
                if (maxX < coordinate.getX()) {
                    maxX = coordinate.getX();
                }
                if (minY > coordinate.getY()) {
                    minY = coordinate.getY();
                }
                if (maxY < coordinate.getY()) {
                    maxY = coordinate.getY();
                }
            }
            return new Rectangular(
                new Coordinate(minX, minY, plane),
                new Coordinate(maxX, maxY, plane)
            );
        }

        @Override
        public int hashCode() {
            int result = bounds.hashCode();
            result = 31 * result + plane;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Polygonal) {
                Polygonal that = (Polygonal) obj;
                if (this.plane == that.plane) {
                    if (bounds == null) {
                        return (that.bounds == null);
                    }
                    if (that.bounds == null) {
                        return false;
                    }
                    if (bounds.npoints != that.bounds.npoints) {
                        return false;
                    }
                    if (!Arrays.equals(bounds.xpoints, that.bounds.xpoints)) {
                        return false;
                    }
                    if (!Arrays.equals(bounds.ypoints, that.bounds.ypoints)) {
                        return false;
                    }
                }
            }
            return false;
        }

        @Override
        public boolean contains(Locatable locatable, boolean ignorePlane) {
            if (locatable != null) {
                final Coordinate position = locatable.getPosition();
                if (position != null) {
                    return (ignorePlane || plane == position.getPlane())
                        && bounds.contains(position.getX(), position.getY());
                }
            }
            return false;
        }

        @NonNull
        @Override
        public Coordinate getCenter() {
            if (bounds.npoints <= 0) {
                return new Coordinate(0, 0, 0);
            }
            int totalX = 0, totalY = 0;
            for (int i = 0; i < bounds.npoints; ++i) {
                totalX += bounds.xpoints[i];
                totalY += bounds.ypoints[i];
            }
            return new Coordinate(totalX / bounds.npoints, totalY / bounds.npoints, plane);
        }

        @Override
        public String toString() {
            return "Area.Polygonal" + getVertices();
        }
    }

    public static class Rectangular extends Area {
        private final Coordinate bottomLeft;
        private final Coordinate topRight;
        private Coordinate center;

        /**
         * Generates an area containing a single coordinate
         */
        public Rectangular(final Coordinate position) {
            this(position, position);
        }

        /**
         * Constructs a rectangular Area from the bottom left and top right coordinates
         *
         * @param bottomLeft The bottom left coordinate of the Area (Inclusive)
         * @param topRight   The top right coordinate of the Area (Inclusive)
         */
        public Rectangular(final Coordinate bottomLeft, final Coordinate topRight) {
            this.bottomLeft = new Coordinate(
                Math.min(bottomLeft.getX(), topRight.getX()),
                Math.min(bottomLeft.getY(), topRight.getY()),
                Math.min(bottomLeft.getPlane(), topRight.getPlane())
            );
            this.topRight = new Coordinate(
                Math.max(bottomLeft.getX(), topRight.getX()),
                Math.max(bottomLeft.getY(), topRight.getY()),
                Math.max(bottomLeft.getPlane(), topRight.getPlane())
            );
        }


        @Override
        public Rectangular derive(int x, int y, int plane) {
            return new Rectangular(bottomLeft.derive(x, y, plane), topRight.derive(x, y, plane));
        }

        /**
         * @param horizontal the amount to grow horizontally on each side on the x axis
         * @param vertical   the amount to grow vertically on each side on the y axis
         * @return an enlarged (or shrunk if negative values are used) Area.Rectangular
         */
        @NonNull
        public Area.Rectangular grow(int horizontal, int vertical) {
            return Area.rectangular(
                bottomLeft.derive(-horizontal, -vertical),
                topRight.derive(horizontal, vertical)
            );
        }

        public Coordinate getBottomLeft() {
            return bottomLeft;
        }

        public Coordinate getBottomRight() {
            return new Coordinate(topRight.getX(), bottomLeft.getY(), bottomLeft.getPlane());
        }

        public int getHeight() {
            return topRight.getY() - bottomLeft.getY() + 1;
        }

        @Override
        public List<Coordinate> getCoordinates() {
            final List<Coordinate> coordinates = new ArrayList<>();
            for (int plane = bottomLeft.getPlane(); plane <= topRight.getPlane(); ++plane) {
                for (int x = bottomLeft.getX(); x <= topRight.getX(); ++x) {
                    for (int y = bottomLeft.getY(); y <= topRight.getY(); ++y) {
                        coordinates.add(new Coordinate(x, y, plane));
                    }
                }
            }
            return coordinates;
        }

        /**
         * Get the coordinates surrounding this rectangular area
         */
        @Deprecated
        public List<Coordinate> getSurrounding() {
            return getSurroundingCoordinates();
        }

        @Override
        public Coordinate getRandomCoordinate() {
            return bottomLeft.derive(Random.nextInt(getWidth()), Random.nextInt(getHeight()));
        }

        public List<Coordinate> getSurroundingCoordinates() {
            int width = getWidth(), height = getHeight();
            final List<Coordinate> coordinates = new ArrayList<>(((width + 2) << 1) + (height << 1));
            for (int x = -1; x < width; x++) {
                coordinates.add(bottomLeft.derive(x, -1));
                coordinates.add(topRight.derive(-x, 1));
            }
            for (int y = 0; y < height + 1; y++) {
                coordinates.add(bottomLeft.derive(-1, y));
                coordinates.add(topRight.derive(1, -y));
            }
            return coordinates;
        }

        public Coordinate getTopLeft() {
            return new Coordinate(bottomLeft.getX(), topRight.getY(), topRight.getPlane());
        }

        public Coordinate getTopRight() {
            return topRight;
        }

        @Override
        public Rectangular toRectangular() {
            return this;
        }

        public int getWidth() {
            return topRight.getX() - bottomLeft.getX() + 1;
        }

        @Override
        public boolean contains(Locatable locatable, boolean ignorePlane) {
            if (locatable == null) {
                return false;
            }
            Coordinate position = locatable.getPosition();
            return position != null
                && position.getX() >= bottomLeft.getX()
                && position.getY() >= bottomLeft.getY()
                && (ignorePlane || position.getPlane() >= bottomLeft.getPlane())
                && position.getX() <= topRight.getX()
                && position.getY() <= topRight.getY()
                && (ignorePlane || position.getPlane() <= topRight.getPlane());
        }

        @NonNull
        @Override
        public Coordinate getCenter() {
            if (center != null) {
                return center;
            }
            return center = bottomLeft.derive(
                (topRight.getX() - bottomLeft.getX()) >> 1,
                (topRight.getY() - bottomLeft.getY()) >> 1
            );
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Rectangular) {
                Rectangular that = (Rectangular) obj;
                return Objects.equals(this.bottomLeft, that.bottomLeft) &&
                    Objects.equals(this.topRight, that.topRight);
            }
            return false;
        }

        @Override
        public int hashCode() {
            int result = bottomLeft.hashCode();
            result = 31 * result + topRight.hashCode();
            return result;
        }

        @Override
        public String toString() {
            if (Objects.equals(bottomLeft, topRight)) {
                return "Area(" + bottomLeft.getX() + ", " + bottomLeft.getY() + ", " +
                    bottomLeft.getPlane() + ')';
            }
            return "Area("
                + bottomLeft.getX() + ", " + bottomLeft.getY() + ", " + bottomLeft.getPlane()
                + " -> "
                + topRight.getX() + ", " + topRight.getY() + ", " + topRight.getPlane()
                + ')';
        }
    }
}
