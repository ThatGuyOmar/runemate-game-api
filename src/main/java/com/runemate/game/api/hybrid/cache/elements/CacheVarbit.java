package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;

public class CacheVarbit extends IncrementallyDecodedItem {
    private final boolean rs3;
    public int typeId, varIndex, lsb, msb;

    public CacheVarbit() {
        this.rs3 = false;
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (rs3) {
            if (opcode == 1) {
                typeId = stream.readUnsignedByte();
                varIndex = stream.readDefaultableUnsignedSmart();
            } else if (opcode == 2) {
                lsb = stream.readUnsignedByte();
                msb = stream.readUnsignedByte();
            } else {
                throw new IllegalStateException("Opcode '" + opcode + "' is unsupported on rs3!");
            }
        } else if (opcode == 1) {
            varIndex = stream.readUnsignedShort();
            lsb = stream.readUnsignedByte();
            msb = stream.readUnsignedByte();
        } else {
            throw new IllegalStateException("Opcode '" + opcode + "' is unsupported on osrs!");
        }
    }

    @Override
    public String toString() {
        return "Varbit{type=" + "varp=" + varIndex + ", lsb=" + lsb + ", msb=" + msb + '}';
    }

}
