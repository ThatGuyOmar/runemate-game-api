package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.annotations.*;
import java.util.regex.*;

public final class NpcContact {

    private NpcContact() {
    }

    public static boolean isOpen() {
        return OSRSNpcContact.isOpen();
    }

    public static boolean cast(Contact contact) {
        return OSRSNpcContact.cast(contact);
    }

    @Deprecated
    @RS3Only
    public enum RS3 implements Contact {
        HONEST_JIMMY(39610),
        BERT_THE_SANDMAN(39639),
        ADVISOR_GRIM(39692),
        SPRIA(35469),
        LANTHUS(85001),
        SUMONA(34607),
        MAZCHNA(76958),
        DURADEL(39789),
        LAPALOK(35497),
        VANNAKA(39680),
        MURPHY(39983),
        CHAELDAR(11907),
        CYRISUS(39916),
        LARRY(39772),
        KURADAL(50668),
        DARK_MAGE(50842),
        PIKKUPSTIX(39845),
        PAULINE_POLARIS(64718),
        ONEIROMANCER(38970),
        ISLWYN(97205),
        MEILIKI_TAPIO(72969),
        MOVRAN(98933),
        RANDOM(24991);
        private static final Pattern ACTION = Pattern.compile("^Speak-to$");
        private final int projectedBufferId;

        RS3(int projectedBufferId) {
            this.projectedBufferId = projectedBufferId;
        }

        @Override
        public InterfaceComponent getComponent() {
            return Interfaces.newQuery()
                .projectedBufferId(projectedBufferId)
                .actions(getAction())
                .containers(getContainerId())
                .types(InterfaceComponent.Type.MODEL)
                .results()
                .first();
        }

        @Override
        public int getContainerId() {
            return 88;
        }

        @Override
        public Pattern getAction() {
            return ACTION;
        }

        @Override
        public String toString() {
            return "Contact." + name();
        }
    }

    @OSRSOnly
    public enum OSRS implements Contact {
        HONEST_JIMMY("Honest Jimmy"),
        BERT_THE_SANDMAN("Bert the Sandman"),
        ADVISOR_GHRIM("Advisor Ghrim"),
        DARK_MAGE("Dark Mage"),
        LANTHUS("Lanthus"),
        TURAEL("Turael"),
        MAZCHNA("Mazchna"),
        VANNAKA("Vannaka"),
        CHAELDAR("Chaeldar"),
        NIEVE_STEVE("Nieve|Steve"),
        DURADEL("Duradel"),
        KRYSTILIA("Krystilia"),
        MURPHY("Murphy"),
        CYRISUS("Cyrisus"),
        SMOGGY("Smoggy"),
        GINEA("Ginea"),
        WATSON("Watson");
        private final Pattern name;

        OSRS(String name) {
            this.name = Pattern.compile("^" + name + "$");
        }

        @Override
        public InterfaceComponent getComponent() {
            return Interfaces.newQuery().actions(getAction()).containers(getContainerId())
                .types(InterfaceComponent.Type.CONTAINER).results().first();
        }

        @Override
        public int getContainerId() {
            return 75;
        }

        @Override
        public Pattern getAction() {
            return name;
        }

        @Override
        public String toString() {
            return "Contact." + name();
        }
    }

    public interface Contact {
        InterfaceComponent getComponent();

        Pattern getAction();

        int getContainerId();

        String name();
    }

}
