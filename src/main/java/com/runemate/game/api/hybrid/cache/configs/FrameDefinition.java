package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;

public class FrameDefinition implements DecodedItem {
    
    private final int id;
    private final FrameMapDefinition framemap;
    public int[] translatorX;
    public int[] translatorY;
    public int[] translatorZ;
    public int translatorCount = -1;
    public int[] indexFrameIds;
    public boolean showing;

    public FrameDefinition(final int id, FrameMapDefinition framemap) {
        this.id = id;
        this.framemap = framemap;
    }

    @Override
    public void decode(final Js5InputStream in) throws IOException {
        final var data = new Js5InputStream(in.getBackingArray());

        int framemapArchiveIndex = in.readUnsignedShort();
        int length = in.readUnsignedByte();

        data.skip(3 + length); // framemapArchiveIndex + length + data

        int[] indexFrameIds = new int[500];
        int[] scratchTranslatorX = new int[500];
        int[] scratchTranslatorY = new int[500];
        int[] scratchTranslatorZ = new int[500];

        int lastI = -1;
        int index = 0;
        for (int i = 0; i < length; ++i)
        {
            int var9 = in.readUnsignedByte();

            if (var9 <= 0)
            {
                continue;
            }

            if (framemap.getTypes()[i] != 0)
            {
                for (int var10 = i - 1; var10 > lastI; --var10)
                {
                    if (framemap.getTypes()[var10] == 0)
                    {
                        indexFrameIds[index] = var10;
                        scratchTranslatorX[index] = 0;
                        scratchTranslatorY[index] = 0;
                        scratchTranslatorZ[index] = 0;
                        ++index;
                        break;
                    }
                }
            }

            indexFrameIds[index] = i;
            short var11 = 0;
            if (framemap.getTypes()[i] == 3)
            {
                var11 = 128;
            }

            if ((var9 & 1) != 0)
            {
                scratchTranslatorX[index] = data.readShortSmart();
            }
            else
            {
                scratchTranslatorX[index] = var11;
            }

            if ((var9 & 2) != 0)
            {
                scratchTranslatorY[index] = data.readShortSmart();
            }
            else
            {
                scratchTranslatorY[index] = var11;
            }

            if ((var9 & 4) != 0)
            {
                scratchTranslatorZ[index] = data.readShortSmart();
            }
            else
            {
                scratchTranslatorZ[index] = var11;
            }

            lastI = i;
            ++index;
            if (framemap.getTypes()[i] == 5)
            {
                showing = true;
            }
        }

        translatorCount = index;
        indexFrameIds = new int[index];
        translatorX = new int[index];
        translatorY = new int[index];
        translatorZ = new int[index];

        for (int i = 0; i < index; ++i)
        {
            this.indexFrameIds[i] = indexFrameIds[i];
            translatorX[i] = scratchTranslatorX[i];
            translatorY[i] = scratchTranslatorY[i];
            translatorZ[i] = scratchTranslatorZ[i];
        }
    }
}
