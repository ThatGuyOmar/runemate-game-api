package com.runemate.game.api.hybrid.location.navigation.web;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.location.navigation.web.exceptions.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.Timer;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.beust.jcommander.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import javax.annotation.Nullable;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

/**
 * A path that is generated from a Web instance.
 */
@Log4j2
public class WebPath extends Path {

    private final List<WebVertex> steps;
    private final double traversalCost;
    private Set<WebRequirement> requirements, blockingConditions;
    private WebVertex current = null;
    private int scanIndex = 0;

    public WebPath(List<WebVertex> vertices, double traversalCost) {
        this.traversalCost = traversalCost;
        this.steps = Collections.unmodifiableList(vertices);
    }

    public WebPath(
        List<WebVertex> vertices, Set<WebRequirement> requirements,
        double traversalCost
    ) {
        this.traversalCost = traversalCost;
        this.requirements = requirements;
        this.steps = Collections.unmodifiableList(vertices);
    }

    public WebPath(
        List<WebVertex> vertices, Set<WebRequirement> requirements,
        Set<WebRequirement> blockingConditions, double traversalCost
    ) {
        this.traversalCost = traversalCost;
        this.requirements = requirements;
        this.blockingConditions = blockingConditions;
        this.steps = Collections.unmodifiableList(vertices);
    }

    public boolean areRequirementsMet() {
        return getRequirements().stream()
            .noneMatch(requirement -> !requirement.isMet() && !requirement.isAlternativeMet())
            && getBlockingConditions().stream()
            .noneMatch(condition -> condition.isMet() || condition.isAlternativeMet());
    }

    @Nullable
    public Locatable getDestination() {
        return !steps.isEmpty() ? steps.get(steps.size() - 1) : null;
    }

    @NonNull
    public Set<WebRequirement> getRequirements() {
        if (requirements == null) {
            requirements = new HashSet<>(steps.size() >> 1);
            steps.forEach(vertex -> {
                requirements.addAll(vertex.getRequirements());
            });
        }
        return requirements;
    }

    @Deprecated
    @NonNull
    public Set<WebRequirement> getBlockingConditions() {
        if (blockingConditions == null) {
            blockingConditions = new HashSet<>(steps.size() >> 1);
            steps.forEach(vertex -> {
                blockingConditions.addAll(vertex.getBlockingConditions());
            });
        }
        return blockingConditions;
    }

    @Nullable
    public Locatable getStart() {
        return !steps.isEmpty() ? steps.get(0) : null;
    }

    public double getTraversalCost() {
        return traversalCost;
    }

    @Override
    public boolean step(@NonNull TraversalOption... options) {
        boolean toggleRunBeforeStepping =
            PlayerSense.getAsBoolean(PlayerSense.Key.TOGGLE_RUN_BEFORE_TRAVERSING);
        if (toggleRunBeforeStepping && !triggerRun(options)) {
            return false;
        }
        if (isEligibleToStep(options)) {
            WebVertex next = Timer.run("WebPath#getNext()", this::getNext);
            if (next != null) {
                this.current = null;
                if (!next.step()) {
                    return false;
                } else if (!(next instanceof PseudoCoordinateVertex)) {
                    int lastSuccess = getVertices().indexOf(next);
                    if (lastSuccess != -1) {
                        if (next instanceof ObjectVertex) {
                            lastSuccess++;
                        }
                        log.debug("Incremented the last successful step index cache to {}", lastSuccess);
                        scanIndex = lastSuccess;
                    } else {
                        throw new IllegalStateException("The vertex for the most recently taken step isn't in the vertex list!");
                    }
                }
            } else {
                if (scanIndex != 0) {
                    log.debug("getNext() returned null after analyzing {} steps, resetting.", scanIndex);
                    reset();
                } else {
                    log.debug("getNext() returned null, construct a new path.");
                }
                return false;
            }
        }
        return triggerStaminaEnhancement(options) &&
            (toggleRunBeforeStepping || triggerRun(options));
    }

    @Override
    public List<WebVertex> getVertices() {
        return steps;
    }

    @Override
    public WebVertex getNext() {
        return getNext(false);
    }

    @Override
    public WebVertex getNext(boolean preferViewportTraversal) {
        if (current != null) {
            final WebVertex result = current;
            current = null;
            return result;
        }
        if (steps.isEmpty()) {
            return null;
        }
        TeleportVertex teleport = null;
        final Player avatar = Players.getLocal();
        if (avatar == null) {
            return null;
        }
        final Area.Rectangular region = Region.getArea();
        if (region == null) {
            return null;
        }
        final Coordinate avatarPosition = avatar.getPosition(region.getBottomLeft());
        if (avatarPosition == null) {
            return null;
        }
        final int[][] flags = Region.getCollisionFlags(region.getPosition().getPlane());
        if (flags == null) {
            return null;
        }
        final Set<Coordinate> reachable = avatarPosition.getReachableCoordinates(flags);
        if (reachable.isEmpty()) {
            return null;
        }
        WebVertex recommended = null;
        Predicate<Npc> npcPredicate = null;
        Predicate<SpriteItem> itemPredicate = null;
        for (WebVertex step : steps) {
            if (step instanceof FilterableVertex) {
                final FilterableVertex s = (FilterableVertex) step;
                final Predicate p = s.getFilter();
                if (s instanceof NpcVertex) {
                    npcPredicate = npcPredicate != null ? npcPredicate.or(p) : p;
                } else if (s instanceof ItemTeleportVertex) {
                    itemPredicate = itemPredicate != null ? itemPredicate.or(p) : p;
                }
            }
        }

        final Predicate<Npc> nFilter = npcPredicate;
        final Predicate<SpriteItem> sFilter = itemPredicate;

        final LocatableEntityQueryResults<Npc> npcPreResults =
            Npcs.newQuery().filter(nFilter).results();
        final SpriteItemQueryResults itemPreResults =
            Inventory.newQuery().filter(sFilter).results();
        itemPreResults.addAll(Equipment.newQuery().filter(sFilter).results());

        final HashMap<String, Object> cache = new HashMap<>();
        cache.put(WebVertex.AVATAR, avatar);
        cache.put(WebVertex.AVATAR_POS, avatarPosition);
        cache.put(WebVertex.REGION, region);
        cache.put(WebVertex.REGION_BASE, region.getBottomLeft());
        cache.put(WebVertex.STEPS, steps);
        cache.put(WebVertex.REACHABLE, reachable);

        cache.put(WebVertex.NPC_CACHE, npcPreResults.asList());
        cache.put(WebVertex.ITEM_CACHE, itemPreResults.asList());
        cache.put(WebVertex.PREFERS_VIEWPORT, preferViewportTraversal);

        int scanned = 0;
        //log.info("Last successful step: " + (scanIndex != -1 ? steps.get(scanIndex) : "-1"));
        for (int i = scanIndex; i < steps.size(); i++) {
            final WebVertex current = steps.get(i);
            final Locatable previous = i > 0 ? steps.get(i - 1) : null;
            cache.put(WebVertex.PREVIOUS, previous);
            cache.put(WebVertex.INDEX_IN_STEPS, i);
            final Pair<WebVertex, VertexSearchAction> step = current.getStep(cache);
            if (step == null) {
                log.warn("Failed to get the potential step of " + current.getClass().getSimpleName());
                return null;
            }
            final WebVertex suggested = step.getLeft();
            if (suggested != null) {
                if (suggested instanceof TeleportVertex) {
                    if (teleport != null) {
                        throw new MalformedWebPathException("The WebPath in usage contains more than one teleport vertex.");
                    }
                    teleport = (TeleportVertex) suggested;
                } else {
                    recommended = suggested;
                }
            }
            scanned++;
            if (VertexSearchAction.STOP.equals(step.getRight())) {
                break;
            }
        }
        if (recommended == null && !avatarPosition.equals(getDestination())) {
            recommended = teleport;
        }
        current = recommended;
        log.debug(
            "Next step is {} at distance {} having scanned {} vertices.",
            current,
            CommonMath.round(Distance.between(avatar, current), 2),
            scanned
        );
        Parallelize.collect(steps, FilterableVertex.class::isInstance).forEach(v -> ((FilterableVertex<?>) v).invalidateCache());
        return recommended;
    }

    /**
     * Resets the last successful step counter
     */
    public void reset() {
        current = null;
        scanIndex = 0;
    }

    @Override
    public String toString() {
        final var join = new StringJoiner(", ", "WebPath(", ")");
        join.add("cost=" + Math.round(traversalCost));

        if (!getRequirements().isEmpty()) {
            join.add("reqs=" + getRequirements());
        }
        if (!getBlockingConditions().isEmpty()) {
            join.add("blocking=" + getBlockingConditions());
        }
        join.add("vertices=" + Strings.join(" -> ", steps.stream().map(Objects::toString).collect(Collectors.toList())));
        return join.toString();
    }

    public enum VertexSearchAction {
        CONTINUE,
        STOP
    }

    @ToString
    public static class PseudoCoordinateVertex extends CoordinateVertex {

        public PseudoCoordinateVertex(Coordinate position, Collection<WebRequirement> requirements) {
            super(position, requirements);
        }
    }
}
