package com.runemate.game.api.hybrid.web.vertex;

import com.runemate.client.game.account.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.util.*;
import lombok.*;

@ToString
public class SpellTeleportVertex extends TeleportVertex {

    private final Spell spell;

    public SpellTeleportVertex(@NonNull final Coordinate position, @NonNull Spell spell) {
        super(position);
        this.spell = spell;
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        if (Bank.isOpen()) {
            return Bank.close();
        }

        var success = spell.activate()
            && Execution.delayUntil(() -> local.getAnimationId() != -1, 2400, 3600)
            && Execution.delayWhile(() -> localPos.equals(local.getPosition()), () -> local.getAnimationId() != -1, 2400);

        if (!success) {
            return false;
        }

        if (isHomeTeleport()) {
            var timers = OpenAccountDetails.getTraversalProfile().getCachedTimers();
            timers.put("home_teleport", new CachedTimer("home_teleport", System.currentTimeMillis()));
        }

        return true;
    }

    private boolean isHomeTeleport() {
        return spell == Magic.LUMBRIDGE_HOME_TELEPORT
            || spell == Magic.Lunar.LUNAR_HOME_TELEPORT
            || spell == Magic.Ancient.EDGEVILLE_HOME_TELEPORT
            || spell == Magic.Arceuus.ARCEUUS_HOME_TELEPORT;
    }
}
