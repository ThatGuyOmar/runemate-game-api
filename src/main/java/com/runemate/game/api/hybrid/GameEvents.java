package com.runemate.game.api.hybrid;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.handler.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.events.*;
import java.util.*;
import java.util.concurrent.*;
import lombok.*;
import org.jetbrains.annotations.*;

public final class GameEvents {

    private GameEvents() {
    }

    @Nullable
    public static GameEvent get(String s) {
        for (GameEvent event : Universal.values()) {
            if (event.getName().equals(s)) {
                return event;
            }
        }
        for (GameEvent event : OSRS.values()) {
            if (event.getName().equals(s)) {
                return event;
            }
        }
        return null;
    }

    public enum Universal implements GameEvent {
        BANK_PIN, //INACTIVITY_SHUTDOWN_FAILSAFE,
        INTERFACE_CLOSER,
        LOBBY_HANDLER,
        UNEXPECTED_ITEM_HANDLER,
        LOGIN_HANDLER,
        GENIE_HANDLER;
        private String name;

        Universal() {
            this(null);
        }

        Universal(String name) {
            this.name = name;
        }

        @NonNull
        @Override
        public List<GameEvent> getChildren(AbstractBot bot) {
            final GameEvent parent = ((GameEventControllerImpl) bot.getGameEventController()).getGameEvent(this);
            return parent == null ? Collections.emptyList() : parent.getChildren(bot);
        }

        @Override
        public String getName() {
            if (name == null) {
                return name = StringFormat.format(name(), "_", " ", StringFormat.FormatStyle.CAMEL_CASE);
            }
            return name;
        }
    }

    @Deprecated
    public enum RS3 implements GameEvent {
        @Deprecated LOGIN_HANDLER,
        @Deprecated LOBBY_HANDLER,
        @Deprecated BANK_PIN,
        @Deprecated INTERFACE_CLOSER,
        @Deprecated UNEXPECTED_ITEM_HANDLER,
        GRIM_REAPERS_OFFICE {
            @Override
            public String getName() {
                return "Grim Reaper's Office";
            }
        },
        BARRICADE;
        private String name;

        RS3() {
            this(null);
        }

        RS3(String name) {
            this.name = name;
        }

        @NonNull
        @Override
        public List<GameEvent> getChildren(AbstractBot bot) {
            final GameEvent parent = ((GameEventControllerImpl) bot.getGameEventController()).getGameEvent(this);
            return parent == null ? Collections.emptyList() : parent.getChildren(bot);
        }

        @Override
        public String getName() {
            if (name == null) {
                return name = StringFormat.format(name(), "_", " ", StringFormat.FormatStyle.CAMEL_CASE);
            }
            return name;
        }
    }

    public enum OSRS implements GameEvent {
        @Deprecated BANK_PIN,
        @Deprecated INTERFACE_CLOSER,
        @Deprecated LOBBY_HANDLER,
        @Deprecated LOGIN_HANDLER,
        NPC_DISMISSER;
        private String name;

        OSRS() {
            this(null);
        }

        OSRS(String name) {
            this.name = name;
        }

        @NonNull
        @Override
        public List<GameEvent> getChildren(AbstractBot bot) {
            final GameEvent parent = ((GameEventControllerImpl) bot.getGameEventController()).getGameEvent(this);
            return parent == null ? Collections.emptyList() : parent.getChildren(bot);
        }

        @Override
        public String getName() {
            if (name == null) {
                return name = StringFormat.format(name(), "_", " ", StringFormat.FormatStyle.CAMEL_CASE);
            }
            return name;
        }
    }

    public interface GameEvent {

        String getName();

        @NonNull
        default List<GameEvent> getChildren() {
            return getChildren(Environment.getBot());
        }

        @NonNull
        default List<GameEvent> getChildren(AbstractBot bot) {
            return Collections.emptyList();
        }

        @Nullable
        default GameEvent getChild(AbstractBot bot, String name) {
            final List<GameEvent> events = getChildren(bot);
            for (final GameEvent event : events) {
                if (event.getName().equals(name)) {
                    return event;
                }
            }
            return null;
        }

        default GameEvent getChild(String name) {
            return getChild(Environment.getBot(), name);
        }

        default boolean isEnabled() {
            return isEnabled(Environment.getBot());
        }

        default boolean isEnabled(AbstractBot active) {
            if (active == null) {
                return false;
            }
            final Boolean value = (Boolean) active.getConfiguration().get("random." + getName().toLowerCase() + ".enabled");
            if (value == null) {
                return true;
            }
            return value;
        }

        default void disable(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".enabled", false);
            }
        }

        default void disable() {
            disable(Environment.getBot());
        }

        default void enable(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".enabled", true);
            }
        }

        default void enable() {
            enable(Environment.getBot());
        }

        default boolean areTrayNotificationsEnabled() {
            return areTrayNotificationsEnabled(Environment.getBot());
        }

        default boolean areTrayNotificationsEnabled(AbstractBot active) {
            if (active == null) {
                return true;
            }
            final Boolean value = (Boolean) active.getConfiguration().get("random." + getName().toLowerCase() + ".tray_notifications");
            if (value == null) {
                return true;
            }
            return value;
        }

        default void disableTrayNotifications() {
            disableTrayNotifications(Environment.getBot());
        }

        default void disableTrayNotifications(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".tray_notifications", false);
            }
        }

        default void enableTrayNotifications() {
            enableTrayNotifications(Environment.getBot());
        }

        default void enableTrayNotifications(AbstractBot active) {
            if (active != null) {
                active.getConfiguration().put("random." + getName().toLowerCase() + ".tray_notifications", true);
            }
        }
    }

    public static class LoginManager {

        static final FailedLoginHandler DEFAULT_FAILED_LOGIN_HANDLER = new FailedLoginHandler() {
            @Override
            public void handle(Fail fail, int occurrencesThusFar) {
                AbstractBot bot = Environment.getBot();
                if (fail == null) {
                    ClientUI.showAlert("A login handler failure occurred but was classified as 'null'");
                    bot.stop("Login failed");
                    return;
                }
                if (fail == Fail.AUTHENTICATOR) {
                    ClientUI.showAlert("Please enter your 6 digit authenticator code " + "to continue the login process.");
                    bot.pause("You need to ever your 6 digit authenticator code " + "to continue the login process.");
                    return;
                }
                if (fail == Fail.SESSION_ENDED || fail == Fail.RUNESCAPE_UPDATED) {
                    Environment.restart();
                } else if (occurrencesThusFar < fail.getAllowedRetries() && (fail == Fail.LOGIN_LIMIT_EXCEEDED
                    || fail == Fail.ALREADY_LOGGED_IN
                    || fail == Fail.CONNECTION_ERROR)) {
                    int secondsUntilRetry = (int) Random.nextGaussian(15, 90, 40);
                    ClientUI.sendTrayNotification(fail.getDescription() + " - trying again in " + secondsUntilRetry + " seconds.");
                    Execution.delay(TimeUnit.SECONDS.toMillis(secondsUntilRetry));
                } else if (occurrencesThusFar >= fail.getAllowedRetries()) {
                    ClientUI.showAlert(fail.getDescription());
                    bot.stop(fail.getDescription());
                }
            }
        };

        @Deprecated
        public static void setCredentials(String username, String password, String pin) {
            OpenAccountDetails.setCredentials(username, password, pin);
        }

        public static FailedLoginHandler getFailedLoginHandler() {
            final AbstractBot active = Environment.getBot();
            FailedLoginHandler handler = null;
            if (active != null) {
                handler = (FailedLoginHandler) active.getConfiguration().get("random.failed_login_handler");
            }
            return handler == null ? DEFAULT_FAILED_LOGIN_HANDLER : handler;
        }

        public static void setFailedLoginHandler(FailedLoginHandler failedLoginHandler) {
            setFailedLoginHandler(Environment.getBot(), failedLoginHandler);
        }

        public static void setFailedLoginHandler(AbstractBot active, FailedLoginHandler failedLoginHandler) {
            if (active != null) {
                active.getConfiguration().put("random.failed_login_handler", failedLoginHandler);
            }
        }

        public enum Fail {
            JAG_BLOCKED("Login blocked: Blocked by JAG"),
            AUTHENTICATOR("Login blocked: Authenticator required"),
            BAN("Login blocked: Account temporary or permanent ban"),
            GENERIC("Login failed: Too many failed attempts", 3),
            SESSION_ENDED("Session Ended"),
            CONNECTION_ERROR("Login blocked: Couldn't connect to server", 4),
            SUSPECTED_STOLEN("Login blocked: Account suspected stolen"),
            LOGIN_LIMIT_EXCEEDED("Login blocked: Login limit exceeded", 4),
            ALREADY_LOGGED_IN("Login blocked: Already logged in", 3),
            RUNESCAPE_UPDATED("Login blocked: RuneScape has updated"),
            INVALID_CREDENTIALS("Login blocked: Invalid credentials", 2),
            MISSING_DISPLAY_NAME("Login blocked: Missing display name"),
            DISCONNECTED_FROM_SERVER("Disconnected from server", 5);

            private final String description;
            private final int allowedRetries;

            Fail(String description, int allowedRetries) {
                this.description = description;
                this.allowedRetries = allowedRetries;
            }

            Fail(String description) {
                this(description, 0);
            }

            public String getDescription() {
                return description;
            }

            public int getAllowedRetries() {
                return allowedRetries;
            }
        }
    }
}
