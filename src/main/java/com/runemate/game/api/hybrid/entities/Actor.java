package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.location.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;
import javax.annotation.Nullable;
import org.jetbrains.annotations.*;

/**
 * An in-game character (i.e. Npc, Player).
 * By default all Actors have a backup model of BoundingModel.HUMANOID
 */
public interface Actor extends LocatableEntity, Rotatable, Modeled, Animable, Onymous {
    /**
     * Gets the current frame of the actors animation
     *
     * @return the current frame, otherwise -1
     */
    int getAnimationFrame();

    /**
     * Gets the current frame of the actors stance animation
     *
     * @return the current frame, otherwise -1
     */
    int getStanceFrame();

    /**
     * Gets the current stance animation id
     *
     * @return the current stance animation id, otherwise -1
     */
    int getStanceId();

    /**
     * Gets whether or not the actor is moving across the world graph
     */
    boolean isMoving();

    /**
     * Gets the health gauge which is displayed during combat
     *
     * @return the health gauge, otherwise null
     */
    @Nullable
    CombatGauge getHealthGauge();

    /**
     * Gets the dialogue above an actors head
     *
     * @return The actors current dialogue, otherwise null
     */
    @Nullable
    String getDialogue();

    /**
     * Gets the Actor that this Actor is targeting (talking, attacking, trading, pickpocketing, etc)
     *
     * @return The target, otherwise null
     */
    @Nullable
    Actor getTarget();

    /**
     * Gets a list of the overhead icons currently above the npc.
     */
    @NonNull
    List<OverheadIcon> getOverheadIcons();

    /**
     * Gets the position of the actor according to the server.
     * <p>
     * This value is typically ahead of where the client renders.
     */
    Coordinate getServerPosition();

    /**
     * Gets a list of the spot animation ids of the entity.
     * A spot animation is a model that is animated simultaneously with the actor's model.
     */
    @NonNull
    List<Integer> getSpotAnimationIds();

    @NonNull
    List<Hitsplat> getHitsplats();

    @NonNull
    List<Coordinate> getPath();
}
