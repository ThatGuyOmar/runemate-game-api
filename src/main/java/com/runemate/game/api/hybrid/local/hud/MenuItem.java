package com.runemate.game.api.hybrid.local.hud;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@Getter
@ToString(of = { "index", "action", "target", "opcode", "arg0", "arg1", "arg2" })
@EqualsAndHashCode(of = { "index", "action", "target", "opcode", "arg0", "arg1", "arg2" })
public final class MenuItem implements Interactable, Renderable {

    private static final Interactable FAILED_TO_RESOLVE_TARGET_ENTITY = new Interactable() {
        @Override
        public boolean isVisible() {
            return false;
        }

        @Override
        public double getVisibility() {
            return 0;
        }

        @Override
        public boolean hasDynamicBounds() {
            return false;
        }

        @Override
        public InteractablePoint getInteractionPoint(Point origin) {
            return null;
        }

        @Override
        public boolean contains(Point point) {
            return false;
        }

        @Override
        public boolean click() {
            return false;
        }

        @Override
        public boolean interact(Pattern action, Pattern target) {
            return false;
        }

        @Override
        public String toString() {
            return "UNRESOLVED";
        }
    };
    private static final Set<Integer> OSRS_UNSUPPORTED_OPCODES = ConcurrentHashMap.newKeySet();
    private static final int HEADER_HEIGHT = 19;

    @NonNull
    private final String action;
    @Nullable
    private final String target;
    private final int index, opcode, arg0, arg1, identifier;

    private final @Deprecated int arg2;

    @Nullable
    @Getter(lazy = true)
    private final Interactable targetEntity = resolveTargetEntity();

    @Nullable
    @Getter(lazy = true)
    private final Type targetType = Type.getByOpcode(opcode);

    @SuppressWarnings("NullableProblems")
    public MenuItem(int index, String action, String target, int opcode, int identifier, int arg0, int arg1) {
        this.index = index;
        this.target = JagTags.remove(target);
        this.opcode = opcode;
        this.identifier = identifier;
        this.arg0 = arg0;
        this.arg1 = arg1;
        this.arg2 = identifier;

        //This can happen occasionally, since 'action' should never be null we will just give it an empty value
        action = JagTags.remove(action);
        if (action == null) {
            log.debug("MenuItem has null 'action': {}", this);
            this.action = "";
        } else {
            this.action = action;
        }
    }

    @Deprecated
    public MenuItem(int index, String action, String target, int opcode, int identifier, int arg0, int arg1, boolean rs3) {
        this(index, action, target, opcode, identifier, arg0, arg1);
    }

    public MenuItem(final OpenMenuItem item) {
        this(item.getIndex(), item.getAction(), item.getTarget(), item.getOpcode(), item.getArg0(), item.getArg1(), item.getArg2());
    }

    public InteractableRectangle getBounds() {
        return getBounds(Menu.getItems());
    }

    private InteractableRectangle getBounds(List<MenuItem> items) {
        final int index = items.indexOf(this);
        if (index != -1) {
            int menuItemCount = OpenMenu.getSize();
            if (menuItemCount > 0) {
                int itemHeight = (OpenMenu.getHeight() - HEADER_HEIGHT) / menuItemCount;
                int itemWidth = OpenMenu.getWidth();
                if (itemWidth > 0 && itemHeight > 1) {
                    return new InteractableRectangle(OpenMenu.getX(), OpenMenu.getY() + HEADER_HEIGHT + (itemHeight * index), itemWidth,
                        itemHeight - 1
                    );
                }
            }
        }
        return null;
    }

    @Override
    public void render(final Graphics2D g2d) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            g2d.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        final Rectangle bounds = getBounds();
        if (bounds != null) {
            gc.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
        }
    }

    private synchronized Interactable resolveTargetEntity() {
        if (opcode != -1 && !OSRS_UNSUPPORTED_OPCODES.contains(opcode)) {
            Type type = Type.getByOpcode(opcode);
            if (type == null) {
                OSRS_UNSUPPORTED_OPCODES.add(opcode);
                log.warn("Unsupported menu opcode {} ({})", opcode, this);
                ClientAlarms.onClientWarn(
                    "Unsupported Menu Opcode",
                    "Revision=" + OpenClient.getRevision() + ", opcode=" + opcode + ", action=" + action + ", target=" + target + ", arg0="
                        + arg0 + ", arg1= " + arg1 + ", arg2=" + arg2
                );
            } else {
                try {
                    Interactable resolved = type.resolve(opcode, identifier, arg0, arg1);
                    if (resolved != null) {
                        return resolved;
                    }
                } catch (Exception e) {
                    log.warn("Failed to resolve target entity of type {} {}", type, this, e);
                    OSRS_UNSUPPORTED_OPCODES.add(opcode);
                    ClientAlarms.onClientWarn(
                        "Error resolving menu target",
                        "Revision=" + OpenClient.getRevision() + ", opcode=" + opcode + ", action=" + action + ", target=" + target
                            + ", arg0=" + arg0 + ", arg1= " + arg1 + ", arg2=" + arg2
                    );
                }
            }
        }
        return FAILED_TO_RESOLVE_TARGET_ENTITY;
    }

    public boolean targets(Interactable desiredTarget) {
        if (desiredTarget == null) {
            return false;
        }
        if (getTargetType() == Type.CANCEL) { //Cancel can never have a target
            return false;
        }

        final Interactable actualTarget = getTargetEntity();
        if (actualTarget == FAILED_TO_RESOLVE_TARGET_ENTITY) {
            //Rather be safe than sorry...
            log.debug("Failed to resolve target entity of {}", this);
            return true;
        } else if (actualTarget instanceof InteractablePoint && desiredTarget instanceof Coordinate) {
            return desiredTarget.contains((InteractablePoint) actualTarget);
        } else if (actualTarget instanceof InterfaceComponent) {
            if (desiredTarget instanceof InterfaceComponent) {
                if (Objects.equals(actualTarget, desiredTarget)) {
                    return true;
                } else {
                    Rectangle entBounds = ((InterfaceComponent) actualTarget).getBounds();
                    if (entBounds == null) {
                        return false;
                    }
                    Rectangle intBounds = ((InterfaceComponent) desiredTarget).getBounds();
                    if (intBounds == null) {
                        return false;
                    }
                    return entBounds.equals(intBounds) || entBounds.contains(intBounds) || intBounds.contains(entBounds);
                }
            } else if (desiredTarget instanceof SpriteItem) {
                int id = ((SpriteItem) desiredTarget).getId();
                if (Objects.equals(id, ((InterfaceComponent) actualTarget).getContainedItemId())) {
                    return true;
                }
                return Parallelize.anyMatch(((InterfaceComponent) actualTarget).getChildren(), ic -> ic.getContainedItemId() == id);
            } else {
                log.debug("Desired target was {} but actual target was {}", desiredTarget, actualTarget);
            }
        } else {
            if (Objects.equals(actualTarget, desiredTarget)) {
                return true;
            } else if (Environment.isVerbose() && actualTarget.getClass().equals(desiredTarget.getClass())) {
                log.debug(
                    "Bad equals on type " + actualTarget.getClass().getSimpleName() + " if identical objects (desired: " + desiredTarget
                        + ", actual: " + actualTarget + ").");
            }
        }
        return false;
    }

    public boolean targets(Interactable entity, Pattern action, Pattern target) {
        if (entity != null && !targets(entity)) {
            return false;
        }
        return (action == null || action.matcher(this.action).matches())
            && (target == null || this.target == null || target.matcher(this.target).matches());
    }

    @Override
    public double getVisibility() {
        return isVisible() ? 100 : 0;
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @Override
    public boolean click() {
        return click(null);
    }

    @Override
    public boolean isVisible() {
        return index != -1 && (index == 0 || OpenMenu.isOpen());
    }

    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        final InteractableRectangle bounds = getBounds();
        return bounds != null ? bounds.getInteractionPoint(origin) : null;
    }


    @Override
    public boolean contains(Point point) {
        List<MenuItem> items = Menu.getItems();
        final int index = items.indexOf(this);
        if (index == -1) {
            return false;
        }
        boolean menuOpen = OpenMenu.isOpen();
        if (index == 0 && !menuOpen) {
            return true;
        }
        if (menuOpen) {
            Rectangle bounds = getBounds(items);
            return bounds != null && bounds.contains(point);
        }
        return false;
    }

    public boolean click(Interactable interactable) {
        final int index = getIndex();
        if (index == -1) {
            return false;
        }
        if (interactable != null && !targets(interactable)) {
            return false;
        }
        if (!OpenMenu.isOpen() && index == 0 && !Mouse.isMenuInteractionForced()) {
            return Mouse.click(Mouse.Button.LEFT);
        }
        if (OpenMenu.isOpen() || Menu.open()) {
            return Mouse.click(this, Mouse.Button.LEFT);
        }
        return false;
    }


    @Override
    public boolean interact(final Pattern action, final Pattern target) {
        if (targets(null, action, target)) {
            return click();
        } else {
            final int size = OpenMenu.getSize();
            if (size == 1 && OpenMenu.isOpen()) {
                int height = OpenMenu.getHeight();
                if (height > 37) {
                    log.warn("Closing the menu in response to a bug in the game where the menu is enlarged to a height of " + height
                        + " yet only contains the Cancel option.");
                    if (!Menu.close()) {
                        log.warn("Failed to close the menu in response to the games enlarged menu bug.");
                    } else {
                        return interact(action, target);
                    }
                }
            }
        }
        return false;
    }

    public enum Type {
        GAME_OBJECT(
            new int[] { 1, 2, 3, 4, 5, 6, 1001, 1002 },
            //osrs identifier is object id, arg0 is region x, arg1 is region y
            (opcode, identifier, arg0, arg1) -> {
                Coordinate base = Region.getBase();
                if (base == null) {
                    return null;
                }
                return GameObjects.getLoadedOn(base.derive(arg0, arg1), obj -> {
                    long id = identifier;
                    GameObjectDefinition def = obj.getDefinition();
                    if (def == null) {
                        return false;
                    } else if (def.getId() == id) {
                        return true;
                    }
                    GameObjectDefinition local = def.getLocalState();
                    return local != null && local.getId() == id;
                }).first();
            }
        ),
        NPC(
            new int[] { 7, 8, 9, 10, 11, 12, 13, 1003, 2009, 2010, 2011, 2012, 2013, 3003 },
            //identifier is uid/index
            (opcode, identifier, arg0, arg1) -> Npcs.getAt(identifier.intValue())
        ), //If it's > 2000, then it's attack
        PLAYER(
            new int[] {
                14, 15, 44, 45, 46, 47, 48, 49, 50, 51, 2044, 2045, 2046, 2047, 2048, 2049, 2051
            },
            //identifier is uid/index
            //arg0 is flagX
            //arg1 is flagY
            //14 = use selected item on player
            //15 = use selected spell on player
            (opcode, identifier, arg0, arg1) -> Players.getAt(identifier.intValue() & 2047)
        ),
        GROUND_ITEM(
            new int[] { 16, 17, 18, 19, 20, 21, 22, 1004 },
            //identifier is item id, arg0 is region x, arg1 is region y
            (opcode, identifier, arg0, arg1) -> {
                var base = Region.getBase();
                return GroundItems.getLoadedOn(base.derive(arg0, arg1), identifier.intValue()).first();
            }
        ),
        WALK_HERE(
            new int[] { 23 },
            //osrs arg0 is mouse x, arg1 is mouse y
            (opcode, identifier, arg0, arg1) -> new InteractablePoint(arg0, arg1)
        ), //Specialized opcodes for buttons
        INTERFACE_BUTTON(
            new int[] { 24, 25, 26, 28, 29, 30 },
            //26 uses none
            //arg1=id for opcodes 24, 28, 29
            //arg1=id, identifier=descendant || -1 for opcodes 25, 30
            (opcode, identifier, arg0, arg1) ->
                opcode == 24 || opcode == 26 || opcode == 28 || opcode == 29 || arg0 == -1 
                    ? OSRSInterfaces.getAt(arg1 >> 16, arg1 & 0xffff)
                    : OSRSInterfaces.getAt(arg1 >> 16, arg1 & 0xffff, arg0)
        ),
        SPRITE_GRID(
            new int[] { 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 1005 },
            (opcode, identifier, arg0, arg1) -> {
                if (opcode == 31) {
                    //use item on item
                    //selected item = source item
                    //identifier = target item id
                    //arg0 = target item table index
                    //arg1 = target item table interface id
                } else if (opcode == 32) {
                    //use spell on item
                    //selected spell = source spell
                    //identifier = target item id
                    //arg0 = target item table index
                    //arg1 = target item table interface id
                } else if (opcode == 38) {
                    //select item (+ deselects spell so perhaps it's the target of opcode 31 & 32)
                    //selected item id = arg0
                    //selected item index = arg1
                    //selected item name = def(selected id).name
                    //interface id = arg2
                } else if (opcode == 1005) {
                    //examine item
                    //interface id = arg2
                    //interface element index = arg1
                    //item id = arg0
                } else if (opcode >= 33 && opcode <= 37) {
                    //item inventory action
                    //identifier = target item id
                    //arg0 = target item table index
                    //arg1 = target item table interface id
                } else if (opcode >= 39 && opcode <= 43) {
                    //item table action
                    //identifier = target item id
                    //arg0 = target item table index
                    //arg1 = target item table interface id
                }
                return Inventory.getItemIn(arg0);
            }
        ),
        INTERFACE(
            new int[] { 57, 58, 1007 },
            //opcode = 57 or 1007, arg0 = option_index
            //action_enabled = (cfg >> 1 + option_index & 1) != 0)
            //arg1 is interface uid, arg0 is child id if not -1
            (opcode, identifier, arg0, arg1) -> arg0 == -1
                ? OSRSInterfaces.getAt(arg1 >> 16, arg1 & 0xffff)
                : OSRSInterfaces.getAt(arg1 >> 16, arg1 & 0xffff, arg0)
        ),
        COORDINATE(new int[] { 59 }, (opcode, identifier, arg0, arg1) -> null),
        CANCEL(
            new int[] { 1006 },
            //No interactable for a type like this
            (opcode, identifier, arg0, arg1) -> null
        ),
        WORLD_MAP_ICON(
            new int[] { 1008 },
            //not supported but has known type
            (opcode, identifier, arg0, arg1) -> null
        );

        private final int[] opcodes;
        private final QuadFunction<Integer, Integer, Integer, Integer, Interactable> resolve;

        Type(int[] opcodes, QuadFunction<Integer, Integer, Integer, Integer, Interactable> resolve) {
            this.opcodes = opcodes;
            Arrays.sort(this.opcodes);
            this.resolve = resolve;
        }

        @Nullable
        public static Type getByOpcode(int opcode) {
            if (opcode != -1) {
                for (Type item : values()) {
                    if (Arrays.binarySearch(item.opcodes, opcode) >= 0) {
                        return item;
                    }
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return StringFormat.format(name(), "_", " ", StringFormat.FormatStyle.CAMEL_CASE);
        }

        @Nullable
        public Interactable resolve(int opcode, int identifier, int param0, int param1) {
            if (opcode == -1) {
                return null;
            }
            return resolve.apply(opcode, identifier, param0, param1);
        }
    }
}
