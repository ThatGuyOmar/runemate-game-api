package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.*;

public final class Sort {
    private Sort() {
    }

    public static <T extends Locatable> List<T> byDistance(final Collection<T> locatables) {
        return byDistanceFrom(Players.getLocal(), locatables);
    }

    public static <T extends Locatable> List<T> byDistance(
        final Collection<T> locatables,
        Distance.Algorithm algorithm
    ) {
        return byDistanceFrom(Players.getLocal(), locatables, algorithm);
    }

    /**
     * Sorts the given Collection by the distance from center with the nearest Locatable being first in the list.
     * If the center is null, a list will be returned in the same order that it was passed
     * If the distance can't be calculated then the locatable will be placed at the end of the list.
     */
    public static <T extends Locatable> List<T> byDistanceFrom(
        final Locatable center,
        final Collection<T> locatables
    ) {
        return byDistanceFrom(center, locatables, Distance.Algorithm.MANHATTAN);
    }

    /**
     * Sorts the given Collection by the distance from center using the specified algorithm with the nearest Locatable being first in the list.
     * If the center is null, a list will be returned in the same order that it was passed
     * If the distance can't be calculated then the locatable will be placed at the end of the list.
     */
    public static <T extends Locatable> List<T> byDistanceFrom(
        final Locatable center,
        final Collection<T> locatables,
        Distance.Algorithm algorithm
    ) {
        final List<T> list = new ArrayList<>(locatables);
        if (center != null) {
            final Coordinate centerPos = center.getPosition();
            final HashMap<T, Double> precomputed = new HashMap<>(Math.max(locatables.size(), 16));
            final HashMap<String, Object> cache = new HashMap<>();
            for (final T locatable : locatables) {
                precomputed.put(
                    locatable,
                    Distance.between(centerPos, locatable, algorithm, cache)
                );
            }
            list.sort((one, two) -> {
                Double d1 = precomputed.get(one);
                Double d2 = precomputed.get(two);
                if (d1 == null || d2 == null) {
                    throw new IllegalStateException("Inconsistent hashcode function for " +
                        (d1 == null ? one : two).getClass().getName());
                }
                return Double.compare(d1, d2);
            });
        }
        return list;
    }
}
