package com.runemate.game.api.hybrid.location;

import com.google.common.hash.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.Menu;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.projection.*;
import com.runemate.game.incubating.util.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.Queue;
import java.util.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import lombok.*;
import javax.annotation.Nullable;
import lombok.extern.log4j.*;

/**
 * A position on the world-graph, also known as a Tile
 */
@Log4j2
public class Coordinate implements Serializable, Locatable, Interactable, Renderable {
    private static final int DATA_ORBS_VARBIT = 4084, DATA_ORBS_ENABLED_VALUE = 0;
    private final int x, y, plane;

    public Coordinate(int x, int y, int plane) {
        this.x = x;
        this.y = y;
        this.plane = plane;
    }

    public Coordinate(int uid) {
        this.x = uid & 0x3fff;
        this.y = uid >> 14 & 0x3fff;
        this.plane = uid >> 28 & 0x3;
    }

    /**
     * Previously a utility constructor for (x, y, 0), however developers got confused by underground areas
     */
    @Deprecated
    public Coordinate(final int x, final int y) {
        this(x, y, 0);
    }

    private static boolean hasFlags(int flags, int[] flagOffsets) {
        for (int offset : flagOffsets) {
            if (((flags >> offset) & 1) == 1) {
                return true;
            }
        }
        return false;
    }

    private static InteractablePoint getDesiredInteractionPoint(
        Point origin, int minX, int minY, int maxX, int maxY
    ) {
        double unroundedX = Random.nextGaussian(minX, maxX);
        double unroundedY = Random.nextGaussian(minY, maxY);
        long roundedX = Math.round(unroundedX);
        long roundedY = Math.round(unroundedY);
        int castedX = (int) roundedX;
        int castedY = (int) roundedY;
        return new InteractablePoint(castedX, castedY);
    }

    public int getContainingRegionId() {
        return (this.y / 64) | ((this.x / 64) << 8);
    }

    public boolean contains(Point point, Shape viewport) {
        final Polygon bounds = getBounds(viewport);
        return bounds != null && bounds.contains(point);
    }

    public boolean contains(Point point, Shape viewport, Coordinate regionBase) {
        final Polygon bounds = getBounds(viewport, regionBase);
        return bounds != null && bounds.contains(point);
    }

    /*@Override
    public boolean isHovered() {
        if (Environment.isOSRS()) {
            return Objects.equals(this, Region.getHoveredCoordinate());
        }
        return Interactable.super.isHovered();
    }*/

    /**
     * Returns a new Coordinate translated by the x-offset and y-offset provided
     *
     * @param xOffset Number of x-tiles to derive Coordinate by
     * @param yOffset Number of y-tiles to derive Coordinate by
     * @return a new Coordinate offset by x/y from the current Coordinate
     */
    public Coordinate derive(final int xOffset, final int yOffset) {
        return derive(xOffset, yOffset, 0);
    }

    /**
     * Returns a new Coordinate translated by the x-offset and y-offset provided
     *
     * @param xOffset     Number of x-tiles to derive Coordinate by
     * @param yOffset     Number of y-tiles to derive Coordinate by
     * @param planeOffset Number of planes to move up/down
     * @return a new Coordinate offset by x/y from the current Coordinate
     */
    public Coordinate derive(final int xOffset, final int yOffset, final int planeOffset) {
        if (xOffset == 0 && yOffset == 0 && planeOffset == 0) {
            return this;
        }
        return new Coordinate(getX() + xOffset, getY() + yOffset, getPlane() + planeOffset);
    }

    public Coordinate getGroundFloor() {
        return new Coordinate(getX(), getY(), 0);
    }

    public Coordinate getFirstFloor() {
        return new Coordinate(getX(), getY(), 1);
    }

    public Coordinate getSecondFloor() {
        return new Coordinate(getX(), getY(), 2);
    }

    public Coordinate getTopFloor() {
        return new Coordinate(getX(), getY(), 3);
    }

    public Coordinate derive(final Transform<Coordinate> transform) {
        return transform.transform(new Coordinate(getX(), getY(), getPlane()));
    }

    @Nullable
    public Polygon getBounds() {
        return getBounds(Projection.getViewport());
    }

    @Nullable
    public Polygon getBounds(Shape viewport) {
        return getBounds(viewport, Region.getBase());
    }

    @Nullable
    public Polygon getBounds(Shape viewport, Coordinate regionBase) {
        return getBounds(viewport, regionBase, new HashMap<>());
    }

    @Nullable
    public Polygon getBounds(Shape viewport, Coordinate regionBase, Map<String, Object> cache) {
        if (getPlane() != regionBase.getPlane()) {
            return null;
        }
        HighPrecision[] hp_offsets = {
            offset(regionBase).getHighPrecision(),
            derive(0, 1).offset(regionBase).getHighPrecision(),
            derive(1, 1).offset(regionBase).getHighPrecision(),
            derive(1, 0).offset(regionBase).getHighPrecision()
        };
        Point[] vertices = Projection.multiAbsoluteToScreen(
            0,
            new int[] {
                hp_offsets[0].getX(),
                hp_offsets[1].getX(),
                hp_offsets[2].getX(),
                hp_offsets[3].getX()
            },
            new int[] { 0, 0, 0, 0 },
            new int[] {
                hp_offsets[0].getY(),
                hp_offsets[1].getY(),
                hp_offsets[2].getY(),
                hp_offsets[3].getY()
            },
            cache
        );
        if (vertices == null) {
            return null;
        }
        List<Point> vertices_in_viewport = new ArrayList<>(4);
        for (Point vertex : vertices) {
            if (vertex != null && (viewport == null || viewport.contains(vertex))) {
                vertices_in_viewport.add(vertex);
            }
        }
        if (vertices_in_viewport.size() < 3) {
            return null;
        }
        final int[] xs = new int[vertices_in_viewport.size()];
        final int[] ys = new int[vertices_in_viewport.size()];
        for (int index = 0; index < vertices_in_viewport.size(); ++index) {
            Point current = vertices_in_viewport.get(index);
            xs[index] = current.x;
            ys[index] = current.y;
        }
        return new Polygon(xs, ys, vertices_in_viewport.size());
    }

    /**
     * An inefficient helper method for getting the collision flag at a single coordinate.
     * Please use Region#getCollisionFlags for bulk analysis
     *
     * @see com.runemate.game.api.hybrid.region.Region#getCollisionFlags()
     */
    public int getCollisionFlag() {
        RegionOffset offset = offset();
        if (!offset.isValid()) {
            return -1;
        }
        int[][] flags = Region.getCollisionFlags(getPlane());
        if (flags == null) {
            return -1;
        }
        return flags[offset.getX()][offset.getY()];
    }

    /**
     * Gets the height of the landscape at this Coordinate.
     *
     * @return an int value representing height, or -1 if the coordinate isn't loaded.
     */
    public int getHeight() {
        Coordinate.RegionOffset offset = offset();
        if (!offset.isValid()) {
            return -1;
        }
        return Region.getHeights()[getPlane()][offset.getX()][offset.getY()];
    }

    /**
     * Gets the render flag of the landscape at this Coordinate.
     *
     * @return an int value representing the render flag, or -1 if the coordinate isn't loaded.
     */
    public int getRenderFlag() {
        Coordinate.RegionOffset offset = offset();
        if (!offset.isValid()) {
            return -1;
        }
        return Region.getRenderFlags()[getPlane()][offset.getX()][offset.getY()];
    }

    /**
     * Gets the z coordinate, also known as getLevel or getFloor
     *
     * @return an int between (0, 3)
     */
    public int getPlane() {
        return plane;
    }

    @Nullable
    @Override
    public Coordinate getPosition() {
        return this;
    }

    @NonNull
    @Override
    public HighPrecision getHighPrecisionPosition() {
        int plane = getPlane();
        return new HighPrecision(getX() << 7, getY() << 7, plane);
    }

    @NonNull
    @Override
    public Area.Rectangular getArea() {
        return new Area.Rectangular(this);
    }

    /**
     * Calculates which coordinates within the loaded region are reachable from this Coordinate.
     */
    public Set<Coordinate> getReachableCoordinates() {
        return getReachableCoordinates(Region.getCollisionFlags(getPlane()));
    }

    public Set<Coordinate> getReachableCoordinates(int[][] collisionFlags) {
        final Set<Coordinate> results = new HashSet<>(256);
        if (collisionFlags != null) {
            final Coordinate start = this;
            final Queue<Coordinate> queue = new LinkedList<>(Collections.singleton(start));
            final Coordinate regionBase = Region.getBase(0);
            while (!queue.isEmpty()) {
                final Coordinate c = queue.poll();
                final int offsetX = c.getX() - regionBase.getX();
                final int offsetY = c.getY() - regionBase.getY();
                if (offsetX < 0 || offsetX > 104 || offsetY < 0 || offsetY > 104) {
                    continue;
                }
                final int flags = collisionFlags[offsetX][offsetY];
                if (hasFlags(flags, Offsets.O) || results.contains(c)) {
                    if (!c.equals(start)) { //You can walk from the start position even if it's blocked (eg. center of fairy ring)
                        continue;
                    }
                }
                results.add(c);
                if (!hasFlags(flags, Offsets.N)) {
                    queue.offer(c.derive(0, 1));
                }
                if (!hasFlags(flags, Offsets.E)) {
                    queue.offer(c.derive(1, 0));
                }
                if (!hasFlags(flags, Offsets.S)) {
                    queue.offer(c.derive(0, -1));
                }
                if (!hasFlags(flags, Offsets.W)) {
                    queue.offer(c.derive(-1, 0));
                }
            }
        }
        return results;
    }

    /**
     * Calculates which coordinates within the loaded region are reachable from this Coordinate.
     */
    public Set<Coordinate> getReachableCoordinates(int[][][] collisionFlags) {
        return getReachableCoordinates(collisionFlags[getPlane()]);
    }

    public double getVisibility(Shape viewport) {
        Polygon bounds = getBounds(viewport);
        if (bounds == null || bounds.npoints < 3) {
            return 0;
        }
        //if there are only 3 points available then only a triangle can be constructed
        //which is only half the original square
        return bounds.npoints == 4 ? 100 : 50;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        return plane << 28 | y << 14 | x;
    }

    @Override
    public boolean equals(final Object o) {
        if (o instanceof Coordinate) {
            Coordinate other = (Coordinate) o;
            return hashCode() == other.hashCode();
        }
        return false;
    }

    @Override
    public String toString() {
        return "Coordinate(" + x + ", " + y + ", " + plane + ')';
    }

    public boolean isLoaded() {
        int plane = getPlane();
        return offset().isValid() && plane >= 0 && plane < Region.getFloors();
    }

    public boolean isLoaded(Coordinate regionBase) {
        int plane = getPlane();
        return offset(regionBase).isValid() && plane >= 0 && plane < Region.getFloors();
    }

    /**
     * Checks if it's reachable from the local player via isReachableFrom
     */
    public boolean isReachable() {
        return isReachableFrom(Players.getLocal());
    }

    /**
     * Checks if this coordinate is reachable from the given Locatable via a flood fill.
     * If either locatable exists outside of the loaded region the return value can no longer be considered reliable.
     */
    public boolean isReachableFrom(final Locatable locatable) {
        if (locatable != null) {
            final Coordinate locatablePosition = locatable.getPosition();
            int plane = getPlane();
            if (locatablePosition != null && plane == locatablePosition.getPlane()) {
                final int[][] flags = Region.getCollisionFlags(plane);
                if (flags != null) {
                    final Coordinate base = Region.getBase(0);
                    final LinkedHashSet<Coordinate> fifoQueue = new LinkedHashSet<>(1000);
                    final HashSet<Coordinate> visited = new HashSet<>(1000);
                    fifoQueue.add(this);
                    final int xFlagOffset = 0;
                    final int yFlagOffset = 0;
                    while (!fifoQueue.isEmpty()) {
                        final Coordinate current = fifoQueue.iterator().next();
                        if (current.getX() == locatablePosition.getX() &&
                            current.getY() == locatablePosition.getY()) {
                            return true;
                        }
                        fifoQueue.remove(current);
                        visited.add(current);
                        int currentXOffset = current.getX() - base.getX() - xFlagOffset;
                        int currentYOffset = current.getY() - base.getY() - yFlagOffset;
                        if (currentXOffset >= 0 && currentYOffset >= 0 && currentXOffset < 104 &&
                            currentYOffset < 104) {
                            int baseFlag = flags[currentXOffset][currentYOffset];
                            int cardinalFlag;
                            Coordinate cardinal = current.derive(0, 1);
                            int cardinalLocalX = cardinal.getX() - base.getX();
                            int cardinalLocalY = cardinal.getY() - base.getY();
                            if (cardinalLocalX >= 0 && cardinalLocalX < 104 &&
                                cardinalLocalY >= 0 && cardinalLocalY < 104) {
                                cardinalFlag = flags[cardinalLocalX - xFlagOffset][cardinalLocalY -
                                    yFlagOffset];
                                if (CollisionUtils.noNorthernBoundary(baseFlag, cardinalFlag)
                                    && !visited.contains(cardinal)) {
                                    fifoQueue.add(cardinal);
                                }
                            }
                            cardinal = current.derive(0, -1);
                            cardinalLocalX = cardinal.getX() - base.getX();
                            cardinalLocalY = cardinal.getY() - base.getY();
                            if (cardinalLocalX >= 0 && cardinalLocalX < 104 &&
                                cardinalLocalY >= 0 && cardinalLocalY < 104) {
                                cardinalFlag = flags[cardinalLocalX - xFlagOffset][cardinalLocalY -
                                    yFlagOffset];
                                if (CollisionUtils.noSouthernBoundary(baseFlag, cardinalFlag)
                                    && !visited.contains(cardinal)) {
                                    fifoQueue.add(cardinal);
                                }
                            }
                            cardinal = current.derive(1, 0);
                            cardinalLocalX = cardinal.getX() - base.getX();
                            cardinalLocalY = cardinal.getY() - base.getY();
                            if (cardinalLocalX >= 0 && cardinalLocalX < 104 &&
                                cardinalLocalY >= 0 && cardinalLocalY < 104) {
                                cardinalFlag = flags[cardinalLocalX - xFlagOffset][cardinalLocalY -
                                    yFlagOffset];
                                if (CollisionUtils.noEasternBoundary(baseFlag, cardinalFlag)
                                    && !visited.contains(cardinal)) {
                                    fifoQueue.add(cardinal);
                                }
                            }
                            cardinal = current.derive(-1, 0);
                            cardinalLocalX = cardinal.getX() - base.getX();
                            cardinalLocalY = cardinal.getY() - base.getY();
                            if (cardinalLocalX >= 0 && cardinalLocalX < 104 &&
                                cardinalLocalY >= 0 && cardinalLocalY < 104) {
                                cardinalFlag = flags[cardinalLocalX - xFlagOffset][cardinalLocalY -
                                    yFlagOffset];
                                if (CollisionUtils.noWesternBoundary(baseFlag, cardinalFlag)
                                    && !visited.contains(cardinal)) {
                                    fifoQueue.add(cardinal);
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean isVisible() {
        return getBounds() != null;
    }

    public boolean isVisible(Shape viewport) {
        return getBounds(viewport) != null;
    }

    public boolean isVisible(Shape viewport, Coordinate regionBase) {
        return getBounds(viewport, regionBase) != null;
    }

    @Nullable
    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        var shape = getBounds();
        if (shape != null) {
            var bounds = shape.getBounds2D();
            for (int i = 0; i < 50; i++) { //maximum of 50 attempts before falling back to legacy point selection
                var point = new InteractablePoint(
                    (int) Random.nextGaussian(bounds.getMinX(), bounds.getMaxX(), bounds.getMinX() + (bounds.getWidth() / 2)),
                    (int) Random.nextGaussian(bounds.getMinY(), bounds.getMaxY(), bounds.getMinY() + (bounds.getHeight() / 2))
                );
                if (shape.contains(point)) {
                    return point;
                }
            }
        }
        return null;
    }


    /**
     * Gets the minimap accessor for this Coordinate
     */
    @NonNull
    public MinimapCoordinate minimap() {
        return new MinimapCoordinate();
    }

    public RegionOffset offset() {
        return offset(Region.getBase(0));
    }

    public RegionOffset offset(Coordinate base) {
        return new RegionOffset(base);
    }

    @Override
    public boolean contains(Point point) {
        final Polygon bounds = getBounds();
        return bounds != null && bounds.contains(point);
    }

    public RegionOffset offset(int baseX, int baseY) {
        return new RegionOffset(baseX, baseY);
    }

    /**
     * Randomizes the coordinate. [x, y]
     *
     * @return A new Coordinate derived from this.
     */
    public Coordinate randomize(final int x, final int y) {
        return derive(Random.nextInt(-x, x + 1), Random.nextInt(-y, y + 1));
    }

    @Override
    public void render(Graphics2D g2d) {
        render(g2d, Projection.getViewport());
    }

    @Override
    public void render(GraphicsContext gc) {
        render(gc, Projection.getViewport());
    }

    public void render(Graphics2D g2d, Shape viewport) {
        render(g2d, viewport, Region.getBase());
    }

    public void render(Graphics2D g2d, Shape viewport, Coordinate regionBase) {
        final Polygon bounds = getBounds(viewport, regionBase);
        if (bounds != null) {
            final Color c = g2d.getColor();
            g2d.setColor(new Color(c.getRed(), c.getGreen(), c.getBlue(), 150));
            g2d.fillPolygon(bounds);
            g2d.setColor(c);
            g2d.drawPolygon(bounds);
        }
        final Point p = minimap().getInteractionPoint();
        if (p != null) {
            g2d.fillRect(p.x - 2, p.y - 2, 4, 4);
        }
    }

    public void render(GraphicsContext gc, Shape viewport) {
        render(gc, viewport, Region.getBase());
    }

    public void render(GraphicsContext gc, Shape viewport, Coordinate regionBase) {
        final Polygon bounds = getBounds(viewport, regionBase);
        if (bounds != null) {
            gc.strokePolygon(Renderable.convert(bounds.xpoints), Renderable.convert(bounds.ypoints),
                bounds.npoints
            );
        }
        final Point p = minimap().getInteractionPoint();
        if (p != null) {
            gc.fillRect(p.x - 2, p.y - 2, 4, 4);
        }
    }

    @Override
    public final boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        return Menu.click(this, action, target);
    }

    @Override
    public double getVisibility() {
        return getVisibility(Projection.getViewport());
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    private interface Offsets {

        /**
         * All north boundary flag offsets
         */
        int[] N = { 1, 10, 23 };

        /**
         * All east boundary flag offsets
         */
        int[] E = { 3, 12, 25 };

        /**
         * All south boundary flag offsets
         */
        int[] S = { 5, 14, 27 };

        /**
         * All west boundary flag offsets
         */
        int[] W = { 7, 16, 29 };

        /**
         * All north-east boundary flag offsets
         */
        int[] NE = { 2, 11, 24 };

        /**
         * All north-west boundary flag offsets
         */
        int[] NW = { 0, 9, 22 };

        /**
         * All south-east boundary flag offsets
         */
        int[] SE = { 4, 13, 26 };

        /**
         * All south-west boundary flag offsets
         */
        int[] SW = { 6, 15, 28 };

        /**
         * All object boundary flag offsets
         */
        int[] O = { 8, 17, 18, 21 };
    }

    public static class HighPrecision {

        private final byte plane;
        private final int x, y;

        public HighPrecision(final int x, final int y, final int plane) {
            if (plane < 0) {
                throw new IllegalArgumentException(
                    "The plane must be greater than or equal to 0 (" + plane +
                        " is not within those bounds)");
            }
            this.x = x;
            this.y = y;
            this.plane = (byte) plane;
        }

        public Coordinate asCoordinate() {
            return new Coordinate(x, y, plane);
        }

        public int getPlane() {
            return plane;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public HighPrecision reducePrecision(final int bits) {
            return new HighPrecision(x >> bits, y >> bits, plane);
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof HighPrecision) {
                HighPrecision hp = (HighPrecision) o;
                return x == hp.x && y == hp.y && plane == hp.plane;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Hashing.goodFastHash(32).newHasher().putInt(x).putInt(y).putByte(plane).hash()
                .asInt();
        }

        @Override
        public String toString() {
            return "Coordinate.HighPrecision(" + x + ", " + y + ", " + plane + ')';
        }
    }

    public final class MinimapCoordinate implements Interactable {

        public boolean isVisible(Area.Rectangular region, Map<String, Object> cache) {
            if (region == null || !region.contains(Coordinate.this) || Environment.isOSRS() && !((boolean) cache.computeIfAbsent("minimap_interactable",
                f -> Minimap.isInteractable()
            ))) {
                return false;
            }
            Player local = (Player) cache.get("LocalPlayer");
            if (local == null) {
                local = Players.getLocal();
                if (local != null) {
                    cache.put("LocalPlayer", local);
                } else {
                    return false;
                }
            }
            final Coordinate center = local.getPosition(region.getBottomLeft());
            if (center == null) {
                return false;
            }
            final int xOff = ((offset(region.getBottomLeft()).getX() - center.offset(region.getBottomLeft()).getX()) << 2) + 2;
            final int yOff = ((offset(region.getBottomLeft()).getY() - center.offset(region.getBottomLeft()).getY()) << 2) + 2;
            if ((xOff * xOff) + (yOff * yOff) <= (58 * 58)) {
                Varbit dataOrbsVarbit = Varbits.load(DATA_ORBS_VARBIT);
                if (dataOrbsVarbit == null) {
                    log.info("The data orbs varbit is null.");
                }
                if (dataOrbsVarbit != null && dataOrbsVarbit.getValue() == DATA_ORBS_ENABLED_VALUE) {
                    //TODO make this accept and use a cache
                    final Point projected = OSRSProjection.coordinateToMinimap(Coordinate.this, cache);
                    if (projected != null) {
                        return getMinimapObscuringRectangles(cache).stream().noneMatch(rectangle -> rectangle.contains(projected));
                    }
                }
                return true;
            }
            return false;
        }

        @SuppressWarnings("unchecked")
        private List<Rectangle> getMinimapObscuringRectangles(Map<String, Object> cache) {
            var minimapOrbBounds = (List<Rectangle>) cache.get("BlockingOrbs");
            if (minimapOrbBounds != null) {
                return minimapOrbBounds;
            }
            final var provider = Interfaces.newQuery()
                .containers(160, InterfaceContainers.getRootIndex())
                .types(InterfaceComponent.Type.SPRITE, InterfaceComponent.Type.LABEL, InterfaceComponent.Type.CONTAINER)
                .results()
                .asList();
            InterfaceComponentQueryResults orbs = Interfaces.newQuery()
                .provider(() -> provider)
                .containers(160, InterfaceContainers.getRootIndex())
                .types(InterfaceComponent.Type.SPRITE, InterfaceComponent.Type.LABEL)
                .actions("Floating", "Look North")
                .results();
            orbs.addAll(Interfaces.newQuery()
                .provider(() -> provider)
                .containers(160)
                .types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(false)
                .widths(49, 50)
                .heights(26)
                .results());
            orbs.addAll(Interfaces.newQuery()
                .provider(() -> provider)
                .containers(160)
                .types(InterfaceComponent.Type.CONTAINER)
                .actions("Open Activity Adviser")
                .results());
            orbs.addAll(Interfaces.newQuery()
                .provider(() -> provider)
                .containers(160)
                .types(InterfaceComponent.Type.CONTAINER)
                .names("Special Attack")
                .results());
            minimapOrbBounds = new ArrayList<>(orbs.size());
            for (InterfaceComponent orb : orbs) {
                Rectangle bounds = orb.getBounds();
                if (bounds != null) {
                    minimapOrbBounds.add(bounds);
                }
            }
            cache.put("BlockingOrbs", minimapOrbBounds);
            return minimapOrbBounds;
        }

        @Override
        public boolean isVisible() {
            Area.Rectangular area = Region.getArea();
            return area != null && area.contains(Coordinate.this) && isVisible(area, new HashMap<>());
        }

        @Override
        public double getVisibility() {
            return isVisible() ? 100 : 0;
        }

        @Override
        public boolean hasDynamicBounds() {
            return true;
        }

        @Nullable
        @Override
        public InteractablePoint getInteractionPoint(Point origin) {
            return Projection.coordinateToMinimap(Coordinate.this);
        }

        @Override
        public boolean contains(Point point) {
            Point projected = Projection.coordinateToMinimap(Coordinate.this);
            return projected != null && point.x == projected.x && point.y == projected.y;
        }

        @Override
        public boolean click() {
            return Mouse.click(this, Mouse.Button.LEFT);
        }

        @Override
        public boolean interact(String action) {
            return click();
        }

        @Override
        public boolean interact(String action, String target) {
            return click();
        }

        @Override
        public boolean interact(Pattern action) {
            return click();
        }

        @Override
        public boolean interact(Pattern action, Pattern target) {
            return click();
        }

        @Override
        public boolean interact(Pattern action, String target) {
            return click();
        }

        @Override
        public boolean interact(String action, Pattern target) {
            return click();
        }

        @Override
        public String toString() {
            return "Coordinate.Minimap(" + getX() + ", " + getY() + ", " + getPlane() + ')';
        }
    }

    public final class RegionOffset implements Validatable {

        private final int baseX, baseY;
        private final int x, y;

        public RegionOffset(Coordinate base) {
            this(base.getX(), base.getY());
        }

        public RegionOffset(int baseX, int baseY) {
            this.x = Coordinate.this.getX() - (this.baseX = baseX);
            this.y = Coordinate.this.getY() - (this.baseY = baseY);
        }

        public RegionOffset derive(int x, int y) {
            return Coordinate.this.derive(x, y).offset(this.baseX, this.baseY);
        }

        public HighPrecision getHighPrecision() {
            return new HighPrecision(this.x << 7, this.y << 7, getPlane());
        }

        public int getX() {
            return this.x;
        }

        public int getY() {
            return this.y;
        }

        @Override
        public boolean isValid() {
            return this.x >= 0 && this.x < Region.getWidth() && this.y >= 0 &&
                this.y < Region.getHeight();
        }

        @Override
        public String toString() {
            return "Coordinate.RegionOffset(" + this.x + ", " + this.y + ')';
        }


    }


}
