package com.runemate.game.api.hybrid.input.direct;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import lombok.*;
import lombok.experimental.*;

@UtilityClass
public class DirectInput {

    /**
     * Sends an action directly to the game, performing no validation as it does, to be processed in the next game cycle.
     * <p>
     * Care should be taken when constructing MenuActions to ensure that they are valid, for example by comparing against "real" actions
     * using the {@link com.runemate.game.api.script.framework.listeners.MenuInteractionListener}. Failure to do so could result in
     * instability in the game client.
     * <p>
     * The resulting red-click visible in-game for entity interactions is purely visual, and does not represent an actual mouse event
     * in-game, and will be drawn in the top-left of the screen if the entity is not visible within the viewport.
     * <br>
     * <br>
     * <p>
     * Examples:
     * <p>
     * <pre>
     *     //Sends the "Drop" action for a SpriteItem
     *     final SpriteItem item = ...;
     *     final var action = MenuAction.forSpriteItem(item, "Drop");
     *     if (action != null) {
     *         DirectInput.send(action);
     *     }
     *
     *     //Sends the "top" action for an InterfaceComponent
     *     final InterfaceComponent component = ...;
     *     final var action = MenuAction.forInterfaceComponent(component, 0);
     *     if (action != null) {
     *         DirectInput.send(action);
     *     }
     * </pre>
     *
     */
    public void send(MenuAction action) {
        if (action == null) {
            return;
        }
        
        var clickX = -1;
        var clickY = -1;
        if (action.getEntity() != null) {
            final var point = action.getEntity().getInteractionPoint();
            if (point != null) {
                clickX = point.x;
                clickY = point.y;
            }
        }
        OpenClient.sendAction(
            action.getParam0(),
            action.getParam1(),
            action.getOpcode(),
            action.getIdentifier(),
            getItemId(action.getIdentifier(), action.getOpcode(), action.getParam0(), action.getParam1(), -1),
            action.getOption(),
            action.getTarget(),
            clickX,
            clickY
        );
    }

    public void sendMovement(Locatable locatable) {
        if (locatable == null) {
            return;
        }

        sendMovement(locatable.getLocalPosition());
    }

    public void sendMovement(Coordinate.RegionOffset offset) {
        if (offset == null) {
            return;
        }

        sendMovement(offset.getX(), offset.getY());
    }

    /**
     * Sends a movement action directly to the client where the parameters are the x-offset and y-offset relative
     * to the scene base (not to be confused with the current map region base).
     * <p>
     * It is almost certainly a better option to use {@link #sendMovement(Locatable)} and allowing the API to do the offset calculations.
     *
     * @see Region#getBase()
     */
    public void sendMovement(int localX, int localY) {
        OpenClient.setDestination(localX, localY);
    }

    /**
     * Directly sets the selected InterfaceComponent for the provided SpriteItem, <b>without verifying that the InterfaceComponent currently
     * contains the item.</b> Be careful when performing subsequent actions, as there is no guarantee the item will still exist once
     * it is selected.
     */
    public boolean setSelectedItem(SpriteItem item) {
        final var component = MenuAction.getSpriteItemComponent(item);
        if (component == null) {
            return false;
        }
        OpenClient.setSelectedItem(component.getId(), item.getIndex(), item.getId());
        return true;
    }

    /**
     * Directly selects the InterfaceComponent for the provided Spell, <b>without verifying that the Spell can actually be cast.</b>
     */
    public boolean setSelectedSpell(Spell spell) {
        final var component = spell.getComponent();
        if (component == null) {
            return false;
        }
        OpenClient.setSelectedSpell(component.getId());
        return true;
    }

    private int getItemId(int identifier, int opcode, int param0, int param1, int currentItemId) {
        switch (opcode) {
            case 1006:
                currentItemId = 0;
                break;
            case 25:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 58:
            case 1005:
                currentItemId = getItemId(param0, param1, currentItemId);
                break;

            case 57:
            case 1007:
                if (identifier >= 1 && identifier <= 10) {
                    currentItemId = getItemId(param0, param1, currentItemId);
                }

                break;
        }

        return currentItemId;
    }

    private int getItemId(int param0, int param1, int currentItemId) {
        int container = param1 >>> 16, componentId = param1 & 0xFFFF;
        InterfaceComponent component = Interfaces.getAt(container, componentId);
        if (component != null) {
            var children = component.getChildren();
            if (children != null && children.size() >= 2 && container == 387) {
                param0 = 1;
            }

            var child = param0 == -1 ? component : component.getChild(param0);
            if (child != null) {
                if (currentItemId != child.getContainedItemId()) {
                    return child.getContainedItemId();
                }
            }
        }

        return currentItemId;
    }

    /**
     * Utility method that combines {@link #setSelectedItem(SpriteItem)} and then {@link #send(MenuAction)}.
     * <p>
     * Can only be used on SpriteItems where the SpriteItem.Origin is {@link SpriteItem.Origin#INVENTORY}
     */
    public void sendItemUseOn(@NonNull SpriteItem item, @NonNull Interactable target) {
        if (item.getOrigin() != SpriteItem.Origin.INVENTORY) {
            throw new UnsupportedOperationException("can only 'Use' items in inventory");
        }

        var action = componentTarget(target);
        if (action == null) {
            return;
        }

        if (setSelectedItem(item)) {
            send(action);
        }
    }

    /**
     * Utility method that combines {@link #setSelectedSpell(Spell)} and then {@link #send(MenuAction)}.
     */
    public void sendSpellCastOn(@NonNull Spell spell, @NonNull Interactable target) {
        var action = componentTarget(target);
        if (action == null) {
            return;
        }

        if (setSelectedSpell(spell)) {
            send(action);
        }
    }

    private MenuAction componentTarget(Interactable target) {
        MenuAction action;
        if (target instanceof InterfaceComponent) {
            action = MenuAction.forInterfaceComponent((InterfaceComponent) target, 0, MenuOpcode.COMPONENT_TARGET_ON_COMPONENT);
        } else if (target instanceof GameObject) {
            action = MenuAction.forGameObject((GameObject) target, MenuOpcode.COMPONENT_TARGET_ON_GAME_OBJECT);
        } else if (target instanceof GroundItem) {
            action = MenuAction.forGroundItem((GroundItem) target, MenuOpcode.COMPONENT_TARGET_ON_GROUND_ITEM);
        } else if (target instanceof Player) {
            action = MenuAction.forPlayer((Player) target, MenuOpcode.COMPONENT_TARGET_ON_PLAYER);
        } else if (target instanceof Npc) {
            action = MenuAction.forNpc((Npc) target, MenuOpcode.COMPONENT_TARGET_ON_NPC);
        } else if (target instanceof SpriteItem) {
            action = MenuAction.forSpriteItem((SpriteItem) target, 0, MenuOpcode.COMPONENT_TARGET_ON_COMPONENT);
        } else {
            throw new IllegalArgumentException("Invalid target type " + target.getClass().getSimpleName());
        }
        return action;
    }
    
    
}
