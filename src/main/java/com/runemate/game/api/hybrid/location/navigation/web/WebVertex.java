package com.runemate.game.api.hybrid.location.navigation.web;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import lombok.*;
import org.jetbrains.annotations.*;

/**
 * A vertex for use in a Web
 */
public abstract class WebVertex implements SerializableVertex, Locatable, Usable {

    public final static String PREVIOUS = "Previous",
        STEPS = "Steps",
        AVATAR = "LocalPlayer",
        REGION = "Region",
        REACHABLE = "Reachable",
        REGION_BASE = "RegionBase",
        AVATAR_POS = "LocalPosition",
        INDEX_IN_STEPS = "IndexInSteps",
        NPC_CACHE = "NpcCache",
        GAMEOBJECT_CACHE = "GameObjectCache",
        ITEM_CACHE = "ItemCache",
        PREFERS_VIEWPORT = "PrefersViewport";
    protected Coordinate position;
    protected Collection<WebRequirement> mustFulfillAnyOfRequirements,
        mustFulfillNoneOfRequirements;
    protected int cachedHashcode = -1;
    private Map<WebVertex, Double> inputCosts, outputCosts;

    public WebVertex(
        @NonNull Coordinate position,
        final Collection<WebRequirement> mustFulfillAnyOfRequirements,
        final Collection<WebRequirement> mustFulfillNoneOfRequirements
    ) {
        this.position = position;
        this.mustFulfillAnyOfRequirements =
            mustFulfillAnyOfRequirements.size() == 0 ? Collections.emptyList() :
                Collections.synchronizedList(new ArrayList<>(mustFulfillAnyOfRequirements));
        this.mustFulfillNoneOfRequirements =
            mustFulfillNoneOfRequirements.size() == 0 ? Collections.emptyList() :
                Collections.synchronizedList(new ArrayList<>(mustFulfillNoneOfRequirements));
    }

    public WebVertex(
        @NonNull Coordinate position,
        final Collection<WebRequirement> mustFulfillAnyOfRequirements
    ) {
        this(position, mustFulfillAnyOfRequirements, Collections.emptyList());
    }

    public WebVertex(
        @NonNull Coordinate position,
        Collection<WebRequirement> mustFulfillAnyOfRequirements,
        Collection<WebRequirement> mustFulfillNoneOfRequirements, int protocol,
        ObjectInput stream
    ) {
        this(position, mustFulfillAnyOfRequirements, mustFulfillNoneOfRequirements);
        this.deserialize(protocol, stream);
    }

    /**
     * Calculates the weight between two {@code WebVertex}s using the default algorithm
     */
    public static double getDefaultWeightBetween(WebVertex first, WebVertex second) {
        double weight =
            Distance.Algorithm.MANHATTAN.calculate(first.position.getX(), first.position.getY(),
                second.position.getX(), second.position.getY()
            );
        if (first.position.getPlane() != second.position.getPlane()) {
            //Add special factor because it requires a plane change
            weight += Math.abs(first.position.getPlane() - second.position.getPlane()) * 10;
        }
        return weight;
    }

    public final void substitute(Web web, WebVertex vertex, double cost) {
        vertex.getInputCosts().putAll(getInputCosts().entrySet().stream()
            .filter(e -> !vertex.getInputCosts().containsKey(e.getKey()))
            .collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue)));
        vertex.getOutputCosts().putAll(getOutputCosts().entrySet().stream()
            .filter(e -> !vertex.getOutputCosts().containsKey(e.getKey()))
            .collect(Collectors.toConcurrentMap(Map.Entry::getKey, Map.Entry::getValue)));
        for (Map.Entry<WebVertex, Double> entry : vertex.getInputCosts().entrySet()) {
            entry.getKey().getOutputCosts().remove(this);
            entry.getKey().getOutputCosts().put(vertex, cost);
            entry.setValue(cost);
        }
        for (Map.Entry<WebVertex, Double> entry : vertex.getOutputCosts().entrySet()) {
            entry.getKey().getInputCosts().remove(this);
            entry.getKey().getInputCosts().put(vertex, cost);
            entry.setValue(cost);
        }
        web.removeVertex(this);
        List<WebVertex> verticesOn = web.getVerticesOn(vertex.position);
        if (verticesOn == null || !verticesOn.contains(vertex)) {
            web.addVertex(vertex);
        }
    }

    /**
     * Adds a bidirectional edge between this vertex and the target with a weight equaling the value returned by {code WebVertex#getDefaultWeightTo(WebVertex)}
     */
    public final void addBidirectionalEdge(final WebVertex target) {
        addBidirectionalEdge(target, getDefaultWeightTo(target));
    }

    /**
     * Adds a bidirectional edge with the specified weight between this vertex and the target.
     */
    public final void addBidirectionalEdge(final WebVertex target, final double weight) {
        this.addDirectedEdge(target, weight);
        target.addDirectedEdge(this, weight);
    }

    /**
     * Adds a directed edge with the specified weight
     */
    public void addDirectedEdge(WebVertex target, double weight) {
        Double existing = getOutputCosts().put(target, weight);
        if (existing != null && existing != weight) {
            throw new IllegalStateException(
                "A vertex can only have a single edge to another vertex, in this case " + this +
                    " -> " + target);
        }
        existing = target.getInputCosts().put(this, weight);
        if (existing != null && existing != weight) {
            throw new IllegalStateException(
                "A vertex can only have a single edge to another vertex, in this case " + this +
                    " <- " + target);
        }
    }

    /**
     * Adds a directed edge with a weight equaling the value returned by {code WebVertex#getDefaultWeightTo(WebVertex)}
     */
    public final void addDirectedEdge(final WebVertex target) {
        addDirectedEdge(target, getDefaultWeightTo(target));
    }

    /**
     * Calculates the default weight that would be used between this vertex and the target vertex
     */
    public double getDefaultWeightTo(WebVertex target) {
        return getDefaultWeightBetween(this, target);
    }

    public Double getInputCost(WebVertex vertex) {
        return getInputCosts().get(vertex);
    }

    public Map<WebVertex, Double> getInputCosts() {
        if (inputCosts == null) {
            inputCosts = new ConcurrentHashMap<>(4);
        }
        return inputCosts;
    }

    public Collection<WebVertex> getInputs() {
        return getInputCosts().keySet();
    }

    public Double getOutputCost(WebVertex vertex) {
        return getOutputCosts().get(vertex);
    }

    public Map<WebVertex, Double> getOutputCosts() {
        if (outputCosts == null) {
            outputCosts = new ConcurrentHashMap<>(4);
        }
        return outputCosts;
    }

    public Collection<WebVertex> getOutputs() {
        return getOutputCosts().keySet();
    }

    @NonNull
    @Override
    public final Coordinate getPosition() {
        return position;
    }

    @NonNull
    @Override
    public final Coordinate.HighPrecision getHighPrecisionPosition() {
        return position.getHighPrecisionPosition();
    }

    @NonNull
    @Override
    public Area.Rectangular getArea() {
        return position.getArea();
    }

    public final Collection<WebRequirement> getRequirements() {
        return mustFulfillAnyOfRequirements;
    }

    @Deprecated
    public final Collection<WebRequirement> getBlockingConditions() {
        return mustFulfillNoneOfRequirements;
    }

    public abstract int hashCode();

    protected int hashCode0() {
        if (cachedHashcode == -1) {
            return cachedHashcode = hashCode();
        }
        return cachedHashcode;
    }

    @Override
    public final boolean equals(final Object o) {
        if (o instanceof WebVertex
            && getOpcode() == ((WebVertex) o).getOpcode()
            && position.equals(((WebVertex) o).position)
            && mustFulfillAnyOfRequirements.size() ==
            ((WebVertex) o).mustFulfillAnyOfRequirements.size()) {
            int alternativeRequirements = 0;
            for (WebRequirement requirement : mustFulfillAnyOfRequirements) {
                while ((requirement = requirement.getAlternative()) != null) {
                    alternativeRequirements++;
                }
            }
            int oAlternativeRequirements = 0;
            for (WebRequirement requirement : ((WebVertex) o).mustFulfillAnyOfRequirements) {
                while ((requirement = requirement.getAlternative()) != null) {
                    oAlternativeRequirements++;
                }
            }
            if (alternativeRequirements == oAlternativeRequirements) {
                int alternativeBlockingRequirements = 0;
                for (WebRequirement requirement : mustFulfillNoneOfRequirements) {
                    while ((requirement = requirement.getAlternative()) != null) {
                        alternativeBlockingRequirements++;
                    }
                }
                int oAlternativeBlockingRequirements = 0;
                for (WebRequirement requirement : ((WebVertex) o).mustFulfillNoneOfRequirements) {
                    while ((requirement = requirement.getAlternative()) != null) {
                        oAlternativeBlockingRequirements++;
                    }
                }
                return alternativeBlockingRequirements == oAlternativeBlockingRequirements &&
                    hashCode0() == ((WebVertex) o).hashCode0();
            }
        }
        return false;

    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '(' + position.getX() + ", " + position.getY() + ", " +
            position.getPlane() + ')';
    }

    public boolean isIsolated() {
        return (outputCosts == null || outputCosts.isEmpty()) &&
            (inputCosts == null || inputCosts.isEmpty());
    }

    /**
     * Determines if the web vertex can be traversed over because all of its requirements are met.
     */
    @Override
    public final boolean isUsable() {
        return mustFulfillAnyOfRequirements.stream()
            .noneMatch(requirement -> !requirement.isMet() && !requirement.isAlternativeMet())
            && mustFulfillNoneOfRequirements.stream()
            .noneMatch(requirement -> requirement.isMet() || requirement.isAlternativeMet());
    }

    /**
     * Removed a directional edge that exists between this vertex and the target vertex.
     *
     * @param target the vertex to to disconnect this vertex from
     */
    public final boolean removeBidirectionalEdge(final WebVertex target) {
        boolean removal = this.removeDirectedEdge(target);
        return target.removeDirectedEdge(this) && removal;
    }

    /**
     * Removes an edge from this vertex to the target vertex, including the output edge from this vertex and the input edge for the target vertex.
     *
     * @param target the vertex to remove the edge towards.
     */
    public final boolean removeDirectedEdge(WebVertex target) {
        boolean removal = false;
        if (this.outputCosts != null) {
            removal = this.outputCosts.remove(target) != null;
        }
        if (target.inputCosts != null) {
            removal = target.inputCosts.remove(this) != null && removal;
        }
        return removal;
    }

    /**
     * Removes all edges that all connected to this vertex.
     */
    public final void removeEdges() {
        if (inputCosts != null) {
            for (WebVertex in : getInputs()) {
                if (in.outputCosts != null) {
                    in.outputCosts.remove(this);
                }
                inputCosts.remove(in);
            }
            inputCosts = null;
        }
        if (outputCosts != null) {
            for (WebVertex out : getOutputs()) {
                if (out.inputCosts != null) {
                    out.inputCosts.remove(this);
                }
                outputCosts.remove(out);
            }
            outputCosts = null;
        }
    }

    public abstract boolean step();

    public abstract boolean step(boolean prefersViewport);

    @NonNull
    protected abstract Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args);
}
