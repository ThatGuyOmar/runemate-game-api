package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.*;
import java.io.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class QuestPointRequirement extends WebRequirement implements SerializableRequirement {
    private int questPointsRequired;

    public QuestPointRequirement(int questPointsRequired) {
        this.questPointsRequired = questPointsRequired;
    }

    public QuestPointRequirement(int protocol, ObjectInput stream) {
        deserialize(protocol, stream);
    }

    @Override
    public boolean isMet0() {
        return Quests.getQuestPoints() >= questPointsRequired;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(questPointsRequired);
        return builder.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof QuestPointRequirement) {
            QuestPointRequirement that = (QuestPointRequirement) o;
            return this.questPointsRequired == that.questPointsRequired;
        }
        return false;
    }

    @Override
    public int getOpcode() {
        return 8;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeInt(questPointsRequired);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        questPointsRequired = stream.readInt();
        return true;
    }
}