package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.internal.exception.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import java.util.function.*;

public class UnderlayDefinitions {
    private static final UnderlayDefinitionLoader LOADER = new UnderlayDefinitionLoader();

    public static UnderlayDefinition load(int id) {
        try {
            UnderlayDefinition definition = LOADER.load(ConfigType.UNDERLAY, id);
            if (definition != null) {
                return definition;
            }
        } catch (IOException e) {
            throw new UnableToParseBufferException("Failed to load OverlayDefinition with id " + id + "from the Js5Cache.", e);
        }
        return null;
    }

    /**
     * Loads all definitions.
     */
    public static List<UnderlayDefinition> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all definitions that are accepted by the filter.
     */
    public static List<UnderlayDefinition> loadAll(final Predicate<UnderlayDefinition> filter) {
        int quantity;
        quantity = LOADER.getFiles(JS5CacheController.getLargestJS5CacheController(), 4).length;
        ArrayList<UnderlayDefinition> definitions = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final UnderlayDefinition definition = load(id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }
}
