package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class BasicNpcVertex extends NpcVertex implements SerializableVertex {
    private Pattern action;
    private double movement_radius;
    private Pattern name;

    public BasicNpcVertex(
        Coordinate position, double movementRadius, Pattern name, Pattern action,
        Collection<WebRequirement> requirements
    ) {
        super(new Area.Circular(position, movementRadius), requirements);
        this.movement_radius = movementRadius;
        this.name = name;
        this.action = action;
    }

    public BasicNpcVertex(
        Coordinate position, double movementRadius, String name, String action,
        Collection<WebRequirement> requirements
    ) {
        this(position, movementRadius, Regex.getPatternForExactString(name),
            Regex.getPatternForExactString(action), requirements
        );
    }

    public BasicNpcVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public Pattern getAction() {
        return action;
    }

    public double getMovementRadius() {
        return movement_radius;
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public Npc getNpc() {
        final NpcQueryBuilder builder = Npcs.newQuery().within(getWalkingBounds());
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder.results().first();
    }

    @Override
    public Predicate<Npc> getFilter() {
        final NpcQueryBuilder builder = Npcs.newQuery().within(getWalkingBounds());
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        return builder::accepts;
    }

    @Override
    public void invalidateCache() {
        provider = null;
    }

    @Override
    public int getOpcode() {
        return 7;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeDouble(movement_radius);
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.walkingBounds = new Area.Circular(position, movement_radius = stream.readDouble());
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition())
            .append(getName().pattern()).append(getAction().pattern()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "BasicNpcVertex(name=" + getName() + ", action=" + getAction() +
            ", x=" + position.getX() + ", y=" + position.getY() + ", plane=" + position.getPlane() +
            ')';
    }

    @Override
    public boolean step() {
        final Npc npc = getNpc();
        if (npc != null && npc.isVisible() && npc.interact(action, name)) {
            final Player avatar = Players.getLocal();
            final Coordinate start_position = avatar.getPosition();
            return Execution.delayWhile(() -> {
                Collection<Coordinate> currently_reachable;
                return npc.isValid() && start_position.isLoaded()
                    && (
                    (
                        currently_reachable =
                            avatar.getPosition().getReachableCoordinates()
                    ).contains(start_position) ||
                        currently_reachable.size() == 1
                );
            }, 3000, 4000);
        }
        return false;
    }
}
