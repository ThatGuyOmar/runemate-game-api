package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import lombok.*;

/**
 * A java.awt.Rectangle wrapped to provide Interactable functionality, use of this class is discouraged.
 */
public final class InteractableRectangle extends Rectangle implements Interactable, Renderable {
    public InteractableRectangle(final Rectangle rectangle) {
        this(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    public InteractableRectangle(final int x, final int y, final int width, final int height) {
        super(x, y, width, height);
        if (width <= 0) {
            throw new IllegalArgumentException(
                "Width is " + width + ", but width must be greater than 0.");
        }
        long maxX = (long) x + width - 1L;
        if (maxX != (int) maxX) {
            throw new IllegalArgumentException(
                "The bounds can overflow with the provided x of " + x + " and width of " + width +
                    '.');
        }
        if (height <= 0) {
            throw new IllegalArgumentException(
                "Height is " + height + ", but height must be greater than 0.");
        }
        long maxY = (long) y + height - 1L;
        if (maxY != (int) maxY) {
            throw new IllegalArgumentException(
                "The bounds can overflow with the provided y of " + y + " and height of " + height +
                    '.');
        }
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public double getVisibility() {
        return 100;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        return new InteractablePoint(
            width == 1 ? x : (int) Math.round(Random.nextGaussian(x, x + width - 1)),
            height == 1 ? y : (int) Math.round(Random.nextGaussian(y, y + height - 1))
        );
    }

    public InteractablePoint getCenterPoint() {
        return new InteractablePoint(x + (width >> 1), y + (height >> 1));
    }

    @Override
    public boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public boolean interact(final String action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public boolean interact(Pattern action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public boolean interact(final Pattern action, final Pattern target) {
        return Menu.click(this, action, target);
    }

    @Override
    public boolean interact(Pattern action, String target) {
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return interact(action, targetPattern);
    }

    @Override
    public boolean interact(String action, Pattern target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        return interact(actionPattern, target);
    }

    @NonNull
    @Override
    public String toString() {
        return "InteractableRectangle(" + (int) getX() + ", " + (int) getY() + ", " +
            (int) getWidth() + ", " + (int) getHeight() + ')';
    }

    @Override
    public void render(@NonNull Graphics2D g2d) {
        g2d.drawRect(x, y, width, height);
    }

    @Override
    public void render(@NonNull GraphicsContext gc) {
        gc.strokeRect(x, y, width, height);
    }
}
