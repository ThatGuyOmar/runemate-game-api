package com.runemate.game.api.hybrid.local;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * Used for getting quest information.
 * In RS3 this information is always available.
 * On OSRS, the quest status may report incorrectly until the quest tab is opened.
 */
public final class Quests {
    public static List<Quest> getAll(final Predicate<Quest> filter) {
        return Arrays.stream(Quest.OSRS.values()).filter(filter).collect(Collectors.toList());
    }

    public static List<Quest> getAll() {
        return Arrays.asList(Quest.OSRS.values());
    }

    public static Quest get(final String name) {
        final List<Quest> quests = get(new String[] { name });
        return !quests.isEmpty() ? quests.get(0) : null;
    }

    public static List<Quest> get(final String... names) {
        final List<String> ns = Arrays.asList(names);
        return Arrays.stream(Quest.OSRS.values()).filter(q -> ns.contains(q.getName())).collect(Collectors.toList());
    }

    public static int getQuestPoints() {
        return Varps.getAt(101).getValue();
    }
}
