package com.runemate.game.api.hybrid.cache.configs;

import com.google.common.cache.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2
@UtilityClass
public class DBTables {

    private static final DBTableLoader LOADER = new DBTableLoader();
    private static final Cache<Integer, DBTable> CACHE = CacheBuilder.newBuilder()
        .maximumSize(100)
        .expireAfterAccess(5, TimeUnit.MINUTES)
        .build();

    public static DBTable load(int table) {
        return load(JS5CacheController.getLargestJS5CacheController(), table);
    }

    public static DBTable load(JS5CacheController controller, int table) {
        if (table >= 0) {
            DBTable def = CACHE.getIfPresent(table);
            if (def == null) {
                try {
                    def = LOADER.load(controller, ConfigType.DBTABLE, table);
                } catch (IOException e) {
                    log.warn("Failed to load DBTable {}", table, e);
                }
            }
            if (def != null) {
                CACHE.put(table, def);
            }
            return def;
        }
        return null;
    }

    public static List<DBTable> loadAll() {
        return loadAll(null);
    }

    public static List<DBTable> loadAll(final Predicate<DBTable> filter) {
        var controller = JS5CacheController.getLargestJS5CacheController();
        var quantity = LOADER.getFiles(controller, ConfigType.DBTABLE).length;
        var tables = new ArrayList<DBTable>(quantity);
        for (int id = 0; id <= quantity; id++) {
            var loaded = load(controller, id);
            if (loaded != null && (filter == null || filter.test(loaded))) {
                tables.add(loaded);
            }
        }
        tables.trimToSize();
        return tables;
    }

}
