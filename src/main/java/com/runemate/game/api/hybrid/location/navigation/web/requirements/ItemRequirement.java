package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;

public class ItemRequirement extends WebRequirement implements SerializableRequirement {
    private int[] itemIds;
    private Pattern name;
    private SpriteItem.Origin origin;
    private int quantity;

    /**
     * @param origin   the origin of this item
     * @param itemIds  the possible ids of this item
     * @param quantity the minimum quantity of this item
     */
    public ItemRequirement(SpriteItem.Origin origin, int[] itemIds, int quantity) {
        this.origin = origin;
        this.itemIds = itemIds;
        this.quantity = quantity;
    }

    /**
     * @param itemId   an int matching the possible id of this item
     * @param origin   the origin of this item
     * @param quantity the minimum quantity of this item
     */
    public ItemRequirement(SpriteItem.Origin origin, int itemId, int quantity) {
        this(origin, new int[] { itemId }, quantity);
    }

    /**
     * @param origin   the origin of this item
     * @param name     a Pattern matching the possible names of this item
     * @param quantity the minimum quantity of this item
     */
    public ItemRequirement(SpriteItem.Origin origin, Pattern name, int quantity) {
        this.origin = origin;
        this.name = name;
        this.quantity = quantity;
    }

    /**
     * @param origin   the origin of this item
     * @param names    the possible names of this item
     * @param quantity the minimum quantity of this item
     */
    public ItemRequirement(SpriteItem.Origin origin, String[] names, int quantity) {
        this(origin, Regex.getPatternForExactStrings(names), quantity);
    }

    /**
     * @param origin   the origin of this item
     * @param name     the name of this item
     * @param quantity the minimum quantity of this item
     */
    public ItemRequirement(SpriteItem.Origin origin, String name, int quantity) {
        this(origin, Regex.getPatternForExactString(name), quantity);
    }

    public ItemRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public Pattern getName() {
        return name;
    }

    @Override
    public int getOpcode() {
        return 2;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(origin.name());
        if (name != null) {
            stream.write(1);
            stream.writeUTF(name.pattern());
            stream.writeInt(name.flags());
        }
        if (itemIds != null) {
            stream.write(2);
            stream.write(itemIds.length);
            for (int itemId : itemIds) {
                stream.writeInt(itemId);
            }
        }
        stream.writeInt(quantity);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        String originString = stream.readUTF();
        origin = Enums.safeValueOf(SpriteItem.Origin.class, originString, true);
        if (origin == null) {
            throw new IllegalArgumentException("Unknown origin: " + originString);
        }
        if (protocol <= 6) {
            int nameCount = stream.readByte();
            if (nameCount <= 0) {
                throw new IllegalArgumentException(
                    "Invalid amount of item names: " + nameCount + " <= 0");
            }
            String[] names = new String[nameCount];
            for (int j = 0; j < nameCount; ++j) {
                names[j] = stream.readUTF();
            }
            name = Regex.getPatternForExactStrings(names);
        } else {
            int type = stream.read();
            if (type == 1) {
                name = Pattern.compile(stream.readUTF(), stream.readInt());
            } else if (type == 2) {
                itemIds = new int[stream.read()];
                for (int i = 0; i < itemIds.length; ++i) {
                    itemIds[i] = stream.readInt();
                }
            } else {
                throw new IllegalArgumentException(
                    "Cannot deserialize an item requirement with identifiers of type " + type);
            }
        }
        quantity = stream.readInt();
        if (quantity < 0) {
            throw new IllegalArgumentException("Invalid item quantity required: " + quantity);
        }
        return true;
    }

    public SpriteItem.Origin getOrigin() {
        return origin;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public int hashCode() {
        int result = origin.hashCode();
        result =
            31 * result + (name != null ? name.pattern().hashCode() : Arrays.hashCode(itemIds));
        result = 31 * result + quantity;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof ItemRequirement) {
            ItemRequirement that = (ItemRequirement) o;
            return quantity == that.quantity && origin == that.origin &&
                Objects.equals(name, that.name);
        }
        return false;
    }

    @Override
    public String toString() {
        return "ItemRequirement(" + origin + ' ' + name + ' ' + quantity + ')';
    }

    @Override
    public boolean isMet0() {
        SpriteItemQueryBuilder builder;
        switch (origin) {
            case INVENTORY:
                builder = Inventory.newQuery();
                break;
            case EQUIPMENT:
                builder = Equipment.newQuery();
                break;
            case BANK:
                builder = Bank.newQuery();
                break;
            default:
                builder = null;
        }
        if (builder != null) {
            if (name != null &&
                builder.names(name).results().stream().mapToInt(SpriteItem::getQuantity).sum() >=
                    quantity) {
                return true;
            }
            return itemIds != null &&
                builder.ids(itemIds).results().stream().mapToInt(SpriteItem::getQuantity).sum() >=
                    quantity;
        }
        return false;
    }
}
