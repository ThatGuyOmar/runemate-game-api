package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.script.annotations.*;
import com.runemate.game.api.script.exceptions.*;
import java.util.*;
import lombok.*;

public interface Quest {

    int getId();

    default QuestDefinition getDefinition() {
        return QuestDefinitions.load(getId());
    }

    /**
     * Gets the name of the quest
     */
    String getName();

    /**
     * Gets the Quest.Status representing the of the quest.
     *
     * @return the status of the quest, {@link Quest.Status#UNKNOWN} if reading the status failed
     * @see Quest.Status
     */
    @NonNull
    Status getStatus();

    /**
     * @deprecated Use {@code getDefinition().isMembers()}
     */
    @Deprecated
    boolean isMembersOnly();

    /**
     * @deprecated Use {@code getDefinition().getRewardPoints()}
     */
    @RS3Only
    @Deprecated
    int getQuestPointsRewarded();

    @RS3Only
    @Deprecated
    List<Attribute> getAttributes();

    @RS3Only
    @Deprecated
    List<Quest> getPrerequisiteQuests();

    enum Status {
        NOT_STARTED,
        IN_PROGRESS,
        COMPLETE,
        UNKNOWN;

        public static Status of(int id) {
            switch (id) {
                case 2:
                    return COMPLETE;
                case 1:
                    return NOT_STARTED;
                case -1:
                    return UNKNOWN;
                default:
                    return IN_PROGRESS;
            }
        }
    }

    enum OSRS implements Quest {
        /* Free */
        BELOW_ICE_MOUNTAIN(6, 162, "Below Ice Mountain", 120, VarbitID.QUEST_BELOW_ICE_MOUNTAIN),
        BLACK_KNIGHTS_FORTRESS(10, 12, "Black Knights' Fortress", 4, VarpID.QUEST_BLACK_KNIGHTS_FORTRESS),
        COOKS_ASSISTANT(17, 1, "Cook's Assistant", 2, VarpID.QUEST_COOKS_ASSISTANT),
        THE_CORSAIR_CURSE(18, 147, "The Corsair Curse", 60, VarbitID.QUEST_THE_CORSAIR_CURSE),
        DEMON_SLAYER(25, 2, "Demon Slayer", 3, VarbitID.QUEST_DEMON_SLAYER),
        DORICS_QUEST(30, 11, "Doric's Quest", 100, VarpID.QUEST_DORICS_QUEST),
        DRAGON_SLAYER_I(31, 17, "Dragon Slayer I", 10, VarpID.QUEST_DRAGON_SLAYER_I),
        ERNEST_THE_CHICKEN(44, 7, "Ernest the Chicken", 3, VarpID.QUEST_ERNEST_THE_CHICKEN),
        GOBLIN_DIPLOMACY(64, 15, "Goblin Diplomacy", 6, VarbitID.QUEST_GOBLIN_DIPLOMACY),
        IMP_CATCHER(76, 9, "Imp Catcher", 2, VarpID.QUEST_IMP_CATCHER),
        THE_KNIGHTS_SWORD(83, 14, "The Knight's Sword", 7, VarpID.QUEST_THE_KNIGHTS_SWORD),
        MISTHALIN_MYSTERY(94, 141, "Misthalin Mystery", 135, VarbitID.QUEST_MISTHALIN_MYSTERY),
        PIRATES_TREASURE(108, 16, "Pirate's Treasure", 4, VarpID.QUEST_PIRATES_TREASURE),
        PRINCE_ALI_RESCUE(112, 10, "Prince Ali Rescue", 110, VarpID.QUEST_PRINCE_ALI_RESCUE),
        THE_RESTLESS_GHOST(120, 3, "The Restless Ghost", 5, VarpID.QUEST_THE_RESTLESS_GHOST),
        ROMEO_AND_JULIET(121, 4, "Romeo & Juliet", 100, VarpID.QUEST_ROMEO_AND_JULIET),
        RUNE_MYSTERIES(125, 53, "Rune Mysteries", 6, VarpID.QUEST_RUNE_MYSTERIES),
        SHEEP_SHEARER(131, 5, "Sheep Shearer", 21, VarpID.QUEST_SHEEP_SHEARER),
        SHIELD_OF_ARRAV(132, 6, "Shield of Arrav", 2, VarpID.QUEST_SHIELD_OF_ARRAV),
        VAMPYRE_SLAYER(155, 8, "Vampyre Slayer", 3, VarpID.QUEST_VAMPYRE_SLAYER),
        WITCHS_POTION(161, 13, "Witch's Potion", 3, VarpID.QUEST_WITCHS_POTION),
        X_MARKS_THE_SPOT(162, 154, "X Marks the Spot", 8, VarbitID.QUEST_X_MARKS_THE_SPOT),

        /* Members */
        ANIMAL_MAGNETISM(0, 123, "Animal Magnetism", 240, VarbitID.QUEST_ANIMAL_MAGNETISM),
        ANOTHER_SLICE_OF_HAM(1, 133, "Another Slice of H.A.M.", 11, VarbitID.QUEST_ANOTHER_SLICE_OF_HAM),
        THE_ASCENT_OF_ARCEUUS(3, 152, "The Ascent of Arceuus", 14, VarbitID.QUEST_THE_ASCENT_OF_ARCEUUS),
        BENEATH_CURSED_SANDS(168, 169, "Beneath Cursed Sands", 108, VarbitID.QUEST_BENEATH_CURSED_SANDS),
        BETWEEN_A_ROCK(7, 76, "Between a Rock...", 110, VarbitID.QUEST_BETWEEN_A_ROCK),
        BIG_CHOMPY_BIRD_HUNTING(8, 54, "Big Chompy Bird Hunting", 65, VarpID.QUEST_BIG_CHOMPY_BIRD_HUNTING),
        BIOHAZARD(9, 39, "Biohazard", 16, VarpID.QUEST_BIOHAZARD),
        BONE_VOYAGE(11, 143, "Bone Voyage", 50, VarbitID.QUEST_BONE_VOYAGE),
        CABIN_FEVER(12, 104, "Cabin Fever", 140, VarpID.QUEST_CABIN_FEVER),
        CLIENT_OF_KOUREND(13, 142, "Client of Kourend", 7, VarbitID.QUEST_CLIENT_OF_KOUREND),
        CLOCK_TOWER(14, 29, "Clock Tower", 8, VarpID.QUEST_CLOCK_TOWER),
        COLD_WAR(15, 126, "Cold War", 135, VarbitID.QUEST_COLD_WAR),
        CONTACT(16, 124, "Contact!", 130, VarbitID.QUEST_CONTACT),
        CREATURE_OF_FENKENSTRAIN(19, 71, "Creature of Fenkenstrain", 9, VarpID.QUEST_CREATURE_OF_FENKENSTRAIN),
        DARKNESS_OF_HALLOWVALE(22, 117, "Darkness of Hallowvale", 320, VarbitID.QUEST_DARKNESS_OF_HALLOWVALE),
        DEATH_PLATEAU(23, 58, "Death Plateau", 80, VarpID.QUEST_DEATH_PLATEAU),
        DEATH_TO_THE_DORGESHUUN(24, 113, "Death to the Dorgeshuun", 13, VarbitID.QUEST_DEATH_TO_THE_DORGESHUUN),
        THE_DEPTHS_OF_DESPAIR(26, 145, "The Depths of Despair", 11, VarbitID.QUEST_THE_DEPTHS_OF_DESPAIR),
        DESERT_TREASURE(27, 79, "Desert Treasure", 15, VarbitID.QUEST_DESERT_TREASURE),
        DEVIOUS_MINDS(28, 101, "Devious Minds", 80, VarbitID.QUEST_DEVIOUS_MINDS),
        THE_DIG_SITE(29, 49, "The Dig Site", 9, VarpID.QUEST_THE_DIG_SITE),
        DRAGON_SLAYER_II(32, 148, "Dragon Slayer II", 215, VarbitID.QUEST_DRAGON_SLAYER_II),
        DREAM_MENTOR(33, 134, "Dream Mentor", 28, VarbitID.QUEST_DREAM_MENTOR),
        DRUIDIC_RITUAL(34, 18, "Druidic Ritual", 4, VarpID.QUEST_DRUIDIC_RITUAL),
        DWARF_CANNON(35, 47, "Dwarf Cannon", 11, VarpID.QUEST_DWARF_CANNON),
        EADGARS_RUSE(36, 62, "Eadgar's Ruse", 110, VarpID.QUEST_EADGARS_RUSE),
        EAGLES_PEAK(37, 122, "Eagles' Peak", 40, VarbitID.QUEST_EAGLES_PEAK),
        ELEMENTAL_WORKSHOP_I(38, 55, "Elemental Workshop I", 2, VarpID.QUEST_ELEMENTAL_WORKSHOP_I),
        ELEMENTAL_WORKSHOP_II(39, 119, "Elemental Workshop II", 11, VarbitID.QUEST_ELEMENTAL_WORKSHOP_II),
        ENAKHRAS_LAMENT(40, 103, "Enakhra's Lament", 70, VarbitID.QUEST_ENAKHRAS_LAMENT),
        ENLIGHTENED_JOURNEY(42, 121, "Enlightened Journey", 200, VarbitID.QUEST_ENLIGHTENED_JOURNEY),
        THE_EYES_OF_GLOUPHRIE(45, 116, "The Eyes of Glouphrie", 60, VarbitID.QUEST_THE_EYES_OF_GLOUPHRIE),
        FAIRYTALE_I_GROWING_PAINS(46, 105, "Fairytale I - Growing Pains", 90, VarbitID.QUEST_FAIRYTALE_I_GROWING_PAINS),
        FAIRYTALE_II_CURE_A_QUEEN(47, 114, "Fairytale II - Cure a Queen", 10, 90, VarbitID.QUEST_FAIRYTALE_II_CURE_A_QUEEN),
        FAMILY_CREST(48, 25, "Family Crest", 11, VarpID.QUEST_FAMILY_CREST),
        THE_FEUD(50, 77, "The Feud", 28, VarbitID.QUEST_THE_FEUD),
        FIGHT_ARENA(51, 33, "Fight Arena", 14, VarpID.QUEST_FIGHT_ARENA),
        FISHING_CONTEST(52, 27, "Fishing Contest", 5, VarpID.QUEST_FISHING_CONTEST),
        FORGETTABLE_TALE(53, 88, "Forgettable Tale...", 140, VarbitID.QUEST_FORGETTABLE_TALE),
        THE_FORSAKEN_TOWER(54, 153, "The Forsaken Tower", 11, VarbitID.QUEST_THE_FORSAKEN_TOWER),
        THE_FREMENNIK_EXILES(55, 157, "The Fremennik Exiles", 130, VarbitID.QUEST_THE_FREMENNIK_EXILES),
        THE_FREMENNIK_ISLES(56, 127, "The Fremennik Isles", 340, VarbitID.QUEST_THE_FREMENNIK_ISLES),
        THE_FREMENNIK_TRIALS(57, 64, "The Fremennik Trials", 10, VarpID.QUEST_THE_FREMENNIK_TRIALS),
        GARDEN_OF_TRANQUILLITY(58, 90, "Garden of Tranquillity", 60, VarbitID.QUEST_GARDEN_OF_TRANQUILLITY),
        GERTRUDES_CAT(60, 50, "Gertrude's Cat", 6, VarpID.QUEST_GERTRUDES_CAT),
        GETTING_AHEAD(61, 161, "Getting Ahead", 34, VarbitID.QUEST_GETTING_AHEAD),
        GHOSTS_AHOY(62, 73, "Ghosts Ahoy", 8, VarbitID.QUEST_GHOSTS_AHOY),
        THE_GIANT_DWARF(63, 84, "The Giant Dwarf", 50, VarbitID.QUEST_THE_GIANT_DWARF),
        THE_GOLEM(65, 78, "The Golem", 10, VarbitID.QUEST_THE_GOLEM),
        THE_GRAND_TREE(66, 41, "The Grand Tree", 160, VarpID.QUEST_THE_GRAND_TREE),
        THE_GREAT_BRAIN_ROBBERY(67, 130, "The Great Brain Robbery", 130, VarpID.QUEST_THE_GREAT_BRAIN_ROBBERY),
        GRIM_TALES(68, 135, "Grim Tales", 60, VarbitID.QUEST_GRIM_TALES),
        THE_HAND_IN_THE_SAND(69, 102, "The Hand in the Sand", 160, VarbitID.QUEST_THE_HAND_IN_THE_SAND),
        HAUNTED_MINE(70, 68, "Haunted Mine", 11, VarpID.QUEST_HAUNTED_MINE),
        HAZEEL_CULT(71, 34, "Hazeel Cult", 9, VarpID.QUEST_HAZEEL_CULT),
        HEROES_QUEST(72, 22, "Heroes' Quest", 15, VarpID.QUEST_HEROES_QUEST),
        HOLY_GRAIL(73, 31, "Holy Grail", 10, VarpID.QUEST_HOLY_GRAIL),
        HORROR_FROM_THE_DEEP(74, 65, "Horror from the Deep", 10, VarbitID.QUEST_HORROR_FROM_THE_DEEP),
        ICTHLARINS_LITTLE_HELPER(75, 80, "Icthlarin's Little Helper", 26, VarbitID.QUEST_ICTHLARINS_LITTLE_HELPER),
        IN_AID_OF_THE_MYREQUE(77, 107, "In Aid of the Myreque", 430, VarbitID.QUEST_IN_AID_OF_THE_MYREQUE),
        IN_SEARCH_OF_THE_MYREQUE(79, 70, "In Search of the Myreque", 105, VarpID.QUEST_IN_SEARCH_OF_THE_MYREQUE),
        JUNGLE_POTION(80, 40, "Jungle Potion", 12, VarpID.QUEST_JUNGLE_POTION),
        KINGS_RANSOM(82, 136, "King's Ransom", 90, VarbitID.QUEST_KINGS_RANSOM),
        A_KINGDOM_DIVIDED(81, 164, "A Kingdom Divided", 150, VarbitID.QUEST_A_KINGDOM_DIVIDED),
        LAND_OF_THE_GOBLINS(165, 166, "Land of the Goblins", 56, VarbitID.QUEST_LAND_OF_THE_GOBLINS),
        LEGENDS_QUEST(85, 51, "Legends' Quest", 75, VarpID.QUEST_LEGENDS_QUEST),
        LOST_CITY(86, 19, "Lost City", 6, VarpID.QUEST_LOST_CITY),
        THE_LOST_TRIBE(87, 83, "The Lost Tribe", 11, VarbitID.QUEST_THE_LOST_TRIBE),
        LUNAR_DIPLOMACY(88, 115, "Lunar Diplomacy", 190, VarbitID.QUEST_LUNAR_DIPLOMACY),
        MAKING_FRIENDS_WITH_MY_ARM(91, 151, "Making Friends with My Arm", 200, VarbitID.QUEST_MAKING_FRIENDS_WITH_MY_ARM),
        MAKING_HISTORY(92, 97, "Making History", 4, VarbitID.QUEST_MAKING_HISTORY),
        MERLINS_CRYSTAL(93, 21, "Merlin's Crystal", 7, VarpID.QUEST_MERLINS_CRYSTAL),
        MONKS_FRIEND(97, 28, "Monk's Friend", 80, VarpID.QUEST_MONKS_FRIEND),
        MONKEY_MADNESS_I(95, 67, "Monkey Madness I", 9, VarpID.QUEST_MONKEY_MADNESS_I),
        MONKEY_MADNESS_II(96, 139, "Monkey Madness II", 195, VarbitID.QUEST_MONKEY_MADNESS_II),
        MOUNTAIN_DAUGHTER(98, 75, "Mountain Daughter", 70, VarbitID.QUEST_MOUNTAIN_DAUGHTER),
        MOURNINGS_END_PART_I(99, 87, "Mourning's End Part I", 9, VarpID.QUEST_MOURNINGS_END_PART_I),
        MOURNINGS_END_PART_II(100, 93, "Mourning's End Part II", 60, VarbitID.QUEST_MOURNINGS_END_PART_II),
        MURDER_MYSTERY(101, 48, "Murder Mystery", 2, VarpID.QUEST_MURDER_MYSTERY),
        MY_ARMS_BIG_ADVENTURE(102, 120, "My Arm's Big Adventure", 320, VarbitID.QUEST_MY_ARMS_BIG_ADVENTURE),
        NATURE_SPIRIT(103, 57, "Nature Spirit", 110, VarpID.QUEST_NATURE_SPIRIT),
        A_NIGHT_AT_THE_THEATRE(104, 163, "A Night at the Theatre", 86, VarbitID.QUEST_A_NIGHT_AT_THE_THEATRE),
        OBSERVATORY_QUEST(105, 44, "Observatory Quest", 7, VarpID.QUEST_OBSERVATORY_QUEST),
        OLAFS_QUEST(106, 132, "Olaf's Quest", 80, VarbitID.QUEST_OLAFS_QUEST),
        ONE_SMALL_FAVOUR(107, 74, "One Small Favour", 285, VarpID.QUEST_ONE_SMALL_FAVOUR),
        PLAGUE_CITY(109, 36, "Plague City", 29, VarpID.QUEST_PLAGUE_CITY),
        A_PORCINE_OF_INTEREST(110, 160, "A Porcine of Interest", 40, VarbitID.QUEST_A_PORCINE_OF_INTEREST),
        PRIEST_IN_PERIL(111, 56, "Priest in Peril", 60, VarpID.QUEST_PRIEST_IN_PERIL),
        THE_QUEEN_OF_THIEVES(113, 144, "The Queen of Thieves", 13, VarbitID.QUEST_THE_QUEEN_OF_THIEVES),
        RAG_AND_BONE_MAN_I(114, 109, "Rag and Bone Man I", 4, VarpID.QUEST_RAG_AND_BONE_MAN_I),
        RAG_AND_BONE_MAN_II(115, 110, "Rag and Bone Man II", 3, 6, VarpID.QUEST_RAG_AND_BONE_MAN_II),
        RATCATCHERS(116, 99, "Ratcatchers", 127, VarbitID.QUEST_RATCATCHERS),
        RECIPE_FOR_DISASTER(117, 106, "Recipe for Disaster", 5, VarbitID.QUEST_RECIPE_FOR_DISASTER),
        RFD_SIR_AMIK_VARZE(2314, 178, "Recipe for Disaster - Sir Amik Varze", 20, VarbitID.QUEST_RECIPE_FOR_DISASTER_SIR_AMIK_VARZE),
        RFD_ANOTHER_COOKS_QUEST(2307, 171, "Recipe for Disaster - Another Cook's Quest", 2, VarbitID.QUEST_RECIPE_FOR_DISASTER),
        RFD_KING_AWOWOGEI(2315, 179, "Recipe for Disaster - King Awowogei", 50, VarbitID.QUEST_RECIPE_FOR_DISASTER_MONKEY_AMBASSADOR),
        RFD_CULINAROMANCER(2316, 180, "Recipe for Disaster - Culinaromancer", 3, 5, VarbitID.QUEST_RECIPE_FOR_DISASTER),
        RFD_EVIL_DAVE(2312, 176, "Recipe for Disaster - Evil Dave", 5, VarbitID.QUEST_RECIPE_FOR_DISASTER_EVIL_DAVE),
        RFD_MOUNTAIN_DWARF(2308, 172, "Recipe for Disaster - Mountain Dwarf", 60, VarbitID.QUEST_RECIPE_FOR_DISASTER_DWARF),
        RFD_WARTFACE_AND_BENTNOZE(2309, 173, "Recipe for Disaster - Wartface & Bentnoze", 40, VarbitID.QUEST_RECIPE_FOR_DISASTER_WARTFACE_AND_BENTNOZE),
        RFD_LUMBRIDGE_GUIDE(2311, 175, "Recipe for Disaster - Lumbridge Guide", 5, VarbitID.QUEST_RECIPE_FOR_DISASTER_LUMBRIDGE_GUIDE),
        RFD_PIRATE_PETE(2310, 174, "Recipe for Disaster - Pirate Pete", 110, VarbitID.QUEST_RECIPE_FOR_DISASTER_PIRATE_PETE),
        RFD_SKRACH_UGLOGWEE(2313, 177, "Recipe for Disaster - Skrach Uglogwee", 170, VarbitID.QUEST_RECIPE_FOR_DISASTER_SKRACH_UGLOGWEE),
        RECRUITMENT_DRIVE(118, 86, "Recruitment Drive", 2, VarbitID.QUEST_RECRUITMENT_DRIVE),
        REGICIDE(119, 61, "Regicide", 15, VarpID.QUEST_REGICIDE),
        ROVING_ELVES(122, 72, "Roving Elves", 6, VarpID.QUEST_ROVING_ELVES),
        ROYAL_TROUBLE(123, 112, "Royal Trouble", 30, VarbitID.QUEST_ROYAL_TROUBLE),
        RUM_DEAL(124, 95, "Rum Deal", 19, VarpID.QUEST_RUM_DEAL),
        SCORPION_CATCHER(126, 24, "Scorpion Catcher", 6, VarpID.QUEST_SCORPION_CATCHER),
        SEA_SLUG(127, 37, "Sea Slug", 12, VarpID.QUEST_SEA_SLUG),
        SHADES_OF_MORTTON(128, 63, "Shades of Mort'ton", 85, VarpID.QUEST_SHADES_OF_MORTTON),
        SHADOW_OF_THE_STORM(129, 96, "Shadow of the Storm", 125, VarbitID.QUEST_SHADOW_OF_THE_STORM),
        SHEEP_HERDER(130, 35, "Sheep Herder", 3, VarpID.QUEST_SHEEP_HERDER),
        SHILO_VILLAGE(133, 42, "Shilo Village", 15, VarpID.QUEST_SHILO_VILLAGE),
        SINS_OF_THE_FATHER(134, 158, "Sins of the Father", 138, VarbitID.QUEST_SINS_OF_THE_FATHER),
        SLEEPING_GIANTS(169, 170, "Sleeping Giants", 30, VarbitID.QUEST_SLEEPING_GIANTS),
        THE_SLUG_MENACE(136, 118, "The Slug Menace", 14, VarbitID.QUEST_THE_SLUG_MENACE),
        SONG_OF_THE_ELVES(137, 156, "Song of the Elves", 200, VarbitID.QUEST_SONG_OF_THE_ELVES),
        A_SOULS_BANE(138, 108, "A Soul's Bane", 13, VarbitID.QUEST_A_SOULS_BANE),
        SPIRITS_OF_THE_ELID(139, 100, "Spirits of the Elid", 60, VarbitID.QUEST_SPIRITS_OF_THE_ELID),
        SWAN_SONG(140, 111, "Swan Song", 200, VarbitID.QUEST_SWAN_SONG),
        TAI_BWO_WANNAI_TRIO(141, 60, "Tai Bwo Wannai Trio", 2, 6, VarpID.QUEST_TAI_BWO_WANNAI_TRIO),
        A_TAIL_OF_TWO_CATS(142, 91, "A Tail of Two Cats", 70, VarbitID.QUEST_A_TAIL_OF_TWO_CATS),
        TALE_OF_THE_RIGHTEOUS(143, 149, "Tale of the Righteous", 17, VarbitID.QUEST_TALE_OF_THE_RIGHTEOUS),
        A_TASTE_OF_HOPE(144, 150, "A Taste of Hope", 165, VarbitID.QUEST_A_TASTE_OF_HOPE),
        TEARS_OF_GUTHIX(145, 81, "Tears of Guthix", 2, VarbitID.QUEST_TEARS_OF_GUTHIX),
        TEMPLE_OF_IKOV(146, 30, "Temple of Ikov", 80, VarpID.QUEST_TEMPLE_OF_IKOV),
        TEMPLE_OF_THE_EYE(167, 168, "Temple of the Eye", 130, VarbitID.QUEST_TEMPLE_OF_THE_EYE),
        THRONE_OF_MISCELLANIA(147, 66, "Throne of Miscellania", 100, VarpID.QUEST_THRONE_OF_MISCELLANIA),
        THE_TOURIST_TRAP(148, 45, "The Tourist Trap", 30, VarpID.QUEST_THE_TOURIST_TRAP),
        TOWER_OF_LIFE(149, 129, "Tower of Life", 18, VarbitID.QUEST_TOWER_OF_LIFE),
        TREE_GNOME_VILLAGE(150, 32, "Tree Gnome Village", 9, VarpID.QUEST_TREE_GNOME_VILLAGE),
        TRIBAL_TOTEM(151, 26, "Tribal Totem", 5, VarpID.QUEST_TRIBAL_TOTEM),
        TROLL_ROMANCE(152, 69, "Troll Romance", 45, VarpID.QUEST_TROLL_ROMANCE),
        TROLL_STRONGHOLD(153, 59, "Troll Stronghold", 50, VarpID.QUEST_TROLL_STRONGHOLD),
        UNDERGROUND_PASS(154, 43, "Underground Pass", 11, VarpID.QUEST_UNDERGROUND_PASS),
        WANTED(156, 92, "Wanted!", 11, VarbitID.QUEST_WANTED),
        WATCHTOWER(157, 46, "Watchtower", 13, VarpID.QUEST_WATCHTOWER),
        WATERFALL_QUEST(158, 38, "Waterfall Quest", 10, VarpID.QUEST_WATERFALL_QUEST),
        WHAT_LIES_BELOW(159, 131, "What Lies Below", 150, VarbitID.QUEST_WHAT_LIES_BELOW),
        WITCHS_HOUSE(160, 20, "Witch's House", 7, VarpID.QUEST_WITCHS_HOUSE),
        ZOGRE_FLESH_EATERS(163, 82, "Zogre Flesh Eaters", 14, VarbitID.QUEST_ZOGRE_FLESH_EATERS),
        SECRETS_OF_THE_NORTH(2338, 183, "Secrets of the North", 90, VarbitID.QUEST_SECRETS_OF_THE_NORTH),
        DESERT_TREASURE_II(2343, 185, "Desert Treasure II - The Fallen Empire", 118, VarbitID.QUEST_DESERT_TREASURE_II),

        /* Miniquests */
        ALFRED_GRIMHANDS_BARCRAWL(4, 23, "Alfred Grimhand's Barcrawl", 2, VarpID.QUEST_ALFRED_GRIMHANDS_BARCRAWL),
        ARCHITECTURAL_ALLIANCE(2, 137, "Architectural Alliance", 3, VarbitID.QUEST_ARCHITECTURAL_ALLIANCE),
        BEAR_YOUR_SOUL(5, 138, "Bear Your Soul", 3, VarbitID.QUEST_BEAR_YOUR_SOUL),
        CURSE_OF_THE_EMPTY_LORD(20, 89, "Curse of the Empty Lord", 6, VarbitID.QUEST_CURSE_OF_THE_EMPTY_LORD),
        DADDYS_HOME(21, 159, "Daddy's Home", 13, VarbitID.QUEST_DADDYS_HOME),
        THE_ENCHANTED_KEY(41, 98, "The Enchanted Key", 2, VarbitID.QUEST_ENCHANTED_KEY),
        ENTER_THE_ABYSS(43, 85, "Enter the Abyss", 4, VarpID.QUEST_ENTER_THE_ABYSS),
        FAMILY_PEST(49, 140, "Family Pest", 3, VarbitID.QUEST_FAMILY_PEST),
        THE_FROZEN_DOOR(164, 165, "The Frozen Door", 10, VarbitID.QUEST_THE_FROZEN_DOOR),
        THE_GENERALS_SHADOW(59, 128, "The General's Shadow", 30, VarbitID.QUEST_THE_GENERALS_SHADOW),
        HOPESPEARS_WILL(166, 167, "Hopespear's Will", 2, VarbitID.QUEST_HOPESPEARS_WILL),
        IN_SEARCH_OF_KNOWLEDGE(78, 155, "In Search of Knowledge", 3, VarbitID.QUEST_IN_SEARCH_OF_KNOWLEDGE),
        INTO_THE_TOMBS(2306, 181, "Into the Tombs", 24, VarbitID.QUEST_INTO_THE_TOMBS),
        LAIR_OF_TARN_RAZORLOR(84, 125, "Lair of Tarn Razorlor", 3, VarbitID.QUEST_LAIR_OF_TARN_RAZORLOR),
        MAGE_ARENA_I(89, 52, "Mage Arena I", 8, VarbitID.QUEST_THE_MAGE_ARENA_II),
        MAGE_ARENA_II(90, 146, "Mage Arena II", 4, VarbitID.QUEST_THE_MAGE_ARENA_II),
        SKIPPY_AND_THE_MOGRES(135, 94, "Skippy and the Mogres", 3, VarbitID.QUEST_SKIPPY_AND_THE_MOGRES),
        ;

        private final int cacheRowId;
        private final int id;
        private final String name;
        private final int progressValue;
        private final int completionValue;

        private final int progressVarp;
        private final int progressVarbit;

        OSRS(int cacheRowId, int id, String name) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = -1;
            this.completionValue = -1;
            this.progressVarp = -1;
            this.progressVarbit = -1;
        }

        OSRS(int cacheRowId, int id, String name, VarpID varp) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = -1;
            this.completionValue = -1;
            this.progressVarp = varp.getId();
            this.progressVarbit = -1;
        }

        OSRS(int cacheRowId, int id, String name, VarbitID varp) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = -1;
            this.completionValue = -1;
            this.progressVarp = -1;
            this.progressVarbit = varp.getId();
        }

        OSRS(int cacheRowId, int id, String name, int completionValue, VarpID varp) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = -1;
            this.completionValue = completionValue;
            this.progressVarp = varp.getId();
            this.progressVarbit = -1;
        }

        OSRS(int cacheRowId, int id, String name, int completionValue, VarbitID varp) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = -1;
            this.completionValue = completionValue;
            this.progressVarp = -1;
            this.progressVarbit = varp.getId();
        }

        OSRS(int cacheRowId, int id, String name, int completionValue) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = -1;
            this.completionValue = completionValue;
            this.progressVarp = -1;
            this.progressVarbit = -1;
        }

        OSRS(
            final int cacheRowId,
            final int id,
            final String name,
            final int progressValue,
            final int completionValue,
            final VarpID varp
        ) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = progressValue;
            this.completionValue = completionValue;
            this.progressVarp = varp.getId();
            this.progressVarbit = -1;
        }

        OSRS(
            final int cacheRowId,
            final int id,
            final String name,
            final int progressValue,
            final int completionValue,
            final VarbitID varp
        ) {
            this.cacheRowId = cacheRowId;
            this.id = id;
            this.name = name;
            this.progressValue = progressValue;
            this.completionValue = completionValue;
            this.progressVarp = -1;
            this.progressVarbit = varp.getId();
        }

        public int getCacheId() {
            return cacheRowId;
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public QuestDefinition getDefinition() {
            return QuestDefinitions.loadByCacheId(cacheRowId);
        }

        @Override
        public String getName() {
            var def = getDefinition();
            return def != null ? def.getDisplayName() : name;
        }


        /*
            def_int $int1 = db_getfield($dbrow0, quest:statusprogress, 0);
            def_int $int2 = db_getfield($dbrow0, quest:statuscomplete, 0);
            def_int $int3 = ~quest_progress_get($dbrow0);
            if ($int1 = -1 | $int2 = -1 | $int3 = -1 | $int2 <= $int1) {
            	return(1);
            }
            if ($int3 >= $int2) {
            	return(2);
            }
            if ($int3 <= $int1) {
            	return(1);
            }
            return(0);
         */
        @NonNull
        @Override
        public Status getStatus() {
            //TODO Cache by AbstactBot/alias
            final int progress = getVarProgress();
            if (progress == 0) {
                return Status.NOT_STARTED;
            }
            if (progress != -1) {
                final QuestDefinition definition = getDefinition();
                if (definition == null) {
                    return Status.UNKNOWN;
                }
                if (completionValue != -1) {
                    if (progress >= completionValue) {
                        return Status.COMPLETE;
                    }
                    if (progressValue != -1) {
                        return progress <= progressValue ? Status.NOT_STARTED : Status.IN_PROGRESS;
                    }
                } else if (definition.getCompletionStatus() != -1 && progress >= definition.getCompletionStatus()) {
                    return Status.COMPLETE;
                }
            }
            final Integer state = CS2ScriptExecutor.getQuestStatus(cacheRowId);
            return state == null ? Status.NOT_STARTED : Status.of(state);
        }

        private int getVarProgress() {
            if (progressVarbit != -1) {
                final Varbit varbit = Varbits.load(progressVarbit);
                return varbit != null ? varbit.getValue() : -1;
            }
            if (progressVarp != -1) {
                return Varps.getAt(progressVarp).getValue();
            }
            return -1;
        }

        @Override
        public boolean isMembersOnly() {
            var def = getDefinition();
            return def != null && def.isMembers();
        }

        @Override
        @RS3Only
        public int getQuestPointsRewarded() {
            throw new RS3Exception();
        }

        @Override
        @RS3Only
        public List<Attribute> getAttributes() {
            throw new RS3Exception();
        }

        @Override
        @RS3Only
        public List<Quest> getPrerequisiteQuests() {
            throw new RS3Exception();
        }
    }
}
