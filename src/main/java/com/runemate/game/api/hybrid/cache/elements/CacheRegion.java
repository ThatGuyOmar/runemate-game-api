package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.cache.*;
import com.runemate.game.cache.file.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.util.*;
import java.io.*;
import lombok.extern.log4j.*;

@Log4j2
public class CacheRegion {

    private final JS5CacheController js5Cache;
    private final boolean rs3;
    private final int x;
    private final int y;
    private final RegionFileCacheLoader<MapFileCacheExtract> mapFileLoader;
    private final RegionFileCacheLoader<LandscapeFileCacheExtract> landscapeFileLoader;
    private boolean extractedMapFile, extractedLandscapeFile;
    private MapFileCacheExtract mapFileExtract;
    private LandscapeFileCacheExtract landscapeFileExtract;
    private CollisionGrid collisionGrid;

    private CacheRegion(JS5CacheController js5Cache, boolean rs3, int x, int y) {
        this.js5Cache = js5Cache;
        this.rs3 = rs3;
        this.x = x;
        this.y = y;
        this.mapFileLoader = new RegionFileCacheLoader<>(MapFileCacheExtract.class, this, rs3);
        this.landscapeFileLoader = new RegionFileCacheLoader<>(LandscapeFileCacheExtract.class, this, rs3);
    }

    public static CacheRegion load(JS5CacheController js5Cache, boolean rs3, int mapX, int mapY) {
        return new CacheRegion(js5Cache, rs3, mapX, mapY);
    }

    public static int getId(int gridX, int gridY) {
        return gridY | (gridX << 8);
    }

    public boolean isRS3() {
        return rs3;
    }

    public int getGridX() {
        return x;
    }

    public int getGridY() {
        return y;
    }

    public int getBaseX() {
        return x * 64;
    }

    public int getBaseY() {
        return y * 64;
    }

    public boolean contains(Coordinate coordinate) {
        return coordinate.getX() >= getBaseX()
            && coordinate.getX() < getBaseX() + 64
            && coordinate.getY() >= getBaseY()
            && coordinate.getY() < getBaseY() + 64;
    }

    public int getId() {
        return CacheRegion.getId(getGridX(), getGridY());
    }

    public int getRS3Group() {
        return x | (y << 7);
    }

    @Override
    public String toString() {
        return String.format("CacheRegion(id=%s, x=%s, y=%s)", getId(), x, y);
    }

    public CollisionGrid getCollisionGrid(File map_cache, File landscape_cache, int[] xtea_keys) {
        if (collisionGrid == null) {
            MapFileCacheExtract map = getMapFileExtract(map_cache);
            if (map == null) {
                return null;
            }
            LandscapeFileCacheExtract landscape = getLandscapeFileExtract(landscape_cache, xtea_keys);
            if (landscape == null) {
                return null;
            }
            if (landscape.objects() == null) {
                return null;
            }
            collisionGrid = new CollisionGrid(4, 64, 64, map, landscape);
        }
        return collisionGrid;
    }

    public boolean hasMapFile() {
        if (rs3) {
            return mapFileLoader.retrieve(js5Cache, getRS3Group(), 3) != null;
        } else {
            ReferenceTable table = js5Cache.getIndexFile(5).getReferenceTable();
            int groupHash = DJB2Hasher.getGroupId(table, "m" + getGridX() + '_' + getGridY());
            return groupHash != -1 && table.exists(groupHash);
        }
    }

    public boolean hasLandscapeFile() throws IOException {
        if (rs3) {
            return landscapeFileLoader.retrieve(js5Cache, getRS3Group(), 0) != null;
        } else {
            ReferenceTable table = js5Cache.getIndexFile(5).getReferenceTable();
            int groupHash = DJB2Hasher.getGroupId(table, "l" + getGridX() + '_' + getGridY());
            return groupHash != -1 && table.exists(groupHash);
        }
    }

    public boolean hasMapFileBeenExtracted() {
        return extractedMapFile;
    }

    public boolean hasLandscapeFileBeenExtracted() {
        return extractedLandscapeFile;
    }

    public MapFileCacheExtract getMapFileExtract(File cache) {
        if (mapFileExtract == null && !extractedMapFile) {
            try {
                if (rs3) {
                    mapFileExtract = mapFileLoader.load(js5Cache, getRS3Group(), 3);
                    if (mapFileExtract != null) {
                        extractedMapFile = true;
                    }
                } else {
                    int file = DJB2Hasher.getGroupId(js5Cache.getIndexFile(5).getReferenceTable(), "m" + getGridX() + '_' + getGridY());
                    if (file != -1) {
                        mapFileExtract = mapFileLoader.load(js5Cache, file, 0);
                        if (mapFileExtract != null) {
                            extractedMapFile = true;
                        }
                    } else {
                        extractedMapFile = true;
                    }
                }
            } catch (IOException ioe) {
                mapFileExtract = null;
                CacheRegionExceptionLogger.writeException(rs3 ? "rs3" : "osrs", ioe, new Coordinate(getBaseX(), getBaseY(), 0));
            }
        }
        return mapFileExtract;
    }

    public LandscapeFileCacheExtract getLandscapeFileExtract(File cache, int[] xtea_keys)
        throws OutdatedXTEAKeyException, MissingXTEAKeyException {
        if (landscapeFileExtract == null && !extractedLandscapeFile) {
            ReferenceTable table = js5Cache.getIndexFile(5).getReferenceTable();
            int file = DJB2Hasher.getGroupId(table, "l" + getGridX() + '_' + getGridY());
            if (file != -1) {
                try {
                    byte[] decrypted = table.loadLandscape(file, xtea_keys).values().iterator().next();
                    landscapeFileExtract = new LandscapeFileCacheExtract(new Coordinate(getBaseX(), getBaseY(), 0), false);
                    landscapeFileExtract.decode(new Js5InputStream(decrypted));
                    if (landscapeFileExtract != null) {
                        extractedLandscapeFile = true;
                    }
                } catch (EmptyLandscapeException ele) {
                    extractedLandscapeFile = true;
                } catch (MissingXTEAKeyException | OutdatedXTEAKeyException | IOException mxke) {
                    landscapeFileExtract = null;
                    CacheRegionExceptionLogger.writeException("osrs", mxke, new Coordinate(getBaseX(), getBaseY(), 0));
                }
            } else {
                extractedLandscapeFile = true;
            }
        }
        return landscapeFileExtract;
    }
}
