package com.runemate.game.api.hybrid.entities.status;

import com.runemate.game.api.hybrid.util.*;

/**
 * A combat gauge displayed above an entities head during combat
 */
public interface CombatGauge extends Validatable {
    /**
     * Gets the percentage of the gauge that is filled up
     *
     * @return The percentage if available, otherwise -1
     */
    int getPercent();

    int getCurrentWidth();

    int getMaxWidth();
}
