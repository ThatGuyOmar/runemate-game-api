package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.awt.*;
import java.util.function.*;
import java.util.regex.*;
import javafx.scene.canvas.*;
import javax.annotation.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public class PredefinedEntity implements LocatableEntity {

    private final Type type;
    private final Area location;
    private final int id;
    private final String name;
    private final Function<GroundItemQueryBuilder, LocatableEntityQueryResults<GroundItem>> giqb;
    private final Function<GameObjectQueryBuilder, LocatableEntityQueryResults<GameObject>> goqb;
    private final Function<NpcQueryBuilder, LocatableEntityQueryResults<Npc>> nqb;
    private Model forcedModel, backupModel;
    private LocatableEntity actual;
    private GameObject.Type actualObjectType;

    public PredefinedEntity(Type type, @NonNull Coordinate location) {
        this(type, location, (String) null);
    }

    public PredefinedEntity(Type type, @NonNull Area location) {
        this(type, location, (String) null);
    }

    public PredefinedEntity(Type type, @NonNull Coordinate position, int id) {
        this(type, Area.singular(position), id);
    }

    public PredefinedEntity(Type type, @NonNull Area location, int id) {
        if (type == null) {
            throw new IllegalArgumentException("The type of a PredefinedEntity cannot be null.");
        }
        this.type = type;
        this.location = location;
        this.id = id;
        this.name = null;
        this.giqb = null;
        this.goqb = null;
        this.nqb = null;
    }

    public PredefinedEntity(Type type, @NonNull Coordinate position, String name) {
        this(type, Area.singular(position), name);
    }

    public PredefinedEntity(Type type, @NonNull Area location, String name) {
        if (type == null) {
            throw new IllegalArgumentException("The type of a PredefinedEntity cannot be null.");
        }
        this.type = type;
        this.location = location;
        this.id = -1;
        this.name = name;
        this.giqb = null;
        this.goqb = null;
        this.nqb = null;
    }

    public <QB extends QueryBuilder, QR extends QueryResults> PredefinedEntity(Type type, @NonNull Coordinate position, Function<QB, QR> query) {
        this(type, Area.singular(position), query);
    }

    public <QB extends QueryBuilder, QR extends QueryResults> PredefinedEntity(Type type, @NonNull Area location, Function<QB, QR> query) {
        if (type == null) {
            throw new IllegalArgumentException("The type of a PredefinedEntity cannot be null.");
        }
        this.type = type;
        this.location = location;
        this.id = -1;
        this.name = null;
        switch (type) {
            case GAME_OBJECT:
                giqb = null;
                goqb = (Function<GameObjectQueryBuilder, LocatableEntityQueryResults<GameObject>>) query;
                nqb = null;
                break;
            case GROUND_ITEM:
                giqb = (Function<GroundItemQueryBuilder, LocatableEntityQueryResults<GroundItem>>) query;
                goqb = null;
                nqb = null;
                break;
            case NPC:
                giqb = null;
                goqb = null;
                nqb = (Function<NpcQueryBuilder, LocatableEntityQueryResults<Npc>>) query;
                break;
            default:
                giqb = null;
                goqb = null;
                nqb = null;
        }
    }

    public static PredefinedEntity gameobject(@NonNull Coordinate position, int id) {
        return new PredefinedEntity(Type.GAME_OBJECT, position, id);
    }

    public static PredefinedEntity gameobject(@NonNull Area area, int id) {
        return new PredefinedEntity(Type.GAME_OBJECT, area, id);
    }

    public static PredefinedEntity gameobject(@NonNull Coordinate position, String name) {
        return new PredefinedEntity(Type.GAME_OBJECT, position, name);
    }

    public static PredefinedEntity gameobject(@NonNull Area area, String name) {
        return new PredefinedEntity(Type.GAME_OBJECT, area, name);
    }

    public static PredefinedEntity gameobject(
            @NonNull Coordinate position, Function<GameObjectQueryBuilder, LocatableEntityQueryResults<GameObject>> query
    ) {
        return new PredefinedEntity(Type.GAME_OBJECT, position, query);
    }

    public static PredefinedEntity gameobject(
            @NonNull Area area, Function<GameObjectQueryBuilder, LocatableEntityQueryResults<GameObject>> query
    ) {
        return new PredefinedEntity(Type.GAME_OBJECT, area, query);
    }

    public static PredefinedEntity npc(@NonNull Coordinate position, int id) {
        return new PredefinedEntity(Type.NPC, position, id);
    }

    public static PredefinedEntity npc(@NonNull Area location, int id) {
        return new PredefinedEntity(Type.NPC, location, id);
    }

    public static PredefinedEntity npc(@NonNull Coordinate position, String name) {
        return new PredefinedEntity(Type.NPC, position, name);
    }

    public static PredefinedEntity npc(@NonNull Area location, String name) {
        return new PredefinedEntity(Type.NPC, location, name);
    }

    public static PredefinedEntity npc(@NonNull Coordinate position, Function<NpcQueryBuilder, LocatableEntityQueryResults<Npc>> query) {
        return new PredefinedEntity(Type.NPC, position, query);
    }

    public static PredefinedEntity npc(
            @NonNull Area location, Function<NpcQueryBuilder, LocatableEntityQueryResults<Npc>> query
    ) {
        return new PredefinedEntity(Type.NPC, location, query);
    }

    public static PredefinedEntity grounditem(@NonNull Coordinate position, int id) {
        return new PredefinedEntity(Type.GROUND_ITEM, position, id);
    }

    public static PredefinedEntity grounditem(@NonNull Area location, int id) {
        return new PredefinedEntity(Type.GROUND_ITEM, location, id);
    }

    public static PredefinedEntity grounditem(@NonNull Coordinate position, String name) {
        return new PredefinedEntity(Type.GROUND_ITEM, position, name);
    }

    public static PredefinedEntity grounditem(@NonNull Area location, String name) {
        return new PredefinedEntity(Type.GROUND_ITEM, location, name);
    }

    public static PredefinedEntity grounditem(
            @NonNull Coordinate position, Function<GroundItemQueryBuilder, LocatableEntityQueryResults<GroundItem>> query
    ) {
        return new PredefinedEntity(Type.GROUND_ITEM, position, query);
    }

    public static PredefinedEntity grounditem(
            @NonNull Area location, Function<GroundItemQueryBuilder, LocatableEntityQueryResults<GroundItem>> query
    ) {
        return new PredefinedEntity(Type.GROUND_ITEM, location, query);
    }

    /**
     * Gets the type of this predefined entity.
     *
     * @return the PredefinedEntity.Type of this entity.
     */
    @NonNull
    public Type getType() {
        return type;
    }

    /**
     * Gets the provided name for this entity.
     *
     * @return The name if it was provided, otherwise null.
     */
    @Nullable
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    @Override
    public double getVisibility() {
        Interactable target = getModel();
        return target != null ? target.getVisibility() : location.getVisibility();
    }

    @Override
    public boolean hasDynamicBounds() {
        return true;
    }

    @NonNull
    @Override
    public Coordinate getPosition() {
        return getPosition(null);
    }

    @NonNull
    @Override
    public Coordinate getPosition(Coordinate regionBase) {
        LocatableEntity actual = resolve();
        if (actual != null) {
            Coordinate position = actual.getPosition(regionBase);
            if (position != null) {
                return position;
            }
        }
        return location.getPosition();
    }

    @NonNull
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition() {
        return getHighPrecisionPosition(null);
    }

    @NonNull
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        LocatableEntity actual = resolve();
        if (actual != null) {
            Coordinate.HighPrecision hpp = actual.getHighPrecisionPosition(regionBase);
            if (hpp != null) {
                return hpp;
            }
        }
        return location.getHighPrecisionPosition();
    }

    @NonNull
    @Override
    public Area.Rectangular getArea() {
        return getArea(null);
    }

    @NonNull
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        LocatableEntity actual = resolve();
        if (actual != null) {
            Area.Rectangular area = actual.getArea(regionBase);
            if (area != null) {
                return area;
            }
        }
        return location.toRectangular();
    }

    @Override
    public boolean isVisible() {
        Interactable target = getModel();
        return target != null ? target.isVisible() : location.isVisible();
    }

    @Override
    @Nullable
    public InteractablePoint getInteractionPoint(Point origin) {
        Interactable target = getModel();
        return target != null ? target.getInteractionPoint(origin) : location.getInteractionPoint(origin);
    }

    @Override
    public boolean contains(Point point) {
        Interactable target = getModel();
        return target != null ? target.contains(point) : location.contains(point);
    }

    @Override
    public boolean click() {
        Interactable target = getModel();
        return target != null ? target.click() : location.click();
    }

    @Override
    public boolean hover() {
        Interactable target = getModel();
        return target != null ? target.hover() : location.hover();
    }

    @Override
    public boolean interact(Pattern action, Pattern target) {
        Interactable interactable = resolve();
        return interactable != null ? interactable.interact(action, target) : location.interact(action, target);
    }

    @Override
    public void render(Graphics2D g2d) {
        LocatableEntity actual = resolve();
        if (actual != null) {
            actual.render(g2d);
        } else {
            location.render(g2d);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        location.render(gc);
    }

    @Override
    public boolean isValid() {
        LocatableEntity actual = resolve();
        if (actual != null) {
            return actual.isValid();
        }
        return location.isLoaded();
    }

    @Override
    @Nullable
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        LocatableEntity actual = resolve();
        if (actual != null) {
            Model model = actual.getModel();
            if (model != null) {
                return model;
            }
        }
        return backupModel;
    }

    @Override
    public void setBackupModel(int[] frontBottomLeft, int[] backTopRight) {
        this.backupModel = new BoundingModel(this, frontBottomLeft, backTopRight);
    }

    @Override
    public void setBackupModel(Pair<int[], int[]> values) {
        this.backupModel = new BoundingModel(this, values);
    }

    @Override
    public void setBackupModel(Model backup) {
        this.backupModel = backup;
    }

    @Override
    public void setForcedModel(int[] frontBottomLeft, int[] backTopRight) {
        this.forcedModel = new BoundingModel(this, frontBottomLeft, backTopRight);
    }

    @Override
    public void setForcedModel(Pair<int[], int[]> values) {
        this.forcedModel = new BoundingModel(this, values);
    }

    @Override
    public void setForcedModel(Model forced) {
        this.forcedModel = forced;
    }

    @Nullable
    public LocatableEntity resolve() {
        if (actual != null && !actual.isValid()) {
            actual = null;
        }
        if (actual == null) {
            switch (type) {
                case GAME_OBJECT: {
                    GameObjectQueryBuilder builder = GameObjects.newQuery().within(location);
                    if (goqb != null) {
                        actual = goqb.apply(builder).first();
                    } else {
                        if (id != -1) {
                            builder.ids(id);
                        }
                        if (name != null) {
                            builder.names(name);
                        }
                        if (actualObjectType != null) {
                            builder.types(actualObjectType);
                        }
                        LocatableEntityQueryResults<GameObject> results = builder.results();
                        if (results.size() > 1) {
                            log.warn("There are {} objects being returned for {}", results.size(), this);
                            results = results.sortByDistance();
                        }
                        actual = results.first();
                        if (actual != null) {
                            actualObjectType = ((GameObject) actual).getType();
                        }
                    }
                    break;
                }
                case NPC: {
                    NpcQueryBuilder builder = Npcs.newQuery().within(location);
                    if (nqb != null) {
                        actual = nqb.apply(builder).nearest();
                    } else {
                        if (id != -1) {
                            builder.ids(id);
                        }
                        if (name != null) {
                            builder.names(name);
                        }
                        LocatableEntityQueryResults<Npc> results = builder.results();
                        if (results.size() > 1) {
                            log.warn("There are {} npcs being returned for {}", results.size(), this);
                            results = results.sortByDistance();
                        }
                        actual = results.first();
                    }
                    break;
                }
                case GROUND_ITEM: {
                    GroundItemQueryBuilder builder = GroundItems.newQuery().within(location);
                    if (giqb != null) {
                        actual = giqb.apply(builder).nearest();
                    } else {
                        if (id != -1) {
                            builder.ids(id);
                        }
                        if (name != null) {
                            builder.names(name);
                        }
                        LocatableEntityQueryResults<GroundItem> results = builder.results();
                        if (results.size() > 1) {
                            log.warn("There are {} items being returned for {}", results.size(), this);
                            results = results.sortByDistance();
                        }
                        actual = results.first();
                    }
                    break;
                }
                default:
                    return null;
            }
        }
        return actual;
    }

    @Override
    public String toString() {
        return String.format("PredefinedEntity(type=%s, loc=%s, id=%s, name=%s)", type, location, id, name);
    }

    public enum Type {
        GAME_OBJECT,
        GROUND_ITEM,
        NPC
    }
}
