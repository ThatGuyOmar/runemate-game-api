package com.runemate.game.api.hybrid.cache.elements;

import com.google.common.primitives.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;
import lombok.*;

public class CacheObjectDefinition extends IncrementallyDecodedItem {
    private final int id;
    private final boolean rs3;
    private final String[] actions;
    public String name = "null";
    public boolean hideableGroundDecoration = false;
    private int insets;
    private int interactable = -1;
    private int[] modelTypes;
    private int[][] objectModelIds;
    private int modelXScale;
    private int modelYScale;
    private int modelZScale;
    private int reachableState = 2;
    private int width = 1;
    private int height = 1;
    private int varbitIndex = -1;
    private int varpIndex = -1;
    private int[] transformationIds;
    private boolean isObjectBlocked = true;
    private boolean impenetrable = false;
    private Integer[] animationIds;
    private HashMap<Long, Attribute> attributes;
    private short[] originalColors, modifiedColors;
    private short[] originalTextureIds, modifiedTextureIds;
    private boolean membersOnly;
    private int mapFunctionSpriteIndex = -1;
    private int mapSceneImageIndex = -1;
    private boolean mirrorModel = false;
    private boolean castsShadow = true;
    private int hillChange = -1;
    private boolean sharesLighting = false;
    private boolean occludes = false;
    private int modelXTranslation = 0;
    private int counterType = -1;
    private int modelYTranslation = 0;
    private int modelZTranslation = 0;
    private int[] questIds;
    private int[] actionCursorIds;
    private byte meshMergeType = 0;
    private boolean invertOneOfMapSceneOrFunction;
    private int accessibleMask = -1;

    public CacheObjectDefinition(int id) {
        this.rs3 = false;
        this.id = id;
        actions = new String[5];
        modelXScale = 128;
        modelYScale = 128;
        modelZScale = 128;
        insets = 64;//A complete guess at the default value
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            //modelTypes & modelIds
            int length = stream.readUnsignedByte();
            if (length > 0) {
                if (rs3) {
                    objectModelIds = new int[length][];
                    modelTypes = new int[length];
                    for (int i = 0; i < length; ++i) {
                        modelTypes[i] = stream.readByte();
                        int length2 = stream.readUnsignedByte();
                        objectModelIds[i] = new int[length2];
                        for (int j = 0; j < length2; j++) {
                            objectModelIds[i][j] = stream.readDefaultableUnsignedSmart();
                        }
                    }
                } else {
                    if (objectModelIds != null/*&& !useLowerResolutionTextures*/) {
                        stream.skip(3 * length);
                    } else {
                        objectModelIds = new int[1][length];
                        modelTypes = new int[length];
                        for (int index = 0; index < length; ++index) {
                            objectModelIds[0][index] = stream.readUnsignedShort();
                            modelTypes[index] = stream.readUnsignedByte();
                        }
                    }
                }
            }
        } else if (opcode == 2) {
            name = stream.readCStyleLatin1String();
        } else if (opcode == 5) {
            int length = stream.readUnsignedByte();
            if (length > 0) {
                if (this.objectModelIds != null/*&& !useLowerResolutionTextures*/) {
                    stream.skipBytes(2 * length);
                } else {
                    this.modelTypes = null;
                    this.objectModelIds = new int[1][length];
                    for (int j = 0; j < length; ++j) {
                        this.objectModelIds[0][j] = stream.readUnsignedShort();
                    }
                }
            }
        } else if (opcode == 14) {
            width = stream.readUnsignedByte();
        } else if (opcode == 15) {
            height = stream.readUnsignedByte();
        } else if (opcode == 17) {
            reachableState = 0;
            isObjectBlocked = false;
        } else if (opcode == 18) {
            isObjectBlocked = false;
        } else if (opcode == 19) {
            interactable = stream.readUnsignedByte();
        } else if (opcode == 21) {
            meshMergeType = 1;
            hillChange = 0;
        } else if (opcode == 22) {
            sharesLighting = true;
        } else if (opcode == 23) {
            occludes = true;
        } else if (opcode == 24) {
            int animationId;
            if (rs3) {
                animationId = stream.readDefaultableUnsignedSmart();
            } else {
                animationId = stream.readUnsignedShort();
                if (animationId == 0xFFFF) {
                    animationId = -1;
                }
            }
            if (animationId != -1) {
                animationIds = new Integer[] { animationId };
            }
        } else if (opcode == 27) {
            reachableState = 1;
        } else if (opcode == 28) {
            insets = stream.readUnsignedByte();
            if (rs3) {
                insets <<= 2;
            }
        } else if (opcode == 29) {
            int lightingDelta = stream.readByte();
        } else if (opcode == 39) {
            int intensityDelta = stream.readByte();
        } else if (opcode >= 30 && opcode < 35) {
            actions[opcode - 30] = stream.readCStyleLatin1String();
            if ("Hidden".equalsIgnoreCase(actions[opcode - 30])) {
                actions[opcode - 30] = null;
            }
        } else if (opcode == 40) {
            int length = stream.readUnsignedByte();
            originalColors = new short[length];
            modifiedColors = new short[length];
            for (int index = 0; index < length; index++) {
                originalColors[index] = (short) stream.readUnsignedShort();
                modifiedColors[index] = (short) stream.readUnsignedShort();
            }
        } else if (opcode == 41) {
            int length = stream.readUnsignedByte();
            originalTextureIds = new short[length];
            modifiedTextureIds = new short[length];
            for (int index = 0; index < length; index++) {
                originalTextureIds[index] = (short) stream.readUnsignedShort();
                modifiedTextureIds[index] = (short) stream.readUnsignedShort();
            }
        } else if (opcode == 42) {
            int length = stream.readUnsignedByte();
            for (int index = 0; index < length; index++) {
                stream.readByte();
            }
        } else if (opcode == 44 || opcode == 45) {
            stream.readUnsignedShort();
        } else if (opcode == 60) {
            mapFunctionSpriteIndex = stream.readUnsignedShort();
        } else if (opcode == 61) {
            stream.readUnsignedShort();
        } else if (opcode == 62) {
            mirrorModel = true;
        } else if (opcode == 64) {
            castsShadow = false;
        } else if (opcode == 65) {
            modelXScale = stream.readUnsignedShort();
        } else if (opcode == 66) {
            modelYScale = stream.readUnsignedShort();
        } else if (opcode == 67) {
            modelZScale = stream.readUnsignedShort();
        } else if (opcode == 68) {
            mapSceneImageIndex = stream.readUnsignedShort();
        } else if (opcode == 69) {
            accessibleMask = stream.readUnsignedByte();
        } else if (opcode == 70) {
            modelXTranslation = stream.readShort();
            if (rs3) {
                modelXTranslation <<= 2;
            }
        } else if (opcode == 71) {
            modelYTranslation = stream.readShort();
            if (rs3) {
                modelYTranslation <<= 2;
            }
        } else if (opcode == 72) {
            modelZTranslation = stream.readShort();
            if (rs3) {
                modelZTranslation <<= 2;
            }
        } else if (opcode == 73) {
            hideableGroundDecoration = true;
        } else if (opcode == 74) {
            impenetrable = true;
        } else if (opcode == 75) {
            counterType = stream.readUnsignedByte();
        } else if (opcode == 77 || opcode == 92) {
            varbitIndex = stream.readUnsignedShort();
            if (varbitIndex == 0xffff) {
                varbitIndex = -1;
            }
            varpIndex = stream.readUnsignedShort();
            if (varpIndex == 0xffff) {
                varpIndex = -1;
            }
            int fallback = -1;
            if (opcode == 92) {
                if (rs3) {
                    fallback = stream.readDefaultableUnsignedSmart();
                } else {
                    fallback = stream.readUnsignedShort();
                    if (fallback == 0xffff) {
                        fallback = -1;
                    }
                }
            }
            int length = rs3 ? stream.readShortSmart() : stream.readUnsignedByte();
            transformationIds = new int[length + 2];
            for (int index = 0; index <= length; ++index) {
                if (rs3) {
                    transformationIds[index] = stream.readDefaultableUnsignedSmart();
                } else {
                    transformationIds[index] = stream.readUnsignedShort();
                    if (transformationIds[index] == 0xffff) {
                        transformationIds[index] = -1;
                    }
                }
            }
            transformationIds[length + 1] = fallback;
        } else if (opcode == 78) {
            int continousSoundEffectId = stream.readUnsignedShort();
            int maxSoundEffectRadius = stream.readUnsignedByte();
        } else if (opcode == 79) {
            int minSoundEffectTime = stream.readUnsignedShort();
            int maxSoundEffectTime = stream.readUnsignedShort();
            int maxSoundEffectRadius = stream.readUnsignedByte();
            int length = stream.readUnsignedByte();
            int[] soundEffectIds = new int[length];
            for (int index = 0; index < length; index++) {
                soundEffectIds[index] = stream.readUnsignedShort();
            }
        } else if (opcode == 81) {
            meshMergeType = 2;
            hillChange = stream.readUnsignedByte() << 8;//May not need shift?shift differs on rs3?
        } else if (opcode == 82) {
            if (!rs3) {
                mapFunctionSpriteIndex = stream.readUnsignedShort();
            }
        } else if (opcode == 88 || opcode == 89) {
        } else if (opcode == 91) {
            membersOnly = true;
        } else if (opcode == 93) {
            meshMergeType = 3;
            hillChange = stream.readUnsignedShort();//Missing a shift?
        } else if (opcode == 94) {
            meshMergeType = 4;
        } else if (opcode == 95) {
            meshMergeType = 5;
            hillChange = stream.readShort();//Missing a shift?, wrong read?
        } else if (opcode == 97 || opcode == 98) {
        } else if (opcode == 99 || opcode == 100) {//Unused
            stream.readUnsignedByte();
            stream.readUnsignedShort();
        } else if (opcode == 101) {
            stream.readUnsignedByte();
        } else if (opcode == 102) {
            mapFunctionSpriteIndex = stream.readUnsignedShort();
        } else if (opcode == 103) {
            //occludes apparently has at least 3 values in rs3
        } else if (opcode == 104) {
            stream.readUnsignedByte();
        } else if (opcode == 105) {
            invertOneOfMapSceneOrFunction = true;
        } else if (opcode == 106) {
            int size = stream.readUnsignedByte();
            animationIds = new Integer[size];
            for (int index = 0; index < size; ++index) {
                animationIds[index] = stream.readDefaultableUnsignedSmart();
                stream.readUnsignedByte();//TODO something is missing here on rs3
            }
        } else if (opcode == 107) {
            mapSceneImageIndex = stream.readUnsignedShort();
        } else if (opcode >= 150 && opcode < 155) {
            actions[opcode - 150] = stream.readCStyleLatin1String();
        } else if (opcode == 160) {//(Cursor IDs?)
            int length = stream.readUnsignedByte();
            questIds = new int[length];
            for (int index = 0; index < length; index++) {
                questIds[index] = stream.readUnsignedShort();
            }
        } else if (opcode == 162) {
            meshMergeType = 3;
            hillChange = stream.readInt() << 8;
        } else if (opcode == 163) {
            stream.readByte();
            stream.readByte();
            stream.readByte();
            stream.readByte();
        } else if (opcode == 164 || opcode == 165 || opcode == 166) {
            stream.readShort();
        } else if (opcode == 167) {
            /*modelHeight =*/
            stream.readUnsignedShort();
        } else if (opcode == 168 || opcode == 169) {
        } else if (opcode == 170 || opcode == 171) {
            stream.readShortSmart();
        } else if (opcode == 173) {
            stream.readUnsignedShort();
            stream.readUnsignedShort();
        } else if (opcode == 177) {
        } else if (opcode == 178 || opcode == 186) {
            stream.readUnsignedByte();
        } else if (opcode == 188 || opcode == 189) {
        } else if (opcode >= 190 && opcode < 196) {
            if (null == actionCursorIds) {
                actionCursorIds = new int[6];
                Arrays.fill(actionCursorIds, -1);
            }
            actionCursorIds[opcode - 190] = stream.readUnsignedShort();
        } else if (opcode == 196 || opcode == 197) {
            stream.readUnsignedByte();
        } else if (opcode == 198 || opcode == 199 || opcode == 200) {
        } else if (opcode == 201) {
            //not sure what those names mean but powerbot has them named that way...
            stream.readQuarterBoundSmart();//xStart?
            stream.readQuarterBoundSmart();//yStart?
            stream.readQuarterBoundSmart();//zStart?
            stream.readQuarterBoundSmart();//xStop?
            stream.readQuarterBoundSmart();//yStop?
            stream.readQuarterBoundSmart();//zStop?
        } else if (opcode == 249) {
            int length = stream.readUnsignedByte();
            if (attributes == null) {
                attributes = new HashMap<>(length);
            }
            for (int index = 0; index < length; index++) {
                boolean stringInstance = stream.readUnsignedByte() == 1;
                int key = stream.readBytes(3);
                Object value = stringInstance ? stream.readCStyleLatin1String() : stream.readInt();
                attributes.put((long) key, new Attribute(key, value));
            }
        } else {
            //throw new DecodingException(stream.getBackingArray());
            throw new DecodingException(opcode);
        }
    }

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        super.decode(stream);
        if (this.interactable == -1) {
            this.interactable = 0;
            if (this.objectModelIds != null &&
                (this.modelTypes == null || this.modelTypes[0] == 10)) {
                this.interactable = 1;
            }

            for (int index = 0; index < 5; ++index) {
                if (this.actions[index] != null) {
                    this.interactable = 1;
                    break;
                }
            }
        }
        if (counterType == -1) {
            counterType = reachableState != 0 ? 1 : 0;
        }
        if (impenetrable) {
            reachableState = 0;
            isObjectBlocked = false;
        }
    }

    public GameObjectDefinition extended() {
        return new Extended();
    }

    public class Extended extends GameObjectDefinition {
        private Extended() {
        }

        @Override
        public final GameObjectDefinition getLocalState() {
            if (transformationIds != null) {
                int index = -1;
                if (varbitIndex != -1) {
                    Varbit vb = Varbits.load(varbitIndex);
                    if (vb != null) {
                        index = vb.getValue();
                    }
                } else if (varpIndex != -1) {
                    index = Varps.getAt(varpIndex).getValue();
                }
                int transformationId;
                if (index >= 0 && index < transformationIds.length - 1) {
                    transformationId = transformationIds[index];
                } else {
                    transformationId = transformationIds[transformationIds.length - 1];
                }
                return transformationId != -1 ? get(transformationId) : null;
            }
            return null;
        }

        @NonNull
        @Override
        public Collection<GameObjectDefinition> getTransformations() {
            if (transformationIds != null) {
                Set<Integer> ids = new HashSet<>(transformationIds.length);
                Collection<GameObjectDefinition> transformations =
                    new ArrayList<>(transformationIds.length);
                for (int childId : transformationIds) {
                    if (childId != -1) {
                        if (ids.contains(childId)) {
                            continue;
                        }
                        ids.add(childId);
                        GameObjectDefinition transformation = get(childId);
                        if (transformation != null) {
                            transformations.add(transformation);
                        }
                    }
                }
                return transformations;
            }
            return Collections.emptyList();
        }

        @NonNull
        @Override
        public List<String> getActions() {
            final List<String> actions = new ArrayList<>(5);
            for (final String action : CacheObjectDefinition.this.actions) {
                if (action != null && !action.isEmpty()) {
                    actions.add(action);
                }
            }
            return actions;
        }

        @Override
        public String[] getRawActions() {
            return CacheObjectDefinition.this.actions;
        }

        @NonNull
        @Override
        public List<Integer> getAnimationIds() {
            if (animationIds == null) {
                return Collections.emptyList();
            }
            return Arrays.asList(animationIds);
        }

        @NonNull
        @Override
        public List<Integer> getAppearance() {
            if (objectModelIds == null) {
                return Collections.emptyList();
            }
            List<Integer> list = new ArrayList<>(objectModelIds.length);
            for (int[] models : objectModelIds) {
                list.addAll(Ints.asList(models));
            }
            return list;
        }

        public int[] transformationIds() {
            return transformationIds;
        }

        @Override
        public int getClippingType() {
            return reachableState;
        }

        @Override
        public int getWidth() {
            return width;
        }

        @Override
        public int getHeight() {
            return height;
        }

        @Override
        public boolean impassable() {
            return isObjectBlocked;
        }

        @Override
        public Attribute getAttribute(long id) {
            if (attributes == null) {
                return null;
            }
            return attributes.get(id);
        }

        @Override
        public List<Attribute> getAttributes() {
            if (attributes == null) {
                return Collections.emptyList();
            }
            return new ArrayList<>(attributes.values());
        }

        @Override
        public Map<Color, Color> getColorSubstitutions() {
            if (originalColors == null) {
                return Collections.emptyMap();
            }
            Map<Color, Color> colorMapping = new HashMap<>(originalColors.length);
            for (int i = 0; i < originalColors.length; ++i) {
                colorMapping.put(
                    RSColors.fromHSV(originalColors[i]),
                    RSColors.fromHSV(modifiedColors[i])
                );
            }
            return colorMapping;
        }

        @Override
        public Map<Material, Material> getMaterialSubstitutions() {
            if (originalTextureIds == null) {
                return Collections.emptyMap();
            }
            Map<Material, Material> textureMapping = new HashMap<>(originalTextureIds.length);
            for (int i = 0; i < originalTextureIds.length; ++i) {
                textureMapping.put(
                    Materials.load(originalTextureIds[i]),
                    Materials.load(modifiedTextureIds[i])
                );
            }
            return textureMapping;
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public int getMapFunction() {
            return mapFunctionSpriteIndex;
        }

        @Override
        public int getMapScene() {
            return mapSceneImageIndex;
        }

        @NonNull
        @Override
        public String getName() {
            return "null".equals(name) ? name : JagTags.remove(name);
        }

        public int[][] getModelIds() {
            return objectModelIds;
        }

        @Override
        public int[] getModelTypes() {
            return modelTypes;
        }

        @Override
        public boolean impenetrable() {
            return impenetrable;
        }

        @Override
        public int getModelXScale() {
            return modelXScale;
        }

        @Override
        public int getModelYScale() {
            return modelYScale;
        }

        @Override
        public int getModelZScale() {
            return modelZScale;
        }

        @Override
        public int getModelXTranslation() {
            return modelXTranslation;
        }

        @Override
        public int getModelYTranslation() {
            return modelYTranslation;
        }

        @Override
        public int getModelZTranslation() {
            return modelZTranslation;
        }

        @Override
        public boolean isModelMirrorable() {
            return mirrorModel;
        }

        @Override
        public boolean isInteractable() {
            return interactable == 1;
        }

        @Override
        public Varp getStateVarp() {
            return varpIndex != -1 ? Varps.getAt(varpIndex) : null;
        }

        @Override
        public Varbit getStateVarbit() {
            return varbitIndex != -1 ? Varbits.load(varbitIndex) : null;
        }

        //@Override
        // public int getInset() {
        //     return insets;
        //}
    }
}
