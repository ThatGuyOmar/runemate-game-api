package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import java.util.*;
import java.util.function.*;

public class Locatables {

    public static <T extends Locatable> Predicate<T> getAreaPredicate(Area... areas) {
        return locatable -> Arrays.stream(areas).anyMatch(area -> area.contains(locatable));
    }
}
