package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.*;
import java.io.*;
import lombok.*;

public class VarpRequirement extends WebRequirement implements SerializableRequirement {
    private int varpIndex;
    private int mask;
    private int desiredResult;

    public VarpRequirement(int varpIndex, int mask) {
        this(varpIndex, mask, mask);
    }

    public VarpRequirement(int varpIndex, int mask, int desiredResult) {
        this.varpIndex = varpIndex;
        this.mask = mask;
        this.desiredResult = desiredResult;
    }

    public VarpRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    @Override
    public boolean isMet0() {
        return (Varps.getAt(varpIndex).getValue() & mask) == desiredResult;
    }

    public int getVarpIndex() {
        return varpIndex;
    }

    public int getMask() {
        return mask;
    }

    @Override
    public String toString() {
        return "VarpRequirement(" + varpIndex + " & 0x" + Integer.toHexString(mask) + " == " +
            desiredResult + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof VarpRequirement) {
            VarpRequirement that = (VarpRequirement) o;
            return varpIndex == that.varpIndex && mask == that.mask;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = varpIndex;
        result = 31 * result + mask;
        return result;
    }

    @Override
    public int getOpcode() {
        return 1;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeInt(varpIndex);
        stream.writeInt(mask);
        stream.writeInt(desiredResult);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        varpIndex = stream.readInt();
        mask = stream.readInt();
        if (protocol <= 6) {
            desiredResult = mask;
        } else {
            desiredResult = stream.readInt();
        }
        return true;
    }
}
