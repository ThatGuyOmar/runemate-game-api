package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.cache.item.*;
import java.awt.*;

public abstract class CacheMaterial implements Material, DecodedItem {
    @Override
    public String toString() {
        Color color = getColor();
        return "Material{" + "id=" + getId()
            + ", color={" + (
            color == null ? "null" : color.getRed()
                + "," + color.getGreen()
                + "," + color.getBlue()
        ) + "}"
            + ", edgeLength=" + getEdgeLength()
            + ", mipmapLevel=" + getMipmapLevel() + '}';
    }
}
