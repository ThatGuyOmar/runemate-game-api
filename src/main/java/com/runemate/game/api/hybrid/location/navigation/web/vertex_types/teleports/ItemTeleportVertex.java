package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public abstract class ItemTeleportVertex extends TeleportVertex
    implements FilterableVertex<SpriteItem> {

    protected List<SpriteItem> provider;

    public ItemTeleportVertex(
        final Coordinate destination,
        final Collection<WebRequirement> conditions
    ) {
        super(destination, conditions);
    }

    public ItemTeleportVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public abstract SpriteItem getItem();

    @Override
    public void setProvider(List<SpriteItem> provider) {
        this.provider = provider;
    }

    @NonNull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        if (getPosition().isVisible()) {
            return new Pair<>(null, WebPath.VertexSearchAction.CONTINUE);
        }
        if (args.containsKey(ITEM_CACHE)) {
            setProvider((List<SpriteItem>) args.get(ITEM_CACHE));
        }
        final SpriteItem item = getItem();
        return new Pair<>(item != null ? this : null, WebPath.VertexSearchAction.CONTINUE);
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "ItemTeleportVertex(" + position.getX() + ", " + position.getY() + ", " +
            position.getPlane() + ')';
    }
}
