package com.runemate.game.api.hybrid.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public final class Region {
    private Region() {
    }

    /**
     * Previously offered a passive mechanism to cache collision flags on RS3 which had very high overhead for building collision flags.
     * Now that it's a sub 100ms calculation (and dropping more), this functionality is no longer needed and attempting to determine whether or not to invalidate the cache takes longer than building a new one.
     *
     * @see Region#getCollisionFlags()
     */
    @Deprecated
    public static void cacheCollisionFlags(boolean enable) {
    }


    public static Area.Rectangular getArea() {
        final Coordinate base = getBase();
        return new Area.Rectangular(base, base.derive(getWidth() - 1, getHeight() - 1));
    }

    /**
     * By default, looks up the result of the last region update from {@link com.runemate.game.api.script.framework.listeners.RegionListener}.
     * Otherwise, uses {@link #getCurrentBase()}.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     */
    public static Coordinate getBase() {
        return OSRSRegion.getBase();
    }

    /**
     * Retrieves the base of the current loaded map directly from the game, updating the cache used by {@link #getBase()}
     * if necessary.
     *
     * @return the base of the current loaded map (plane = local player's plane).
     */
    public static Coordinate getCurrentBase() {
        var base = OSRSRegion.getCurrentBase();

        //Base can get sometimes get desynced on teleport/death/other (cause unknown), causing functionality that relies on it to fail.
        if (!Objects.equals(base, getBase())) {
            log.debug("Current base differs from cached base, updating it.");
            OSRSRegion.setStoredBase(base);
        }
        return base;
    }


    @Nullable
    public static int[][][] getCollisionFlags() {
        return getCollisionFlagsFromHook();
    }


    @Nullable
    public static int[][] getCollisionFlags(int plane) {
        return getCollisionFlagsFromHook(plane);
    }


    private static int[][] getCollisionFlagsFromHook(int plane) {
        return OpenRegion.getFlags(plane);
    }


    private static int[][][] getCollisionFlagsFromHook() {
        return OpenRegion.getFlags();
    }

    public static int getCurrentPlane() {
        return OSRSRegion.getCurrentPlane();
    }

    /**
     * Gets the depth (quantity of planes)
     *
     * @see Region#getFloors
     */
    @Deprecated
    public static int getDepth() {
        return getFloors();
    }

    /**
     * Gets the amount of floors available starting at 0.
     */
    public static int getFloors() {
        return 4;
    }

    /**
     * Gets the height (y size)
     */
    public static int getHeight() {
        return 104;
    }

    public static Coordinate.HighPrecision getHighPrecisionBase() {
        final Coordinate base = getBase();
        if (base != null) {
            final int shift = 7;
            return new Coordinate.HighPrecision(base.getX() << shift, base.getY() << shift,
                base.getPlane()
            );
        } else {
            return null;
        }
    }

    public static int[] getLoadedRegionIds() {
        return OpenRegion.getLoadedRegionIds();
    }

    /**
     * Gets with width (x size)
     */
    public static int getWidth() {
        return 104;
    }

    /**
     * Gets if the current bot is using cached collision flags
     */
    @Deprecated
    public static boolean isCachingCollisionFlags() {
        return false;
    }

    /**
     * @deprecated Region#isInstanced()
     */
    @Deprecated
    public static boolean isUnique() {
        return isInstanced();
    }

    public static boolean isInstanced() {
        return OpenRegion.isInstanced();
    }

    @RS3Only
    public static boolean isShardedInstance() {
        return false;
    }

    /**
     * Contains the raw data (as it is represented in the game client) that defines the currently loaded instance template chunks.
     *
     * @see Template
     * @see #getTemplates()
     */
    public static int[][][] getInstanceTemplateChunks() {
        return OpenRegion.getInstanceTemplateChunks();
    }

    public static List<Template> getTemplates() {
        List<Template> templates = new ArrayList<>();
        int[][][] chunks3d = getInstanceTemplateChunks();
        for (int[][] chunks2d : chunks3d) {
            for (int[] chunks : chunks2d) {
                for (int templateId : chunks) {
                    if (templateId > 0) {
                        templates.add(new Template(templateId));
                    }
                }
            }
        }
        return templates;
    }

    public static Template getTemplate(int localX, int localY, int plane) {
        if (!Region.isInstanced()) {
            return null;
        }
        var templates = getInstanceTemplateChunks();
        return new Template(templates[plane][localX / 8][localY / 8]);
    }

    public static Template getTemplate(Coordinate loaded) {
        if (!isInstanced()) {
            return null;
        }
        Coordinate.RegionOffset offset = loaded.offset();
        if (!offset.isValid()) {
            return null;
        }
        return getTemplate(offset.getX(), offset.getY(), loaded.getPlane());
    }

    private static int rotateTemplateX(int chunkX, int chunkY, int rotation) {
        switch (rotation) {
            case 0:
                return chunkX;
            case 1:
                return chunkY;
            case 2:
                return (8 - 1) - chunkX;
            case 3:
                return (8 - 1) - chunkY;
        }
        throw new IllegalStateException(
            "Cannot rotate because the templates orientation is outside the bounds of [0,3]");
    }

    private static int rotateTemplateY(int chunkX, int chunkY, int rotation) {
        switch (rotation) {
            case 0:
                return chunkY;
            case 1:
                return (8 - 1) - chunkX;
            case 2:
                return (8 - 1) - chunkY;
            case 3:
                return chunkX;
        }
        throw new IllegalStateException(
            "Cannot rotate because the templates orientation is outside the bounds of [0,3]");
    }

    public static byte[][][] getRenderFlags() {
        return OpenRegion.getLandscapeData();
    }

    public static int[][][] getHeights() {
        return OpenRegion.getTileHeights();
    }

    @Nullable
    public static Coordinate getBase(int plane) {
        return OSRSRegion.getBase(plane);
    }

    public static List<Entity> getHoveredEntities() {
        return OSRSRegion.getHoveredEntities();
    }

    public interface CollisionFlags {
        //Legacy
        int NORTH_WEST_BOUNDARY_OBJECT = 0b1;
        int NORTH_BOUNDARY_OBJECT = 0x2;
        int NORTH_EAST_BOUNDARY_OBJECT = 0x4;
        int EAST_BOUNDARY_OBJECT = 0b1000;
        int SOUTH_EAST_BOUNDARY_OBJECT = 0b10000;
        int SOUTH_BOUNDARY_OBJECT = 0x20;
        int SOUTH_WEST_BOUNDARY_OBJECT = 0x40;
        int WEST_BOUNDARY_OBJECT = 0x80;
        int OBJECT_TILE = 0x100;
        int RANGE_BLOCKING_NORTH_WEST_BOUNDARY_OBJECT = 0b1000000000;
        int RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT = 0b10000000000;
        int RANGE_BLOCKING_NORTH_EAST_BOUNDARY_OBJECT = 0b100000000000;
        int RANGE_BLOCKING_EAST_BOUNDARY_OBJECT = 0b1000000000000;
        int RANGE_BLOCKING_SOUTH_EAST_BOUNDARY_OBJECT = 0b10000000000000;
        int RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT = 0b100000000000000;
        int RANGE_BLOCKING_SOUTH_WEST_BOUNDARY_OBJECT = 0b1000000000000000;
        int RANGE_BLOCKING_WEST_BOUNDARY_OBJECT = 0b10000000000000000;
        int UNSTEPPABLE_OBJECT = 0x20000;
        int BLOCKING_FLOOR_OBJECT = 0x40000;
        /**
         * Clipped
         */
        int BLOCKED_TILE = 0b1000000000000000000000;
        int RANGE_ALLOWING_NORTH_WEST_BOUNDARY_OBJECT = 0b10000000000000000000000;
        int RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT = 0b100000000000000000000000;
        int RANGE_ALLOWING_NORTH_EAST_BOUNDARY_OBJECT = 0b1000000000000000000000000;
        int RANGE_ALLOWING_EAST_BOUNDARY_OBJECT = 0b10000000000000000000000000;
        int RANGE_ALLOWING_SOUTH_EAST_BOUNDARY_OBJECT = 0b100000000000000000000000000;
        int RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT = 0b1000000000000000000000000000;
        int RANGE_ALLOWING_SOUTH_WEST_BOUNDARY_OBJECT = 0b10000000000000000000000000000;
        int RANGE_ALLOWING_WEST_BOUNDARY_OBJECT = 0b100000000000000000000000000000;
        int RANGEABLE_OBJECT = 0x40000000;
        int PADDING = 0b11111111111111111111111111111111;

        interface HelperFlags {
            int BLOCKED_OFF = OBJECT_TILE | BLOCKED_TILE | BLOCKING_FLOOR_OBJECT;
        }
    }

    public interface RenderFlags {
        //Such as "air" (ex: area around lumbridge castle, planes 1+) and water
        int CLIPPED = 0x1;
        //Not the proper name but I need it available here and haven't found time to debug it properly
        int LOWER_OBJECTS_TO_OVERRIDE_CLIPPING = 0x2;
        int UNDER_ROOF = 0x4;
        int FORCE_TO_BOTTOM = 0x8;
        int ROOF = 0x10;
        //who knows...
        //int FORCE_RENDER_PLANE_0 = 0x8;
        //int RENDER_ABOVE_PLANE= 0x8 according to runelites instance minimap code....
        //Possibly don't add objects

        /*
        static boolean canAddObjects(int y, int player_plane, int object_plane, int x) {
            if ((RenderFlags.LOWER_OBJECTS_TO_OVERRIDE_CLIPPING & renderFlags[0][x][y]) != 0) {
                return true;
            }
            if ((RenderFlags.ROOF & renderFlags[object_plane][x][y]) != 0) {
                return false;
            }
            return getPlaneToRenderOn(object_plane, x, y) == player_plane;
        }

        static int getPlaneToRenderOn(int plane, int x, int y) {
            if ((renderFlags[plane][x][y] & TheorizedFlags.FORCE_RENDER_PLANE_0) != 0) {
                return 0;
            }
            if (plane > 0 && (TheorizedFlags.LOWER_OBJECTS_TO_OVERRIDE_CLIPPING & renderFlags[1][x][y]) != 0) {
                return plane - 1;
            } else {
                return plane;
            }
        }
        */
    }

    public static class Template {
        /**
         * For reference, a chunk is an 8x8 map sub-region
         */
        private final int id, plane, x, y, orientation;

        public Template(int templateId) {
            id = templateId;
            orientation = templateId >> 1 & 0x3;
            y = (templateId >> 3 & 0x7FF) * 8;
            x = (templateId >> 14 & 0x3FF) * 8;
            plane = templateId >> 24 & 0x3;
        }

        public int getOrientation() {
            return orientation;
        }

        public int getId() {
            return id;
        }

        public Coordinate getBase() {
            return new Coordinate(x, y, plane);
        }

        public Area.Rectangular getArea() {
            Coordinate base = getBase();
            return Area.rectangular(base, base.derive(7, 7));
        }

        public boolean contains(Coordinate coordinate) {
            Area.Rectangular area = getArea();
            return area.contains(coordinate) || area.contains(fromGlobalCoordinate(coordinate));
        }

        public Coordinate fromGlobalCoordinate(Coordinate loaded) {
            Coordinate.RegionOffset offset = loaded.offset();
            if (!offset.isValid()) {
                return null;
            }
            int chunkX = offset.getX() & (8 - 1);
            int chunkY = offset.getY() & (8 - 1);
            int rotation = (4 - orientation) % 4;
            return new Coordinate(x + rotateTemplateX(chunkX, chunkY, rotation),
                y + rotateTemplateY(chunkX, chunkY, rotation), plane
            );
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Template && ((Template) obj).id == id;
        }

        @Override
        public String toString() {
            return "Template[x=" + x
                + ", y=" + y
                + ", plane=" + plane
                + ", orientation=" + orientation + "]";
        }
    }
}
