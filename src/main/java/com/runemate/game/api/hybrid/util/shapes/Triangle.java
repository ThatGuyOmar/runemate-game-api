package com.runemate.game.api.hybrid.util.shapes;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.util.*;
import java.awt.*;
import java.util.List;
import java.util.*;

public class Triangle implements Validatable {
    private final Point a, b, c;
    private InteractableRectangle cached_bounding_box;
    private double cachedContainmentArea = Double.NaN;
    private boolean negative_area;
    private double cachedArea = Double.NaN;
    private double cachedLongestSideLength = Double.NaN;
    private Line cachedLongestSide = null;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private static double getSlope(Point a, Point b) {
        if (a.x == b.x) {
            return 0;
        }
        return (b.y - a.y) / (double) (b.x - a.x);
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }

    public boolean contains(Point p) {
        return contains(p.x, p.y);
    }

    public boolean contains(int x, int y) {
        if (Double.isNaN(cachedContainmentArea)) {
            double area = -b.y * c.x + a.y * (-b.x + c.x) + a.x * (b.y - c.y) + b.x * c.y;
            if (area < 0) {
                area = -area;
                negative_area = true;
            } else {
                negative_area = false;
            }
            this.cachedContainmentArea = area;
        }
        double s = a.y * c.x - a.x * c.y + (c.y - a.y) * x + (a.x - c.x) * y;
        if (negative_area) {
            s = -s;
        }
        if (s <= 0) {
            return false;
        }
        double t = a.x * b.y - a.y * b.x + (a.y - b.y) * x + (b.x - a.x) * y;
        if (negative_area) {
            t = -t;
        }
        return t > 0 && s + t < cachedContainmentArea;
    }

    public double getArea() {
        if (!Double.isNaN(cachedArea)) {
            return cachedArea;
        }
        double signless = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) / 2.0d;
        //Inlined Math.abs(double)
        return cachedArea = (signless <= 0.0D ? 0.0D - signless : signless);
    }

    public double getLongestSidesLength() {
        if (!Double.isNaN(cachedLongestSideLength)) {
            return cachedLongestSideLength;
        }
        double sideA1Length = Math.sqrt((a.x - b.x) ^ 2 + (a.y - b.y) ^ 2);
        double sideB1Length = Math.sqrt((b.x - c.x) ^ 2 + (b.y - c.y) ^ 2);
        double sideC1Length = Math.sqrt((c.x - a.x) ^ 2 + (c.y - a.y) ^ 2);
        return cachedLongestSideLength =
            Math.max(sideA1Length, Math.max(sideB1Length, sideC1Length));
    }

    public Line getLongestSide() {
        if (cachedLongestSide != null) {
            return cachedLongestSide;
        }
        Line longestSide = null;
        double longestLength = 0;
        for (Line edge : getSides()) {
            double length = edge.getLength();
            if (length > longestLength) {
                longestSide = edge;
                longestLength = length;
            }
        }
        return cachedLongestSide = longestSide;
    }

    public Polygon toPolygon() {
        return new Polygon(new int[] { a.x, b.x, c.x }, new int[] { a.y, b.y, c.y }, 3);
    }

    public List<Line> getSides() {
        return Arrays.asList(new Line(a, b), new Line(b, c), new Line(c, a));
    }

    public Set<InteractablePoint> getPoints() {
        InteractableRectangle bounding = getBoundingRectangle();
        Set<InteractablePoint> points = new HashSet<>();
        if (bounding == null) {
            return points;
        }
        for (int x = bounding.x; x < bounding.x + bounding.width - 1; x++) {
            for (int y = bounding.y; y < bounding.y + bounding.height - 1; y++) {
                if (contains(x, y)) {
                    points.add(new InteractablePoint(x, y));
                }
            }
        }
        return points;
    }

    private InteractableRectangle getBoundingRectangle() {
        if (!isValid()) {
            return null;
        }
        if (cached_bounding_box != null) {
            return cached_bounding_box;
        }
        int boundsMinX = Integer.MAX_VALUE;
        if (boundsMinX > a.x) {
            boundsMinX = a.x;
        }
        if (boundsMinX > b.x) {
            boundsMinX = b.x;
        }
        if (boundsMinX > c.x) {
            boundsMinX = c.x;
        }
        int boundsMaxX = Integer.MIN_VALUE;
        if (boundsMaxX < a.x) {
            boundsMaxX = a.x;
        }
        if (boundsMaxX < b.x) {
            boundsMaxX = b.x;
        }
        if (boundsMaxX < c.x) {
            boundsMaxX = c.x;
        }
        int boundsMinY = Integer.MAX_VALUE;
        if (boundsMinY > a.y) {
            boundsMinY = a.y;
        }
        if (boundsMinY > b.y) {
            boundsMinY = b.y;
        }
        if (boundsMinY > c.y) {
            boundsMinY = c.y;
        }
        int boundsMaxY = Integer.MIN_VALUE;
        if (boundsMaxY < a.y) {
            boundsMaxY = a.y;
        }
        if (boundsMaxY < b.y) {
            boundsMaxY = b.y;
        }
        if (boundsMaxY < c.y) {
            boundsMaxY = c.y;
        }
        return cached_bounding_box =
            new InteractableRectangle(boundsMinX, boundsMinY, boundsMaxX - boundsMinX,
                boundsMaxY - boundsMinY
            );
    }

    @Override
    public String toString() {
        return "Triangle{a=" + a.toString().replace("java.awt.", "") + ", b=" +
            b.toString().replace("java.awt.", "") + ", c=" + c.toString().replace("java.awt.", "") +
            '}';
    }

    @Override
    public boolean isValid() {
        return !a.equals(b) && !b.equals(c) && !a.equals(c) && !isCollinear();
    }

    private boolean isCollinear() {
        return getSlope(a, b) == getSlope(b, c);
    }
}
