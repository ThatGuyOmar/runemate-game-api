package com.runemate.game.api.hybrid.web.vertex.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@ToString
public class DialogNpcVertex extends BasicNpcVertex {

    private final Pattern dialog;

    public DialogNpcVertex(final Coordinate position, final Pattern action, final Pattern dialog, final NpcQueryBuilder builder) {
        super(position, action, builder);
        this.dialog = dialog;
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        if (isDialogAvailable()) {
            return handleDialog();
        }

        final var npc = getNpc();
        if (npc == null) {
            log.warn("Failed to resolve target entity for {}", this);
            return false;
        }

        if (npc.getVisibility() <= 40) {
            Camera.concurrentlyTurnTo(npc);
        }

        if ((boolean) cache.get(WebPath.DIRECT_INPUT)) {
            var ma = MenuAction.forNpc(npc, action);
            if (ma != null) {
                DirectInput.send(ma);
                return Execution.delayUntil(this::isDialogAvailable, moving(local, localPos), 3000);
            }
        }

        return npc.interact(action) && Execution.delayUntil(this::isDialogAvailable, moving(local, localPos), 3000);
    }

    private boolean isDialogAvailable() {
        return ChatDialog.getContinue() != null
            || ChatDialog.getOption(dialog) != null
            || getHeadsUpComponent() != null;
    }

    private boolean handleDialog() {
        ChatDialog.Selectable selectable;
        if ((selectable = ChatDialog.getContinue()) != null) {
            return selectable.select();
        } else if ((selectable = ChatDialog.getOption(dialog)) != null) {
            return selectable.select();
        }

        var iface = getHeadsUpComponent();
        return iface != null && iface.click();
    }

    private @Nullable InterfaceComponent getHeadsUpComponent() {
        return Interfaces.newQuery().containers(187).types(InterfaceComponent.Type.LABEL).texts(dialog).results().first();
    }
}
