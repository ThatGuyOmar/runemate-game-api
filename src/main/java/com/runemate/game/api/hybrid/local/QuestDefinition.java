package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.location.*;
import java.util.*;
import lombok.*;

/*
dbrow format:
    0,0	    id
    0,1	    sortname
    0,2	    displayname
    0,3	    autodisable
    0,4	    type
    0,5	    members
    0,6	    difficulty
    0,7	    length
    0,8	    location
    0,9	    year
    0,10	storyline
    0,11	storyindex
    0,12	mapcoord
    0,13	mapicon
    0,14	points
    0,15	statusprogress
    0,16	statuscomplete
    0,18	mainquest
    0,19	subquests
    0,20	statreq
    0,21	statrec
    0,22	questreq
    0,23	pointsreq
    0,24	comlevelreq
    0,25	comlevelrec
    0,28	speedrunning_mode
    0,29	speedrunning_bronze
    0,30	speedrunning_silver
    0,31	speedrunning_gold
    0,32	speedrunning_platinum
    0,33	speedrunning_inv
    0,34	speedrunning_worn
    0,35	speedrunning_bank
 */
public interface QuestDefinition {

    /**
     * Internal ID of the Quest
     */
    int getId();

    /**
     * The name used to sort the quests alphabetically. ie. "Grand Tree, The" instead of "The Grand Tree"
     */
    String getSortName();

    /**
     * The name of the quest as displayed in the quest log
     */
    String getDisplayName();

    Type getType();

    boolean isMembers();

    Difficulty getDifficulty();

    Length getLength();

    int getReleaseYear();

    /**
     * Coordinate used to draw the quest icon on the world map
     */
    Coordinate getStartPosition();

    int getRewardPoints();

    /**
     * Values of the varp or varbit that represents quest progress
     */
    List<Integer> getProgressionStatus();

    /**
     * Value of the varp or varbit that repesents quest completion
     */
    int getCompletionStatus();

    List<SkillRequirement> getSkillRequirements();

    List<SkillRequirement> getSkillRecommendations();

    List<QuestRequirement> getQuestRequirements();

    int getQuestPointRequirement();

    int getCombatLevelRequirement();

    int getCombatLevelRecommended();

    @AllArgsConstructor
    @Getter
    enum Type {
        QUEST(0),
        MINIQUEST(1);

        private final int id;
    }

    @AllArgsConstructor
    @Getter
    enum Difficulty {
        NOVICE(0),
        INTERMEDIATE(1),
        EXPERIENCED(2),
        MASTER(3),
        GRANDMASTER(4),
        SPECIAL(5);

        private final int id;
    }

    @AllArgsConstructor
    @Getter
    enum Length {
        VERY_SHORT(0),
        SHORT(1),
        MEDIUM(2),
        LONG(3),
        VERY_LONG(4);

        private final int id;
    }

    @Data
    class SkillRequirement {

        private final Skill skill;
        private final int requirement;
    }

    @Data
    class QuestRequirement {
        private final QuestDefinition quest;
    }
}