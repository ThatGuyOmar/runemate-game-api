package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;

public final class Equipment {
    private Equipment() {
    }

    public static boolean contains(Predicate<SpriteItem> filter) {
        return Items.contains(getItems(), filter);
    }

    public static boolean contains(int id) {
        return Items.contains(getItems(), id);
    }

    public static boolean contains(String name) {
        return Items.contains(getItems(), name);
    }

    public static boolean contains(Pattern name) {
        return Items.contains(getItems(), name);
    }

    @SafeVarargs
    public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAllOf(getItems(), filters);
    }

    public static boolean containsAllOf(final int... ids) {
        return Items.containsAllOf(getItems(), ids);
    }

    public static boolean containsAllOf(final String... names) {
        return Items.containsAllOf(getItems(), names);
    }

    public static boolean containsAllOf(final Pattern... names) {
        return Items.containsAllOf(getItems(), names);
    }

    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyExcept(getItems(), filters);
    }

    public static boolean containsAnyExcept(final String... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    public static boolean containsAnyExcept(final Pattern... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    public static boolean containsAnyExcept(final int... ids) {
        return Items.containsAnyExcept(getItems(), ids);
    }

    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<SpriteItem>... filter) {
        return Items.containsAnyOf(getItems(), filter);
    }

    public static boolean containsAnyOf(final int... ids) {
        return Items.containsAnyOf(getItems(), ids);
    }

    public static boolean containsAnyOf(final String... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    public static boolean containsAnyOf(final Pattern... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    @SafeVarargs
    public static boolean containsOnly(final Predicate<SpriteItem>... filter) {
        return Items.containsOnly(getItems(), filter);
    }

    public static boolean containsOnly(final String... names) {
        return Items.containsOnly(getItems(), names);
    }

    public static boolean containsOnly(final Pattern... names) {
        return Items.containsOnly(getItems(), names);
    }

    public static int getQuantity() {
        return Items.getQuantity(getItems());
    }

    @SafeVarargs
    public static int getQuantity(final Predicate<SpriteItem>... filter) {
        return Items.getQuantity(getItems(), filter);
    }

    public static int getQuantity(final int... ids) {
        return Items.getQuantity(getItems(), ids);
    }

    public static int getQuantity(final String... names) {
        return Items.getQuantity(getItems(), names);
    }

    public static int getQuantity(final Pattern... names) {
        return Items.getQuantity(getItems(), names);
    }

    @Nullable
    public static InteractableRectangle getBoundsOf(final Slot slot) {
        if (slot == null) {
            return null;
        }
        return OSRSEquipment.getBoundsOf(slot);
    }

    @Nullable
    public static SpriteItem getItemIn(final Slot slot) {
        return slot == null ? null : getItemIn(slot.index);
    }

    @Nullable
    public static SpriteItem getItemIn(final int index) {
        return newQuery().indices(index).results().first();
    }

    /**
     * Returns a list of all the items you have equipped (Does not require opening the tab)
     *
     * @return a list of all the items you have equipped
     */
    public static SpriteItemQueryResults getItems() {
        return Inventories.lookup(Inventories.Documented.EQUIPMENT);
    }

    public static SpriteItemQueryResults getItems(final Predicate<SpriteItem> filter) {
        return Inventories.lookup(Inventories.Documented.EQUIPMENT, filter);
    }

    public static SpriteItemQueryResults getItems(final int... ids) {
        return getItems(Items.getIdPredicate(ids));
    }

    public static SpriteItemQueryResults getItems(final String... names) {
        return getItems(Items.getNamePredicate(names));
    }

    public static SpriteItemQueryResults getItems(final Pattern... names) {
        return getItems(Items.getNamePredicate(names));
    }

    @Nullable
    public static InteractableRectangle getViewport() {
        return null;
    }

    public static boolean isEmpty() {
        return getItems().isEmpty();
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.EQUIPMENT);
    }

    public enum Slot {
        HEAD(0, 8, 156),
        CAPE(1, 157),
        NECK(2, 158),
        WEAPON(3, 159),
        BODY(4, 161),
        SHIELD(5, 162),
        LEGS(7, 163),
        HANDS(9, 164),
        FEET(10, 165),
        RING(12, 160),
        AMMUNITION(13, 166),
        @Deprecated
        AURA(14),
        @Deprecated
        POCKET(17);
        private final int index;
        private final int cacheIndex;
        private final int textureId;

        Slot(final int index) {
            this(index, -1);
        }

        Slot(final int index, final int textureId) {
            this(index, index, textureId);
        }

        Slot(final int index, final int cacheIndex, final int textureId) {
            this.index = index;
            this.cacheIndex = cacheIndex;
            this.textureId = textureId;
        }

        public static Slot resolveFromDefinition(final int cacheIndex) {
            return Arrays.stream(values())
                .filter(slot -> slot.cacheIndex == cacheIndex)
                .findAny()
                .orElse(null);
        }

        public static Slot resolve(final int index) {
            return Arrays.stream(values())
                .filter(slot -> slot.index == index)
                .findAny()
                .orElse(null);
        }

        public final InterfaceComponent getComponent() {
            if (textureId != -1) {
                final InterfaceComponent slot =
                    Interfaces.newQuery().containers(387).types(InterfaceComponent.Type.SPRITE)
                        .sprites(textureId).results().first();
                if (slot != null) {
                    return slot.getParentComponent();
                }
            }
            return null;
        }

        public final int getIndex() {
            return index;
        }

        @Override
        public String toString() {
            return name().substring(0, 1).toUpperCase() + name().substring(1).toLowerCase();
        }
    }
}
