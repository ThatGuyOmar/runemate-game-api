package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;
import lombok.*;

public final class OSRSGroundItems {
    private OSRSGroundItems() {
    }


    public static LocatableEntityQueryResults<GroundItem> getLoaded(
        final Predicate<? super GroundItem> filter
    ) {
        final List<GroundItem> ground_items = new ArrayList<>();
        final int plane = Region.getCurrentPlane();
        final Coordinate base = Region.getBase(0);
        final long[] rowx = OpenItemNode.getColumnX(plane);
        if (rowx == null) {
            return new LocatableEntityQueryResults<>(Collections.emptyList());
        }
        for (int x = 0; x < rowx.length; ++x) {
            final long[] column = OpenItemNode.getColumnY(rowx[x]);
            if (column == null) {
                continue;
            }
            for (int y = 0; y < column.length; ++y) {
                final long deque = column[y];
                if (deque != 0L) {
                    final long[] uids = OpenItemNode.getItemsAt(deque);
                    if (uids == null) {
                        continue;
                    }
                    for (long uid : uids) {
                        final GroundItem item = new OSRSGroundItem(
                            uid,
                            new Coordinate(base.getX() + x, base.getY() + y, plane)
                        );
                        if (filter == null || filter.test(item)) {
                            ground_items.add(item);
                        }
                    }
                }
            }
        }
        return new LocatableEntityQueryResults<>(ground_items);
    }


    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(
        final Coordinate position,
        final Predicate<? super GroundItem> filter
    ) {
        final long loadedGroundItemsUid = OpenClient.getLoadedGroundItemsUid();
        return (loadedGroundItemsUid == 0L)
            ? new LocatableEntityQueryResults<>(Collections.emptyList())
            : getLoadedOn(position, loadedGroundItemsUid, filter);
    }

    @SneakyThrows
    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(
        final Coordinate position,
        long loadedGroundItemsUid,
        final Predicate<? super GroundItem> filter
    ) {
        final Coordinate.RegionOffset offset = position.offset();
        final long[] uids =
            OpenItemNode.getLoadedOn(loadedGroundItemsUid, offset.getX(), offset.getY(),
                position.getPlane()
            );
        if (uids == null) {
            return new LocatableEntityQueryResults<>(Collections.emptyList());
        }
        List<GroundItem> items = new ArrayList<>();
        for (long uid : uids) {
            final GroundItem item = new OSRSGroundItem(uid, position);
            if (filter == null || filter.test(item)) {
                items.add(item);
            }
        }
        return new LocatableEntityQueryResults<>(items);
    }


    public static LocatableEntityQueryResults<GroundItem> getLoadedWithin(
        Area area,
        Predicate<? super GroundItem> filter
    ) {
        List<GroundItem> items = new ArrayList<>();
        List<Coordinate> coordinates = area.getCoordinates();
        final Coordinate base = Region.getBase(0);
        if (!coordinates.isEmpty()) {
            final long loadedGroundItemsUid = OpenClient.getLoadedGroundItemsUid();
            for (final Coordinate coordinate : coordinates) {
                final Coordinate.RegionOffset offset = coordinate.offset(base);
                final long[] uids =
                    OpenItemNode.getLoadedOn(loadedGroundItemsUid, offset.getX(), offset.getY(),
                        coordinate.getPlane()
                    );
                for (long uid : uids) {
                    final GroundItem item = new OSRSGroundItem(uid, coordinate);
                    if (filter == null || filter.test(item)) {
                        items.add(item);
                    }
                }
            }
        }
        return new LocatableEntityQueryResults<>(items, null);
    }
}
