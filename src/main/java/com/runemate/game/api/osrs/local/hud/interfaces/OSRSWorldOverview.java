package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.osrs.location.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;

public class OSRSWorldOverview implements WorldOverview {

    private final long uid;
    private final int id;
    private final int type;
    private final String activity;
    private final String address;
    private final int location;
    private final int population;

    public OSRSWorldOverview(long uid) {
        this.uid = uid;
        this.id = -1;
        this.type = -1;
        this.activity = null;
        this.address = null;
        this.location = -1;
        this.population = -1;
    }

    public OSRSWorldOverview(int id, int type, String activity, String address, int location, int population) {
        this.address = address;
        this.uid = -1;
        this.id = id;
        this.type = type;
        this.activity = activity;
        this.location = location;
        this.population = population;
    }

    @Override
    public int getPopulation() {
        return population;
    }

    @Override
    public WorldRegion getRegion() {
        return WorldRegion.valueOf(location);
    }

    @Override
    public int getId() {
        return uid != -1 ? OpenWorld.getNumber(uid) : id;
    }

    @Override
    @OSRSOnly
    public EnumSet<WorldType> getWorldTypes() {
        return WorldType.of(getMarkerBits());
    }

    @OSRSOnly
    @Override
    public String getActivity() {
        if (uid == -1) {
            return activity;
        }
        return OpenWorld.getActivity(uid);
    }

    private boolean hasSkillTotalRequirement() {
        return getWorldTypes().contains(WorldType.SKILL_TOTAL);
    }

    @Override
    public boolean isMembersOnly() {
        return getWorldTypes().contains(WorldType.MEMBERS);
    }

    @Override
    public boolean isPVP() {
        return getWorldTypes().contains(WorldType.PVP);
    }

    @Override
    @RS3Only
    public boolean isLootShare() {
        return false;
    }

    @Override
    public boolean isBounty() {
        return getWorldTypes().contains(WorldType.BOUNTY);
    }

    @Override
    public boolean isPvpArena() {
        return getWorldTypes().contains(WorldType.PVP_ARENA);
    }

    @Override
    public boolean isSpeedrunning() {
        return getWorldTypes().contains(WorldType.QUEST_SPEEDRUNNING);
    }

    @Override
    public boolean isHighRisk() {
        return getWorldTypes().contains(WorldType.HIGH_RISK);
    }

    @OSRSOnly
    @Override
    public boolean isLastManStanding() {
        return getWorldTypes().contains(WorldType.LAST_MAN_STANDING);
    }

    @OSRSOnly
    @Override
    public boolean isFreshStart() {
        return getWorldTypes().contains(WorldType.FRESH_START_WORLD);
    }

    @RS3Only
    @Override
    public boolean isQuickChat() {
        return false;
    }

    @RS3Only
    @Deprecated
    @Override
    public boolean isSkillTotal2400() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isSkillTotal2600() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isVIP() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isLegacyOnly() {
        return false;
    }

    @RS3Only
    @Override
    public boolean isEoCOnly() {
        return false;
    }

    @Override
    public boolean isLeague() {
        return getWorldTypes().contains(WorldType.SEASONAL);
    }

    @OSRSOnly
    @Override
    public boolean isDeadman() {
        return getWorldTypes().contains(WorldType.DEADMAN);
    }

    @OSRSOnly
    @Override
    public boolean isTournament() {
        return getWorldTypes().contains(WorldType.TOURNAMENT_WORLD);
    }

    @OSRSOnly
    @Override
    public boolean isCastleWars() {
        return getWorldTypes().contains(WorldType.CASTLE_WARS);
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal500() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 500;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal750() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 750;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal1250() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 1250;
    }

    @Override
    public boolean isSkillTotal1500() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 1500;
    }

    @OSRSOnly
    @Override
    public boolean isSkillTotal1750() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 1750;
    }

    @Override
    public boolean isSkillTotal2000() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 2000;
    }

    @Override
    public boolean isSkillTotal2200() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 2200;
    }

    public int getMarkerBits() {
        if (uid == -1) {
            return type;
        }
        return OpenWorld.getMarkerBits(uid);
    }

    private int getUnknownType() {
        int mask = getMarkerBits();
        if (isMembersOnly()) {
            mask ^= 0x1;
        }
        if (isPVP()) {
            mask ^= 0x4;
        }
        if (isLootShare()) {
            mask ^= 0x8;
        }
        if (isBounty()) {
            mask ^= 0x20;
        }
        if (isPvpArena()) {
            mask ^= 0x40;
        }
        if (hasSkillTotalRequirement()) {
            mask ^= 0x80;
        }
        if (isSpeedrunning()) {
            mask ^= 0x100;
        }
        if (isHighRisk()) {
            mask ^= 0x400;
        }
        if (isLastManStanding()) {
            mask ^= 0x4000;
        }
        if (isTournament()) {
            mask ^= 0x2000000;
        }
        if (isCastleWars()) {
            mask ^= 0x8000000;
        }
        if (isDeadman()) {
            mask ^= 0x20000000;
        }
        if (isLeague()) {
            mask ^= 0x40000000;
        }
        return mask;
    }

    @Override
    public String toString() {
        return "World " + getId() + " Overview";
    }

    public String getAddress() {
        return address;
    }
}
