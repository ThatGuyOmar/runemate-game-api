package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.exceptions.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public final class OSRSBank {

    public static final int DEFAULT_QUANTITY_VARBIT = 6590;
    public static final int CUSTOM_DEFAULT_QUANTITY_VARBIT = 3960;
    private static final Pattern BANK_OPEN_TITLE_PATTERN = Pattern.compile(
        "The Bank of Gielinor|Tab [1-9]|Bank settings menu|Showing items: .+");
    private static final int BANK_CONTAINER = 12;
    private static final String WITHDRAW_ALL = "Withdraw-All";
    private static final String WITHDRAW_ALL_BUT_ONE = "Withdraw-All-but-1";
    private static final String WITHDRAW_X = "Withdraw-X";
    private static final Pattern WITHDRAW_NUMBER_PATTERN = Regex.getPattern("^Withdraw-(\\d+)$");
    private static final Pattern DEPOSIT_ALL_PATTERN = Regex.getPattern("^Deposit[- ]All$");
    private static final Pattern DEPOSIT_X_PATTERN = Regex.getPattern("^Deposit[- ]X$");
    private static final Pattern DEPOSIT_NUMBER_PATTERN = Regex.getPattern("^Deposit[- ](\\d+)$");

    private OSRSBank() {
    }

    public static boolean isOpen() {
        return !Interfaces.newQuery()
            .containers(BANK_CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.LABEL)
            .texts(BANK_OPEN_TITLE_PATTERN)
            .visible()
            .results()
            .isEmpty();
    }

    public static boolean close(boolean hotkey) {
        if (!Bank.isOpen()) {
            return false;
        }
        log.info("Closing bank");
        if (hotkey && OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()) {
            if (Keyboard.typeKey(KeyEvent.VK_ESCAPE)) {
                return Execution.delayWhile(Bank::isOpen, 2000, 2500);
            }
        } else {
            InterfaceComponent button = Interfaces.newQuery()
                .containers(BANK_CONTAINER)
                .grandchildren(true)
                .types(InterfaceComponent.Type.SPRITE)
                .actions("Close")
                .visible()
                .results()
                .first();
            if (button != null && button.interact("Close")) {
                return Execution.delayWhile(Bank::isOpen, 2000);
            }
        }
        return false;
    }

    public static boolean deposit(final SpriteItem item, final int amount) {
        if (item == null || !item.hover()) {
            return false;
        }
        log.info("Depositing {} {}", amount, item);


        int invQuantity = Inventory.getQuantity(item.getId());
        final int quantityToDeposit = amount == 0 ? invQuantity : amount;
        boolean willDepositRemaining = quantityToDeposit >= invQuantity;

        Set<String> actions = Menu.getItems().stream()
            .map(MenuItem::getAction)
            .collect(Collectors.toSet());

        Set<String> validActions = actions.stream()
            .filter(action -> {
                int quantity;
                Matcher matcher;
                if (DEPOSIT_ALL_PATTERN.matcher(action).find()) {
                    quantity = invQuantity;
                } else if ((matcher = DEPOSIT_NUMBER_PATTERN.matcher(action)).find()) {
                    quantity = Integer.parseInt(matcher.group(1));
                } else {
                    return false;
                }
                return quantity == quantityToDeposit
                    || (willDepositRemaining && quantity >= invQuantity);
            })
            .collect(Collectors.toSet());

        Pattern action;
        boolean manual = false;
        if (!validActions.isEmpty()) {
            //We make a pattern with all valid actions and let .interact() select the correct one (i.e left-click)
            action = Regex.getPatternForExactStrings(validActions.toArray(new String[0]));
        } else {
            //We default to Withdraw-X if no other action is valid
            if (actions.stream().anyMatch(a -> DEPOSIT_X_PATTERN.matcher(a).find())) {
                action = DEPOSIT_X_PATTERN;
                manual = true;
            } else {
                log.warn("Action not found in {}", actions);
                return false;
            }
        }

        log.debug("Using action {}", action);
        if (item.interact(action)) {
            if (manual && Execution.delayUntil(InputDialog::isOpen, 1400, 2200)) {
                return InputDialog.enterAmount(amount) && Execution.delayWhile(item::isValid, 1400, 2200);
            }
            return Execution.delayWhile(item::isValid, 1400, 2200);
        }
        return false;
    }

    @SuppressWarnings("Duplicates")
    public static boolean withdraw(final SpriteItem item, final int amount) {
        if (item == null) {
            return false;
        }
        log.info("Withdrawing {} {}", amount, item);
        if (item.hover()) {
            int availableQuantity = Bank.getQuantity(item.getId());
            final int quantityToWithdraw;
            if (amount == 0) {
                quantityToWithdraw = availableQuantity;
            } else if (amount == -1) {
                quantityToWithdraw = availableQuantity - 1;
            } else {
                quantityToWithdraw = amount;
            }

            if (quantityToWithdraw <= 0) {
                log.info("There must be at least 1 item available for withdrawal");
                return false;
            }

            ItemDefinition def = item.getDefinition();
            int emptySlots = Inventory.getEmptySlots();
            boolean willFillInventory = def != null && !def.stacks()
                && Bank.getWithdrawMode() != Bank.WithdrawMode.NOTE
                && quantityToWithdraw >= emptySlots;

            boolean willEmptyBank = quantityToWithdraw >= availableQuantity;

            Set<String> actions = Menu.getItems().stream()
                .map(MenuItem::getAction)
                .collect(Collectors.toSet());

            Set<String> validActions = actions.stream()
                .filter(action -> {
                    int quantity;
                    Matcher matcher;
                    if (WITHDRAW_ALL.equals(action)) {
                        quantity = availableQuantity;
                    } else if (WITHDRAW_ALL_BUT_ONE.equals(action)) {
                        quantity = availableQuantity - 1;
                    } else if ((matcher = WITHDRAW_NUMBER_PATTERN.matcher(action)).find()) {
                        quantity = Integer.parseInt(matcher.group(1));
                    } else {
                        return false;
                    }
                    return quantity == quantityToWithdraw
                        || (willFillInventory && quantity >= emptySlots)
                        || (willEmptyBank && quantity >= availableQuantity);
                })
                .collect(Collectors.toSet());

            Pattern action;
            boolean manual = false;
            if (!validActions.isEmpty()) {
                //We make a pattern with all valid actions and let .interact() select the correct one (i.e left-click)
                action = Regex.getPatternForExactStrings(validActions.toArray(new String[0]));
            } else {
                //We default to Withdraw-X if no other action is valid
                if (actions.contains(WITHDRAW_X)) {
                    action = Regex.getPatternForExactString(WITHDRAW_X);
                    manual = true;
                } else {
                    log.warn("Action not found in {}", actions);
                    return false;
                }
            }

            log.debug("Using action {}", action);
            if (item.interact(action)) {
                if (manual && Execution.delayUntil(InputDialog::isOpen, 1200, 2400)) {
                    return InputDialog.enterAmount(quantityToWithdraw) &&
                        Execution.delayWhile(item::isValid, 1200, 2400);
                }
                return Execution.delayWhile(item::isValid, 1200, 2400);
            }
            return Execution.delayWhile(item::isValid, 1200, 2400);
        }
        return false;
    }

    private static InterfaceComponent getSearchDialogComponent() {
        return Interfaces.newQuery()
            .containers(162)
            .types(InterfaceComponent.Type.LABEL)
            .grandchildren(false)
            .texts("Show items whose names contain the following text:")
            .results()
            .first();
    }

    private static InterfaceComponent getSearchTextComponent() {
        return Interfaces.newQuery()
            .containers(162)
            .types(InterfaceComponent.Type.LABEL)
            .grandchildren(false)
            .heights(20)
            .fonts(496)
            .textContains("*")
            .results()
            .first();
    }

    public static boolean withdrawToBeastOfBurden(final SpriteItem item, final int amount) {
        if (Environment.getBot().getMetaData().isOSRSOnly()) {
            throw new OSRSException("Bank", "withdrawToBeastOfBurden");
        }
        return false;
    }

    public static boolean depositInventory() {
        if (!Inventory.getItems().isEmpty()) {
            log.info("Depositing inventory");
            final InterfaceComponent button = Interfaces.newQuery()
                .containers(BANK_CONTAINER)
                .types(InterfaceComponent.Type.SPRITE)
                .actions("Deposit inventory")
                .grandchildren(false)
                .results()
                .first();
            if (button != null && button.isVisible() && button.click()) {
                Execution.delayUntil(Inventory::isEmpty, 2000);
            }
        }
        return Inventory.getItems().isEmpty();
    }

    public static boolean depositWornItems() {
        if (!Equipment.getItems().isEmpty()) {
            final InterfaceComponent button = Interfaces.newQuery()
                .containers(BANK_CONTAINER)
                .types(InterfaceComponent.Type.SPRITE)
                .actions("Deposit worn items")
                .grandchildren(false)
                .results()
                .first();
            if (button != null && button.isVisible() && button.click()) {
                Execution.delayUntil(Equipment::isEmpty, 2000);
            }
        }
        return Equipment.isEmpty();
    }

    public static InteractableRectangle getViewport() {
        InterfaceComponent viewport = Interfaces.newQuery()
            .grandchildren(false)
            .containers(BANK_CONTAINER)
            .types(InterfaceComponent.Type.CONTAINER)
            .widths(460)
            .visible()
            .results()
            .first();
        return viewport != null ? viewport.getBounds() : null;
    }

    public static List<InteractableRectangle> getSlotBounds() {
        final InterfaceComponent container = Interfaces.newQuery()
            .containers(BANK_CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER)
            .filter(it -> it.getChildQuantity() >= 800)
            .results()
            .first();
        return container != null
            ? Parallelize.mapAndCollectToList(container.getChildren(), InterfaceComponent::getBounds)
            : Collections.emptyList();
    }

    public static InteractableRectangle getBoundsOf(int index) {
        InterfaceComponent component = Interfaces.newQuery()
            .containers(BANK_CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER)
            .filter(it -> it.getChildQuantity() >= 800)
            .results()
            .first();
        if (component != null) {
            final InterfaceComponent c = component.getChild(index);
            if (c != null) {
                return c.getBounds();
            }
        }
        return null;
    }

    public static boolean openMainTab() {
        if (Bank.getCurrentTab() == 0) {
            return true;
        }
        final InterfaceComponent first = Interfaces.newQuery()
            .containers(BANK_CONTAINER)
            .types(InterfaceComponent.Type.SPRITE)
            .actions("View all items")
            .results()
            .first();
        return first != null && first.isVisible() && first.interact("View all items");
    }

    public static boolean openTabContaining(@NonNull SpriteItem item) {
        final var targetTab = Bank.getTabContaining(item);
        if (targetTab == -1) {
            return false;
        }
        final var currentTab = Bank.getCurrentTab();
        if (currentTab == targetTab) {
            return true;
        }

        if (targetTab == 0) {
            return openMainTab();
        }

        final var tabs = Interfaces.newQuery()
            .containers(BANK_CONTAINER)
            .types(InterfaceComponent.Type.SPRITE)
            .actions("View tab")
            .results();

        if (tabs.size() < targetTab) {
            return false;
        }

        final var targetHeader = tabs.get(targetTab - 1);
        return targetHeader != null && targetHeader.interact("View tab");
    }
}
