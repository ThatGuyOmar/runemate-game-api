package com.runemate.game.api.osrs;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import lombok.extern.log4j.*;

@Log4j2
public final class OSRSRunescape {
    private static final int LOGOUT_WIDGET = 182;

    private OSRSRunescape() {
    }

    /**
     * Also known as getGameState
     */

    public static int getEngineState() {
        return OpenClient.getEngineState();
    }


    public static int getLoginState() {
        return OpenClient.getLoginState();
    }


    public static String getServerResponseMessage() {
        return OpenClient.getServerResponseMessage();
    }

    public static boolean isLoggedIn() {
        Player avatar;
        int state = getEngineState();
        return state == RuneScape.EngineState.LOADING_REGION.getValue()
            || state == RuneScape.EngineState.SESSION_LOADED.getValue()
            && (avatar = Players.getLocal()) != null
            && avatar.getModel() != null;
    }

    public static boolean logout() {
        if (!isLoggedIn()) {
            return true;
        }
        log.info("Logging out");
        if (ControlPanelTab.LOGOUT.open()) {
            InterfaceComponent button =
                Interfaces.newQuery().containers(LOGOUT_WIDGET).types(InterfaceComponent.Type.LABEL)
                    .grandchildren(false).texts("Click here to logout").results().first();
            if (button == null || !button.isVisible()) {
                button = Interfaces.newQuery().containers(69).actions("Logout").results().first();
            }
            if (button != null && button.isVisible() && button.click()) {
                return Execution.delayUntil(() -> !isLoggedIn(), 1250, 2000);
            }
        }
        return false;
    }
}
