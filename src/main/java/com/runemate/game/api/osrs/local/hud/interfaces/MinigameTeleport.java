package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.account.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.*;

public enum MinigameTeleport {
    BARBARIAN_ASSAULT(2530, 3577, 0, "Barbarian Assault"),
    BLAST_FURNACE(2933, 10184, 0, "Blast Furnace"),
    BURTHORPE_GAMES_ROOM(2207, 4940, 0, "Burthorpe Games Room"),
    CASTLE_WARS(2441, 3094, 0, "Castle Wars"),
    CLAN_WARS(3368, 3163, 0, "Clan Wars"),
    FISHING_TRAWLER(2658, 3159, 0, "Fishing Trawler"),
    GIANTS_FOUNDRY(3361, 3147, 0, "Giants' Foundry"),
    LAST_MAN_STANDING(3400, 3177, 0, "Last Man Standing"),
    NIGHTMARE_ZONE(2610, 3122, 0, "Nightmare Zone"),
    PEST_CONTROL(2655, 2655, 0, "Pest Control"),
    //RAT_PITS("Rat Pits"), Requires a secondary dialogue
    SHADES_OF_MORTTON(3499, 3299, 0, "Shades of Mort'ton"),
    TITHE_FARM(1790, 3591, 0, "Tithe Farm"),
    TROUBLE_BREWING(3816, 3023, 0, "Trouble Brewing"),
    TZHAAR_FIGHT_PIT(2404, 5180, 0, "TzHaar Fight Pit");
    private final String name;
    private final Coordinate destination;

    MinigameTeleport(int x, int y, int plane, String name) {
        this.destination = new Coordinate(x, y, plane);
        this.name = name;
    }

    public Coordinate getDestination() {
        return destination;
    }

    public String getName() {
        return name;
    }

    public boolean activate() {
        if (Bank.isOpen()) {
            Bank.close();
        }
        if (GrandExchange.isOpen()) {
            GrandExchange.close();
        }
        ChatDialog.Continue continueDialog = ChatDialog.getContinue();
        if (continueDialog != null) {
            continueDialog.select();
        }
        if (ControlPanelTab.GROUPING.open()) {
            InterfaceComponent dropdownMenu =
                Interfaces.newQuery().containers(76).types(InterfaceComponent.Type.LABEL)
                    .grandchildren(false).widths(155).results().first();
            if (dropdownMenu != null) {
                if (!getName().equals(dropdownMenu.getText())) {
                    InterfaceComponent minigameButton =
                        Interfaces.newQuery().containers(76).types(InterfaceComponent.Type.LABEL)
                            .texts(getName()).results().first();
                    if (minigameButton == null || !minigameButton.isVisible()) {
                        if (dropdownMenu.click()) {
                            //TODO fix this delay
                            Execution.delay(600);
                            minigameButton = Interfaces.newQuery().containers(76)
                                .types(InterfaceComponent.Type.LABEL).texts(getName()).results()
                                .first();
                        }
                    }
                    if (minigameButton != null && minigameButton.isVisible() &&
                        Interfaces.scrollTo(minigameButton, minigameButton.getParentComponent())
                        && minigameButton.click()) {
                        Execution.delayUntil(() -> getName().equals(dropdownMenu.getText()), 600,
                            1200
                        );
                    }
                }
                if (getName().equals(dropdownMenu.getText())) {
                    InterfaceComponent teleportButton =
                        Interfaces.newQuery().containers(76).grandchildren(false)
                            .types(InterfaceComponent.Type.LABEL).texts("Teleport").results()
                            .first();
                    if (teleportButton != null && teleportButton.isVisible() &&
                        teleportButton.click()) {
                        Player local = Players.getLocal();
                        if (local != null) {
                            Coordinate startCoordinate = local.getPosition();
                            if (startCoordinate != null &&
                                Execution.delayUntil(() -> local.getAnimationId() != -1, 2400,
                                    3600
                                )) {
                                Execution.delayWhile(() -> local.getAnimationId() != -1);
                                if (Execution.delayUntil(
                                    () -> !startCoordinate.equals(local.getPosition()), 2400,
                                    3600
                                )) {
                                    AccountTraversalProfile atp = OpenAccountDetails.getTraversalProfile();
                                    atp.getCachedTimers()
                                        .put("minigame_teleports", new CachedTimer("minigame_teleports", System.currentTimeMillis()));
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
