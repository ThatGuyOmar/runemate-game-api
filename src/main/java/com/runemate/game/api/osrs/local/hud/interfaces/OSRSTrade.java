package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.extern.log4j.*;

@Log4j2
public class OSRSTrade {
    private final static int AMOUNT_ENTRY_CONTAINER = 162;
    private final static Pattern TRADING_PATTERN =
        Pattern.compile("^Trading ([Ww])ith:( |\\r?\\n)(?<name>.+)$");

    public static String getTradersName() {
        InterfaceComponent traderComp =
            Interfaces.newQuery().containers(Trade.OFFERS_CONTAINER, Trade.CONFIRM_CONTAINER)
                .texts(TRADING_PATTERN).visible().results().first();
        String text;
        Matcher matcher;
        return traderComp != null && (text = traderComp.getText()) != null
            && (matcher = TRADING_PATTERN.matcher(text)).find() ? matcher.group("name") : null;
    }

    public static boolean offer(final SpriteItem item, final int amount) {
        log.info("Offering {} {}", amount, item);
        final var amountEntryBuilder = Interfaces.newQuery()
            .containers(AMOUNT_ENTRY_CONTAINER)
            .texts("Enter amount:")
            .visible();
        InteractableRectangle rect;
        if (!amountEntryBuilder.results().isEmpty()
            && (!Keyboard.typeKey(KeyEvent.VK_ESCAPE)
            || !Execution.delayUntil(() -> amountEntryBuilder.results().isEmpty(), 1200))
            || item == null
            || (rect = item.getBounds()) == null
            || !rect.hover()) {
            return false;
        }
        boolean manual = false;
        String action;
        Set<String> actions;
        if (amount == 1 || amount == 0 && Trade.Outgoing.getQuantity(item.getId()) == 1) {
            action = "Offer";
        } else if (!(actions = Menu.getItems().stream().map(MenuItem::getAction).collect(Collectors.toSet())).isEmpty()
            && amount == 0
            && actions.contains("Offer-All")) {
            action = "Offer-All";
        } else if (actions.contains("Offer-" + amount)) {
            action = "Offer-" + amount;
        } else if (actions.contains("Offer-X")) {
            manual = true;
            action = "Offer-X";
        } else {
            action = null;
        }
        return action != null
            && item.interact(action)
            && (!manual
            || Execution.delayUntil(() -> !amountEntryBuilder.results().isEmpty(), 1200) && Keyboard.type(Integer.toString(amount), true))
            && Execution.delayWhile(item::isValid, 1200);
    }

    public static boolean remove(final SpriteItem item, final int amount) {
        log.info("Removing {} {}", item, amount);
        InterfaceComponentQueryBuilder amountEntryBuilder = Interfaces.newQuery()
            .containers(AMOUNT_ENTRY_CONTAINER)
            .texts("Enter amount:")
            .visible();
        InteractableRectangle rect;
        if (!amountEntryBuilder.results().isEmpty() && (!Keyboard.typeKey(KeyEvent.VK_ESCAPE)
            || !Execution.delayUntil(() -> amountEntryBuilder.results().isEmpty(), 1200))
            || item == null
            || (rect = item.getBounds()) == null
            || !rect.hover()) {
            return false;
        }
        boolean manual = false;
        String action;
        Set<String> actions;
        if (amount == 1 || amount == 0 && Trade.Outgoing.getQuantity(item.getId()) == 1) {
            action = "Remove";
        } else if (!(
            actions =
                Menu.getItems().stream().map(MenuItem::getAction).collect(Collectors.toSet())
        ).isEmpty()
            && amount == 0 && actions.contains("Remove-All")) {
            action = "Remove-All";
        } else if (actions.contains("Remove-" + amount)) {
            action = "Remove-" + amount;
        } else if (actions.contains("Remove-X")) {
            manual = true;
            action = "Remove-X";
        } else {
            action = null;
        }
        return action != null
            && item.interact(action)
            && (!manual
            || Execution.delayUntil(() -> !amountEntryBuilder.results().isEmpty(), 1200) && Keyboard.type(Integer.toString(amount), true))
            && Execution.delayWhile(item::isValid, 1200);
    }

}
