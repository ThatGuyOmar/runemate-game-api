package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;

public final class OSRSHealth {
    private OSRSHealth() {
    }

    public static int getCurrent() {
        return Skill.CONSTITUTION.getCurrentLevel();
    }

    public static int getMaximum() {
        return Skill.CONSTITUTION.getBaseLevel();
    }
}
