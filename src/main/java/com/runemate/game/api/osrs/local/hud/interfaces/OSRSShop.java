package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.script.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import lombok.extern.log4j.*;

@Log4j2
public final class OSRSShop {
    private static final int GENERAL_SHOP_CONTAINER = 300;
    private static final Color HEADER_COLOR = new Color(255, 152, 31);
    private final static int[] AMOUNTS = { 50, 10, 5, 1 };

    private OSRSShop() {
    }

    public static String getName() {
        InterfaceComponent ic = Interfaces.newQuery()
            .containers(GENERAL_SHOP_CONTAINER)
            .types(InterfaceComponent.Type.LABEL)
            .heights(24)
            .textColors(HEADER_COLOR)
            .results()
            .first();
        return ic == null ? null : ic.getText();
    }

    private static Map<SpriteItem, InterfaceComponent> getItemComponents(
        Predicate<SpriteItem> filter
    ) {
        List<InterfaceComponent> items = Interfaces.newQuery()
            .containers(GENERAL_SHOP_CONTAINER)
            .types(InterfaceComponent.Type.SPRITE)
            .containedItem(i -> i != null && !"null".equals(i.getName()))
            .results()
            .asList();
        Map<SpriteItem, InterfaceComponent> res = new HashMap<>(items.size());
        for (int i = 0; i < items.size(); i++) {
            InterfaceComponent ic = items.get(i);
            SpriteItem item =
                new SpriteItem(ic.getContainedItemId(), ic.getContainedItemQuantity(), i,
                    SpriteItem.Origin.SHOP
                );
            if (filter == null || filter.test(item)) {
                res.put(item, ic);
            }
        }
        return res;
    }

    public static SpriteItemQueryResults getItems(Predicate<SpriteItem> filter) {
        return new SpriteItemQueryResults(new ArrayList<>(getItemComponents(filter).keySet()));
    }

    public static boolean close() {
        if (!Shop.isOpen()) {
            return true;
        } else {
            log.info("Closing shop interface");
            final InterfaceComponent button = Interfaces.newQuery().containers(GENERAL_SHOP_CONTAINER).actions("Close").results().first();
            if (button != null && button.isVisible()) {
                return button.interact("Close") && Execution.delayUntil(() -> !Shop.isOpen(), 600, 1200);
            }
        }
        return false;
    }

    public static boolean isOpen() {
        return !Interfaces.newQuery()
            .containers(GENERAL_SHOP_CONTAINER)
            .types(InterfaceComponent.Type.CONTAINER)
            .widths(488)
            .heights(300)
            .visible()
            .results()
            .isEmpty();
    }

    public static List<InteractableRectangle> getSlotBounds() {
        InterfaceComponent itemContainer = Interfaces.newQuery()
            .containers(GENERAL_SHOP_CONTAINER)
            .types(InterfaceComponent.Type.CONTAINER)
            .filter(i -> i.getComponents().stream().anyMatch(inner -> inner.getContainedItemId() != -1))
            .results()
            .first();
        List<InterfaceComponent> items = itemContainer == null ? null : itemContainer.getComponents();
        List<InteractableRectangle> res;
        if (items != null) {
            res = items.stream().filter(i -> i.getContainedItemId() > 0).map(InterfaceComponent::getBounds).collect(Collectors.toList());
        } else {
            res = Collections.emptyList();
        }
        return res;
    }

    public static boolean buy(final Predicate<SpriteItem> filter, int quantity) {
        if (!isOpen()) {
            return false;
        }

        final int initialAmount = Inventory.getQuantity(filter);
        SpriteItem itemToBuy;
        int amountToBuy;
        if (quantity <= 0) {
            SpriteItem firstItem = Shop.getItems(filter).first();
            quantity = firstItem == null ? 0 : firstItem.getQuantity();
        }
        boolean shopSupports50 = getItemComponents(filter).values().stream()
            .anyMatch(i -> i.getActions().contains("Buy 50"));
        int emptySlots;
        for (int retry = 0; retry < 10
            && (amountToBuy = (initialAmount + quantity) - Inventory.getQuantity(filter)) > 0
            && (itemToBuy = Shop.getItems(filter).first()) != null && itemToBuy.getQuantity() > 0
            && ((emptySlots = Inventory.getEmptySlots()) > 0
            || itemToBuy.getDefinition().stacks()
            && Inventory.contains(itemToBuy.getId())); retry++) {
            String action = null;
            final int finalAmount = amountToBuy;
            final int finalEmptyAmount = emptySlots;

            //overbuying
            OptionalInt bestAmount;
            if (PlayerSense.getAsBoolean(PlayerSense.Key.OVERBUY_FROM_SHOP) &&
                !itemToBuy.getDefinition().stacks()) { //do not use overbuying for noted items
                bestAmount = Arrays.stream(AMOUNTS).filter(
                    i -> (finalAmount >= i || finalEmptyAmount < finalAmount) &&
                        (i != 50 || shopSupports50)).findFirst();
            } else {
                bestAmount = Arrays.stream(AMOUNTS).filter(i -> finalAmount >= i && (i != 50 || shopSupports50)).findFirst();
            }

            if (bestAmount.isPresent()) {
                action = "Buy " + bestAmount.getAsInt();
            }
            log.info("Buying {} {}", bestAmount.orElse(0), itemToBuy);
            if (action != null && itemToBuy.interact(action)) {
                final SpriteItem finalItem = itemToBuy;
                Execution.delayUntil(() -> !finalItem.isValid(), 300, 1800);
            }
        }
        return Inventory.getQuantity(filter) == initialAmount + quantity;
    }

    public static boolean sell(final Predicate<SpriteItem> filter, int quantity) {
        if (!isOpen()) {
            return false;
        }
        final int initialAmount = Inventory.getQuantity(filter);
        SpriteItem itemToSell;
        int amountToSell;
        if (quantity == 0) {
            SpriteItem firstItem = Inventory.getItems(filter).first();
            quantity = firstItem == null ? 0 : firstItem.getQuantity();
        }
        for (int retry = 0; retry < 10
            && (amountToSell = Inventory.getQuantity(filter) - (initialAmount - quantity)) > 0
            && (itemToSell = Inventory.getItems(filter).first()) != null
            && itemToSell.getQuantity() > 0; retry++) {
            String action = null;
            final int finalAmountToSell = amountToSell;
            OptionalInt bestAmount = Arrays.stream(AMOUNTS).filter(i -> finalAmountToSell >= i).findFirst();
            if (bestAmount.isPresent()) {
                action = "Sell " + bestAmount.getAsInt();
            }
            log.info("Selling {} {}", bestAmount.orElse(0), itemToSell);
            if (action != null && itemToSell.interact(action)) {
                final SpriteItem finalItemToSell = itemToSell;
                Execution.delayUntil(() -> !finalItemToSell.isValid(), 300, 1800);
            }
        }
        return Inventory.getQuantity(filter) == initialAmount - quantity;
    }
}
