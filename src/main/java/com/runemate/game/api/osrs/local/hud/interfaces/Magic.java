package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public enum Magic implements Spell {
    LUMBRIDGE_HOME_TELEPORT(406),
    WIND_STRIKE(65, 1),
    CONFUSE(66),
    ENCHANT_CROSSBOW_BOLT(408),
    WATER_STRIKE(67, 2),
    LVL_1_ENCHANT(68),
    EARTH_STRIKE(69, 3),
    WEAKEN(70),
    FIRE_STRIKE(71, 4),
    BONES_TO_BANANAS(72),
    WIND_BOLT(73, 5),
    CURSE(74),
    BIND(369),
    LOW_LEVEL_ALCHEMY(75),
    WATER_BOLT(76, 6),
    VARROCK_TELEPORT(77),
    GRAND_EXCHANGE_TELEPORT(77) {
        @Override
        public boolean activate() {
            return activate("Grand Exchange");
        }
    },
    LVL_2_ENCHANT(78),
    EARTH_BOLT(79, 7),
    LUMBRIDGE_TELEPORT(80),
    TELEKINETIC_GRAB(81),
    FIRE_BOLT(82, 8),
    FALADOR_TELEPORT(83),
    CRUMBLE_UNDEAD(84, 17),
    TELEPORT_TO_HOUSE(405),
    TELEPORT_OUTSIDE_HOUSE(405) {
        @Override
        public boolean activate() {
            return activate("Outside");
        }
    },
    WIND_BLAST(85, 9),
    SUPERHEAT_ITEM(86),
    CAMELOT_TELEPORT(87),
    SEERS_TELEPORT(87) {
        @Override
        public boolean activate() {
            return activate("Seers'");
        }
    },
    WATER_BLAST(88, 10),
    LVL_3_ENCHANT(89),
    IBAN_BLAST(103, 47),
    SNARE(370),
    MAGIC_DART(374, 18),
    ARDOUGNE_TELEPORT(104),
    EARTH_BLAST(90, 11),
    HIGH_LEVEL_ALCHEMY(91),
    CHARGE_WATER_ORB(92),
    LVL_4_ENCHANT(93),
    WATCHTOWER_TELEPORT(105),
    YANILLE_TELEPORT(105) {
        @Override
        public boolean activate() {
            return activate("Yanille");
        }
    },
    FIRE_BLAST(94, 12),
    CHARGE_EARTH_ORB(95),
    BONES_TO_PEACHES(404),
    SARADOMIN_STRIKE(111, 52),
    CLAWS_OF_GUTHIX(110, 19),
    FLAMES_OF_ZAMORAK(109, 20),
    TROLLHEIM_TELEPORT(373),
    WIND_WAVE(96, 13),
    CHARGE_FIRE_ORB(97),
    TELEPORT_TO_APE_ATOLL(407),
    WATER_WAVE(98, 14),
    CHARGE_AIR_ORB(99),
    VULNERABILITY(106),
    LVL_5_ENCHANT(100),
    KOUREND_CASTLE_TELEPORT(410),
    EARTH_WAVE(101, 15),
    ENFEEBLE(107),
    TELEOTHER_LUMBRIDGE(399),
    FIRE_WAVE(102, 16),
    ENTANGLE(371),
    STUN(108),
    CHARGE(372),
    WIND_SURGE(412, 48),
    TELEOTHER_FALADOR(400),
    WATER_SURGE(413, 49),
    TELE_BLOCK(402),
    TELEPORT_TO_BOUNTY_TARGET(409),
    LVL_6_ENCHANT(403),
    TELEOTHER_CAMELOT(401),
    EARTH_SURGE(414, 50),
    LEVEL_7_ENCHANT(411),
    FIRE_SURGE(415, 51);
    private static final int CONTAINER = 218;
    private final int disabledSpriteId;
    private final int enabledSpriteId;
    private final int autocastingValue;

    Magic(int disabledSpriteId) {
        this(disabledSpriteId, -1);
    }

    Magic(int disabledSpriteId, int autocastingVarbitValue) {
        this.disabledSpriteId = disabledSpriteId;
        this.enabledSpriteId = disabledSpriteId - 50;
        this.autocastingValue = autocastingVarbitValue;
    }

    /**
     * @return the currently selected spell from the enumeration, null if none is selected or an unrecognised spell is selected.
     */
    public static Magic getSelected() {
        InterfaceComponent selected = Interfaces.getSelected();
        if (selected != null) {
            int spriteId = selected.getSpriteId();
            if (spriteId != -1) {
                for (Magic magic : values()) {
                    if (magic.disabledSpriteId == spriteId || magic.enabledSpriteId == spriteId) {
                        return magic;
                    }
                }
            }
        }
        return null;
    }

    public static Spell getAutocastingSpell() {
        for (Magic magic : Magic.values()) {
            if (magic.isAutocasting()) {
                return magic;
            }
        }
        for (Lunar lunar : Lunar.values()) {
            if (lunar.isAutocasting()) {
                return lunar;
            }
        }
        for (Ancient ancient : Ancient.values()) {
            if (ancient.isAutocasting()) {
                return ancient;
            }
        }
        for (Arceuus arceuus : Arceuus.values()) {
            if (arceuus.isAutocasting()) {
                return arceuus;
            }
        }
        return null;
    }

    /**
     * Implementation detail so no longer exposed.
     *
     * @return an int that should be the sprite id when a spell is castable.
     */
    @Deprecated
    public int getSpriteIdWhenAvailable() {
        return enabledSpriteId;
    }

    /**
     * Implementation detail so no longer exposed.
     *
     * @return an int that should be the sprite id when a spell is not castable.
     */
    @Deprecated
    public int getSpriteIdWhenUnavailable() {
        return disabledSpriteId;
    }


    /**
     * Passes a custom string to Magic#active() to allow for interaction with spells with more than one action. For example,
     * Camelot Teleport and Varrock Teleport have more than one action upon completion of Achievement Diaries.
     * <p>
     * This defaults to cast if #active() is called (without the custom action string).
     *
     * @param action The action to perform. eg. "Cast" or "Camelot"
     * @return true the spell was successfully interacted with using the String provided.
     */
    public boolean activate(final String action) {
        final InterfaceComponent button;
        log.info("Activating {} with action {}", this, action);
        return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
            && (button = getComponent()) != null
            && button.isVisible()
            && button.interact(action);
    }


    /**
     * Activates the given spell using the action "Cast".
     *
     * @return true if the spell was successfully interacted with
     * @see #activate(String)
     */
    public boolean activate() {
        return activate("Cast");
    }


    /**
     * Deactivates a spell which is currently activated.
     *
     * @return If the spell was successfully deactivated or the spell was already deactivated.
     */
    public boolean deactivate() {
        if (!isSelected()) {
            return true;
        }
        log.info("Deactivating {}", this);
        final InterfaceComponent button;
        return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
            && (button = getComponent()) != null
            && button.isVisible()
            && button.click()
            && Execution.delayWhile(this::isSelected, 600, 1200);
    }

    /**
     * Gets the {@code InterfaceComponent} used to select/deselect the spell
     *
     * @return the {@code InterfaceComponent} used to interact with the spell
     */
    public InterfaceComponent getComponent() {
        return Interfaces.newQuery()
            .containers(CONTAINER)
            .grandchildren(false)
            .types(InterfaceComponent.Type.SPRITE)
            .sprites(disabledSpriteId, enabledSpriteId)
            .results().first();
    }

    /**
     * Determines whether or not a spell is currently selected by the border it has in the spellbook interface. eg. High Level Alchemy has been selected
     * but has not yet been cast on an item.
     *
     * @return true if the spell is currently selected
     */
    public boolean isSelected() {
        InterfaceComponent component = getComponent();
        return component != null && component.getSpriteBorderInset() == 2;
    }

    public boolean isAutocasting() {
        if (autocastingValue != -1) {
            Varbit varbit = Varbits.load(276);
            return varbit != null && varbit.getValue() == autocastingValue;
        }
        return false;
    }

    @Override
    public SpellBook getSpellBook() {
        return Book.STANDARD;
    }

    public enum Ancient implements Spell {
        EDGEVILLE_HOME_TELEPORT(406),
        SMOKE_RUSH(379, 31),
        SHADOW_RUSH(387, 32),
        PADDEWWA_TELEPORT(391),
        BLOOD_RUSH(383, 33),
        ICE_RUSH(375, 34),
        SENNTISTEN_TELEPORT(392),
        SMOKE_BURST(380, 35),
        SHADOW_BURST(388, 36),
        KHARYRLL_TELEPORT(393),
        BLOOD_BURST(384, 37),
        ICE_BURST(376, 38),
        LASSAR_TELEPORT(394),
        SMOKE_BLITZ(381, 39),
        SHADOW_BLITZ(389, 40),
        DAREEYAK_TELEPORT(395),
        BLOOD_BLITZ(385, 41),
        ICE_BLITZ(377, 42),
        CARRALLANGAR_TELEPORT(396),
        TELEPORT_TO_BOUNTY_TARGET(409),
        SMOKE_BARRAGE(382, 43),
        SHADOW_BARRAGE(390, 44),
        ANNAKARL_TELEPORT(397),
        BLOOD_BARRAGE(386, 45),
        ICE_BARRAGE(378, 46),
        GHORROCK_TELEPORT(398);
        private static final int CONTAINER = 218;
        private final int disabledSpriteId;
        private final int enabledSpriteId;
        private final int autocastingValue;

        Ancient(int disabledSpriteId) {
            this(disabledSpriteId, -1);
        }

        Ancient(int disabledSpriteId, int autocastingVarbitValue) {
            this.disabledSpriteId = disabledSpriteId;
            this.enabledSpriteId = disabledSpriteId - 50;
            this.autocastingValue = autocastingVarbitValue;
        }


        public static Ancient getSelected() {
            InterfaceComponent selected = Interfaces.getSelected();
            if (selected != null) {
                int spriteId = selected.getSpriteId();
                if (spriteId != -1) {
                    for (Ancient magic : values()) {
                        if (magic.disabledSpriteId == spriteId ||
                            magic.enabledSpriteId == spriteId) {
                            return magic;
                        }
                    }
                }
            }
            return null;
        }

        public int getSpriteIdWhenAvailable() {
            return enabledSpriteId;
        }

        public int getSpriteIdWhenUnavailable() {
            return disabledSpriteId;
        }

        /**
         * Activates the desired spell using the specified action.
         *
         * @param action the menu action to interact with
         * @return True if the interaction was successful.
         * @see Magic#activate(String)
         */
        public boolean activate(final String action) {
            log.info("Activating {} with action {}", this, action);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible()
                && button.interact(action);
        }

        /**
         * @see Magic#activate()
         */
        public boolean activate() {
            return activate("Cast");
        }

        /**
         * @return True if the interaction to deactivate the spell was successfully.
         * @see Magic#deactivate()
         */
        public boolean deactivate() {
            log.info("Deactivating {}", this);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible() && button.interact("Cancel");
        }

        /**
         * @see Magic#getComponent()
         */
        public InterfaceComponent getComponent() {
            return Interfaces.newQuery().grandchildren(false).containers(CONTAINER)
                .types(InterfaceComponent.Type.SPRITE)
                .sprites(disabledSpriteId, enabledSpriteId).results().first();
        }

        /**
         * @see Magic#isSelected()
         */
        public boolean isSelected() {
            InterfaceComponent component = getComponent();
            return component != null && component.getSpriteBorderInset() == 2;
        }

        public boolean isAutocasting() {
            if (autocastingValue != -1) {
                Varbit varbit = Varbits.load(276);
                return varbit != null && varbit.getValue() == autocastingValue;
            }
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.ANCIENT;
        }
    }

    public enum Arceuus implements Spell {
        @Deprecated REANIMATE_GOBLIN(-1, -1, "Reanimate"),
        @Deprecated LUMBRIDGE_GRAVEYARD_TELEPORT(-1, -1),
        @Deprecated REANIMATE_MONKEY(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_IMP(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_MINOTAUR(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_SCORPION(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_BEAR(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_UNICORN(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_DOG(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_CHAOS_DRUID(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_GIANT(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_OGRE(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_ELF(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_TROLL(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_HORROR(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_KALPHITE(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_DAGANNOTH(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_BLOODVELD(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_TZHAAR(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_DEMON(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_AVIANSIE(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_ABYSSAL_CREATURE(-1, -1, "Reanimate"),
        @Deprecated REANIMATE_DRAGON(-1, -1, "Reanimate"),


        ARCEUUS_HOME_TELEPORT(1276, 1251),
        ARCEUUS_LIBRARY_TELEPORT(1277, 1252),
        BASIC_REANIMATION(1272, 1247, "Reanimate"),
        DRAYNOR_MANOR_TELEPORT(1278, 1253),
        BATTLEFRONT_TELEPORT(1280, 1255),
        MIND_ALTAR_TELEPORT(1281, 1256),
        RESPAWN_TELEPORT(1282, 1257),
        GHOSTLY_GRASP(1292, 1267, 56),
        RESURRECT_LESSER_GHOST(1295, 1270),
        RESURRECT_LESSER_SKELETON(1296, 1271),
        RESURRECT_LESSER_ZOMBIE(1319, 1300),
        SALVE_GRAVEYARD_TELEPORT(1283, 1258),
        ADEPT_REANIMATION(1273, 1248, "Reanimate"),
        INFERIOR_DEMONBANE(1321, 1302, 53),
        SHADOW_VEIL(1334, 1315),
        FENKENSTRAINS_CASTLE_TELEPORT(1284, 1259),
        DARK_LURE(1335, 1316),
        SKELETAL_GRASP(1293, 1268, 57),
        RESURRECT_SUPERIOR_GHOST(2985, 2979),
        RESURRECT_SUPERIOR_SKELETON(2987, 2981),
        RESURRECT_SUPERIOR_ZOMBIE(2989, 2983),
        MARK_OF_DARKNESS(1324, 1305),
        WEST_ARDOUGNE_TELEPORT(1285, 1260),
        SUPERIOR_DEMONBANE(1322, 1303, 54),
        LESSER_CORRUPTION(1326, 1307),
        HARMONY_ISLAND_TELEPORT(1286, 1261),
        VILE_VIGOUR(1336, 1317),
        DEGRIME(1337, 1318),
        CEMETERY_TELEPORT(1289, 1264),
        EXPERT_REANIMATION(1274, 1249, "Reanimate"),
        WARD_OF_ARCEUUS(1325, 1306),
        RESURRECT_GREATER_GHOST(2986, 2980),
        RESURRECT_GREATER_SKELETON(2988, 2982),
        RESURRECT_GREATER_ZOMBIE(2990, 2984),
        RESURRECT_CROPS(1291, 1266),
        UNDEAD_GRASP(1294, 1269, 58),
        DEATH_CHARGE(1329, 1310),
        DARK_DEMONBANE(1323, 1304, 55),
        BARROWS_TELEPORT(1287, 1262),
        DEMONIC_OFFERING(1330, 1311),
        GREATER_CORRUPTION(1327, 1308),
        MASTER_REANIMATION(1275, 1250, "Reanimate"),
        APE_ATOLL_TELEPORT(1288, 1263),
        SINISTER_OFFERING(1331, 1312);

        private static final int CONTAINER = 218;
        private final int disabledSpriteId;
        private final int enabledSpriteId;
        private final int autocastVarbitState;
        private final String action;

        Arceuus(int disabledSpriteId, int enabledSpriteId) {
            this(disabledSpriteId, enabledSpriteId, "Cast");
        }

        Arceuus(int disabledSpriteId, int enabledSpriteId, String action) {
            this.disabledSpriteId = disabledSpriteId;
            this.enabledSpriteId = enabledSpriteId;
            this.action = action;
            this.autocastVarbitState = -1;
        }

        Arceuus(int disabledSpriteId, int enabledSpriteId, int autocastVarbitState) {
            this.disabledSpriteId = disabledSpriteId;
            this.enabledSpriteId = enabledSpriteId;
            this.autocastVarbitState = autocastVarbitState;
            this.action = "Cast";
        }

        public static Arceuus getSelected() {
            InterfaceComponent selected = Interfaces.getSelected();
            if (selected != null) {
                int spriteId = selected.getSpriteId();
                if (spriteId != -1) {
                    for (Arceuus magic : values()) {
                        if (magic.disabledSpriteId == spriteId ||
                            magic.enabledSpriteId == spriteId) {
                            return magic;
                        }
                    }
                }
            }
            return null;
        }

        public int getSpriteIdWhenAvailable() {
            return enabledSpriteId;
        }

        public int getSpriteIdWhenUnavailable() {
            return disabledSpriteId;
        }

        /**
         * Activates the desired spell using the specified action.
         *
         * @param action the menu action to interact with
         * @return True if the interaction was successful.
         * @see Magic#activate(String)
         */
        public boolean activate(final String action) {
            log.info("Activating {} with action {}", this, action);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open()) &&
                (button = getComponent()) != null && button.isVisible() && button.interact(action);
        }


        /**
         * @see Magic#activate()
         */
        public boolean activate() {
            return activate(action);
        }

        /**
         * @return True if the interaction to deactivate the spell was successfully.
         * @see Magic#deactivate()
         */
        public boolean deactivate() {
            log.info("Deactivating {}", this);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible() && button.interact("Cancel");
        }

        /**
         * @see Magic#getComponent()
         */
        public InterfaceComponent getComponent() {
            return Interfaces.newQuery().grandchildren(false).containers(CONTAINER)
                .types(InterfaceComponent.Type.SPRITE)
                .sprites(disabledSpriteId, enabledSpriteId).results().first();
        }

        /**
         * @see Magic#isSelected()
         */
        public boolean isSelected() {
            InterfaceComponent component = getComponent();
            return component != null && component.getSpriteBorderInset() == 2;
        }

        @Override
        public boolean isAutocasting() {
            if (autocastVarbitState != -1) {
                Varbit varbit = Varbits.load(276);
                return varbit != null && varbit.getValue() == autocastVarbitState;
            }
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.ARCEUUS;
        }
    }

    public enum Lunar implements Spell {
        LUNAR_HOME_TELEPORT(406),
        BAKE_PIE(593),
        GEOMANCY(613),
        CURE_PLANT(617),
        MONSTER_EXAMINE(627),
        NPC_CONTACT(618),
        CURE_OTHER(609),
        HUMIDIFY(628),
        MOONCLAN_TELEPORT(594),
        OURANIA_TELEPORT(636),
        TELE_GROUP_MOONCLAN(619),
        CURE_ME(612),
        HUNTER_KIT(629),
        WATERBIRTH_TELEPORT(595),
        TELE_GROUP_WATERBIRTH(620),
        CURE_GROUP(615),
        STAT_SPY(626),
        BARBARIAN_TELEPORT(597),
        TELE_GROUP_BARBARIAN(621),
        SUPERGLASS_MAKE(598),
        KHAZARD_TELEPORT(599),
        TELE_GROUP_KHAZARD(622),
        DREAM(630),
        STRING_JEWELLERY(600),
        SPIN_FLAX(585),
        STAT_RESTORE_POT_SHARE(604),
        MAGIC_IMBUE(602),
        FERTILE_SOIL(603),
        BOOST_POTION_SHARE(601),
        FISHING_GUILD_TELEPORT(605),
        TELE_GROUP_FISHING_GUILD(623),
        PLANK_MAKE(631),
        CATHERBY_TELEPORT(606),
        TELE_GROUP_CATHERBY(624),
        RECHARGE_DRAGONSTONE(634),
        ICE_PLATEAU_TELEPORT(607),
        TELE_GROUP_ICE_PLATEAU(625),
        ENERGY_TRANSFER(608),
        HEAL_OTHER(610),
        VENGEANCE_OTHER(611),
        VENGEANCE(614),
        HEAL_GROUP(616),
        SPELLBOOK_SWAP(632),
        TAN_LEATHER(633);
        private static final int CONTAINER = 218;
        private final int disabledSpriteId;
        private final int enabledSpriteId;

        Lunar(int disabledSpriteId) {
            this.disabledSpriteId = disabledSpriteId;
            this.enabledSpriteId = disabledSpriteId - 50;
        }

        public static Lunar getSelected() {
            InterfaceComponent selected = Interfaces.getSelected();
            if (selected != null) {
                int spriteId = selected.getSpriteId();
                if (spriteId != -1) {
                    for (Lunar magic : values()) {
                        if (magic.disabledSpriteId == spriteId ||
                            magic.enabledSpriteId == spriteId) {
                            return magic;
                        }
                    }
                }
            }
            return null;
        }

        public int getSpriteIdWhenAvailable() {
            return enabledSpriteId;
        }

        public int getSpriteIdWhenUnavailable() {
            return disabledSpriteId;
        }

        /**
         * Activates the desired spell using the specified action.
         *
         * @param action the menu action to interact with
         * @return True if the interaction was successful.
         * @see Magic#activate(String)
         */
        public boolean activate(final String action) {
            log.info("Activating {} with action {}", this, action);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible()
                && button.interact(action);
        }

        /**
         * @see Magic#activate()
         */
        public boolean activate() {
            return activate("Cast");
        }

        /**
         * @see Magic#deactivate()
         */
        public boolean deactivate() {
            log.info("Deactivating {}", this);
            final InterfaceComponent button;
            return (ControlPanelTab.MAGIC.isOpen() || ControlPanelTab.MAGIC.open())
                && (button = getComponent()) != null
                && button.isVisible()
                && button.interact("Cancel");
        }

        /**
         * @see Magic#getComponent()
         */
        public InterfaceComponent getComponent() {
            return Interfaces.newQuery()
                .containers(CONTAINER)
                .grandchildren(false)
                .types(InterfaceComponent.Type.SPRITE)
                .sprites(disabledSpriteId, enabledSpriteId)
                .results()
                .first();
        }

        /**
         * @see Magic#isSelected()
         */
        public boolean isSelected() {
            InterfaceComponent component = getComponent();
            return component != null && component.getSpriteBorderInset() == 2;
        }

        @Override
        public boolean isAutocasting() {
            return false;
        }

        @Override
        public SpellBook getSpellBook() {
            return Book.LUNAR;
        }
    }

    public enum Book implements SpellBook {
        STANDARD,
        ANCIENT,
        LUNAR,
        ARCEUUS;

        /**
         * @return the currently activated spell-book from the enumeration
         */
        public static Book getCurrent() {
            switch (Varps.getAt(439).getValue() & 0b11) {
                case 0:
                    return STANDARD;
                case 1:
                    return ANCIENT;
                case 2:
                    return LUNAR;
                case 0x3:
                    return ARCEUUS;
                default:
                    return null;
            }
        }

        public static Book get(int id) {
            for (Book book : values()) {
                if (book.getId() == id) {
                    return book;
                }
            }
            return null;
        }

        @Override
        public int getId() {
            return 439 + ordinal();
        }

        @Override
        public boolean isCurrent() {
            return this == getCurrent();
        }
    }

    @RequiredArgsConstructor
    public enum Filter {
        SHOW_COMBAT_SPELLS(6605),
        SHOW_TELEPORT_SPELLS(6609),
        SHOW_UTILITY_SPELLS(6606),
        SHOW_MISSING_LEVEL(6607),
        SHOW_MISSING_RUNES(6608),
        SHOW_MISSING_REQUIREMENTS(12137),
        ICON_RESIZING(6548);

        private final int varbit;

        public boolean isEnabled() {
            final var varbit = Varbits.load(this.varbit);
            return varbit != null && varbit.getValue() == 0;
        }
    }
}
