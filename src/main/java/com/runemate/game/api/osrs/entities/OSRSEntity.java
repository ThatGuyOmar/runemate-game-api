package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.shapes.*;
import java.awt.*;
import javafx.scene.canvas.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public abstract class OSRSEntity extends OSRSCacheModelEntity {

    protected Model cacheModel;

    OSRSEntity(long uid) {
        super(uid);
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        final Coordinate position = getPosition(regionBase);
        return position != null ? position.getArea() : null;
    }

    @Nullable
    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        var real = getModel();
        if (real != null) {
            return real.getInteractionPoint();
        }
        log.trace("Falling back to legacy point selection");
        return super.getInteractionPoint(origin);
    }

    @Override
    public boolean isHovered() {
        if (this instanceof Player || this instanceof Npc || this instanceof GameObject || this instanceof GroundItem) {
            return Region.getHoveredEntities().contains(this);
        }
        return super.isHovered();
    }

    @Override
    public boolean contains(Point point) {
        final var real = getShape();
        if (real != null) {
            return real.contains(point);
        }
        return super.contains(point);
    }

    @Override
    public final boolean isVisible() {
        if (!isValid()) {
            return false;
        }

        final var shape = getShape();
        if (shape != null) {
            final var area = new ViewportArea(shape);
            return area.getVisibility(Projection.getViewport()) > 0.0;
        }

        return super.isVisible();
    }

    @Override
    public final double getVisibility() {
        if (!isValid()) {
            return 0;
        }

        final var real = getShape();
        if (real != null) {
            final var area = new ViewportArea(real);
            return area.getVisibility(Projection.getViewport());
        }
        return super.getVisibility();
    }

    @Nullable
    @Override
    public Model getModel() {
        final var shape = getShape();
        return shape != null ? new RemoteModel(this, shape) : null;
    }

    private Shape getShape() {
        return OpenHull.lookup(uid);
    }

    @Override
    public void render(final Graphics2D g2d) {
        var model = getModel();
        if (model != null) {
            model.render(g2d);
        }
    }

    @Override
    public void render(final GraphicsContext gc) {
        var model = getModel();
        if (model != null) {
            model.render(gc);
        }
    }

    @Override
    public String toString() {
        return "OSRSEntity";
    }
}
