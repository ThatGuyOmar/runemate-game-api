package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import com.runemate.game.api.osrs.local.hud.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSNpcs {
    private OSRSNpcs() {
    }


    public static LocatableEntityQueryResults<Npc> getLoaded(final Predicate<? super Npc> filter) {
        return new LocatableEntityQueryResults<>(Arrays.stream(OpenNpc.getLoaded())
            .filter(it -> it > 0)
            .mapToObj(OSRSNpc::new)
            .filter(it -> filter == null || filter.test(it))
            .collect(Collectors.toList()));
    }


    public static OSRSNpc getByIndex(final int index) {
        //The arrays size is 32768
        final long npc_uid = OpenNpc.getNpcByIndex(index);
        return npc_uid != 0 ? new OSRSNpc(npc_uid) : null;
    }


    public static OSRSModel lookupModel(final int id, final LocatableEntity entity) {
        long modeluid = OpenNpc.lookupNpcModel(id);
        if (modeluid != 0) {
            var points = OpenHull.lookupPoints(modeluid);
            if (points.length > 0) {
                return new OSRSModel(modeluid, entity, points);
            }
        }
        return null;
    }
}
