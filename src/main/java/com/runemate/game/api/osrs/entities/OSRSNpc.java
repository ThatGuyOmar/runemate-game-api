package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import lombok.*;
import javax.annotation.Nullable;

public final class OSRSNpc extends OSRSActor implements Npc {

    private OpenNpc npc;

    public OSRSNpc(long uid) {
        super(uid);
    }

    @Override
    protected OpenNpc actor() {
        if (npc == null) {
            npc = OpenNpc.create(uid);
        }
        return npc;
    }


    @Override
    public int getId() {
        return OpenClient.getNpcIdFromComposite(actor().getComposite());
    }

    @Override
    public int getLevel() {
        final NpcDefinition def = getDefinition();
        return def != null ? def.getLevel() : -1;
    }

    @Override
    @Nullable
    public NpcDefinition getDefinition() {
        return NpcDefinition.get(getId());
    }

    @NonNull
    @Override
    public List<OverheadIcon> getOverheadIcons() {
        final NpcDefinition definition = getActiveDefinition();
        return definition != null ? definition.getOverheadIcons() : Collections.emptyList();
    }

    @Override
    public String getName() {
        NpcDefinition def = getDefinition();
        if (def != null) {
            String name = def.getName();
            if ("null".equals(name)) {
                def = def.getLocalState();
                if (def != null) {
                    return def.getName();
                }
            }
            return name;
        }
        return null;
    }

    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        var real = super.getModel();
        if (real instanceof RemoteModel) {
            return real;
        }
        if (cacheModel != null && cacheModel.isValid()) {
            return cacheModel;
        }
        CacheNpcDefinition.Extended def = (CacheNpcDefinition.Extended) getDefinition();
        if (def != null) {
            final NpcDefinition localState = def.getLocalState();
            if (localState != null) {
                def = (CacheNpcDefinition.Extended) localState;
            }
            final List<Integer> appearance = def.getAppearance();
            final List<CacheModel> components = new ArrayList<>(appearance.size());
            for (int index = 0; index < appearance.size(); ++index) {
                final CacheModel component = CacheModel.load(appearance.get(index));
                if (component != null) {
                    int[][] translations = def.getXYZModelTranslations();
                    if (translations != null) {
                        component.translate(translations[index]);
                    }
                    components.add(component);
                }
            }
            if (!components.isEmpty()) {
                CompositeCacheModel model = new CompositeCacheModel(this, components);
                model.setScale(def.getModelXZScale(), def.getModelYScale(), def.getModelXZScale());
                return cacheModel = model;
            }
        }
        return backupModel;
    }

    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        Coordinate position = getPosition(regionBase);
        if (position == null) {
            return null;
        }
        int size = getSize();
        if (size == 1) {
            return new Area.Rectangular(position);
        }
        return Area.rectangular(position, position.derive(size - 1, size - 1));
    }


    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        if (regionBase == null) {
            regionBase = Region.getBase();
        }
        var base = regionBase.getHighPrecisionPosition();
        var size = getSize();
        var x = actor().getRegionX() - 128 * (size - 1) / 2;
        var y = actor().getRegionY() - 128 * (size - 1) / 2;
        return new Coordinate.HighPrecision(base.getX() + x, base.getY() + y, regionBase.getPlane());
    }

    private int getSize() {
        int size = 1;
        var def = getDefinition();
        if (def != null) {
            var ls = def.getLocalState();
            if (ls != null) {
                def = ls;
            }
        }
        if (def != null) {
            size = def.getAreaEdgeLength();
        }
        return size;
    }

    @Override
    public boolean isValid() {
        return OpenClient.validate(uid) && getId() != -1;
    }
}
