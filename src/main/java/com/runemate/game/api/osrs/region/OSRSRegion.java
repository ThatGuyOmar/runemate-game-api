package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.internal.*;
import java.util.*;

@InternalAPI
public final class OSRSRegion {
    private OSRSRegion() {
    }

    public static void setStoredBase(Coordinate base) {
        final var bot = Environment.getBot();
        if (bot != null) {
            bot.getCache().put("RegionBase", new Coordinate(base.getX(), base.getY(), 0));
            bot.getCache().put("CurrentPlane", base.getPlane());
        }
    }

    public static Coordinate getStoredBase() {
        final var bot = Environment.getBot();
        if (bot == null) {
            return null;
        }
        Coordinate base = (Coordinate) bot.getCache().get("RegionBase");
        Integer plane = (Integer) bot.getCache().get("CurrentPlane");
        if (base == null || plane == null) {
            return null;
        }

        return new Coordinate(base.getX(), base.getY(), plane);
    }

    /**
     * Retrieves the cached region base if available, otherwise defers to {@link #getCurrentBase()}
     */
    public static Coordinate getBase() {
        var base = getStoredBase();
        if (base == null) {
            base = getCurrentBase();
            setStoredBase(base);
        }
        return base;
    }

    /**
     * Retrieves the current region base from the game client, rather than the (potentially) cached base from {@link #getBase()}
     */
    public static Coordinate getCurrentBase() {
        return new Coordinate(OpenRegion.getBaseX(), OpenRegion.getBaseY(), OpenRegion.getPlane());
    }

    public static int getCurrentPlane() {
        final AbstractBot bot = Environment.getBot();
        Integer plane = (Integer) bot.getCache().get("CurrentPlane");
        if (plane == null) {
            plane = OpenRegion.getPlane();
            bot.getCache().put("CurrentPlane", plane);
        }
        return plane;
    }


    public static Coordinate getBase(int plane) {
        final AbstractBot bot = Environment.getBot();
        int xBase;
        int yBase;
        if (bot != null) {
            Coordinate base = (Coordinate) bot.getCache().get("RegionBase");
            if (base != null) {
                xBase = base.getX();
                yBase = base.getY();
            } else {
                xBase = OpenRegion.getBaseX();
                yBase = OpenRegion.getBaseY();
                bot.getCache().put("RegionBase", new Coordinate(xBase, yBase, 0));
            }
            return new Coordinate(xBase, yBase, plane);
        }
        //Where did this instance of client come from? If not the bot,
        // then can it be safely trusted for our cached base?
        return new Coordinate(
            OpenRegion.getBaseX(),
            OpenRegion.getBaseY(),
            plane
        );
    }

    public static List<Entity> getHoveredEntities() {
        int hoveredEntityUidCount = OpenClient.getHoveredEntityCount();
        if (hoveredEntityUidCount <= 0) {
            return Collections.emptyList();
        }
        List<Entity> hoveredEntities = new ArrayList<>(hoveredEntityUidCount);
        long[] hoveredEntityUids = OpenClient.getHoveredEntityUids();
        if (hoveredEntityUids == null) {
            return Collections.emptyList();
        }
        long previousHoveredEntityUID = -1;
        for (int index = 0; index < hoveredEntityUidCount; index++) {
            long hoveredEntityUid = hoveredEntityUids[index];
            if (hoveredEntityUid != previousHoveredEntityUID) {
                previousHoveredEntityUID = hoveredEntityUid;
                /*
                 * 0 = Player
                 * 1 = Npc
                 * 2 = SceneObject
                 * 3 = GroundItem
                 */
                int entityType = (int) (hoveredEntityUid >>> 14 & 3L);
                int entityIdentifier = (int) (hoveredEntityUid >>> 17 & 4294967295L);
                int regionX = (int) (hoveredEntityUid & 127L);
                int regionY = (int) (hoveredEntityUid >>> 7 & 127L);
                /*
                The entityIdentifier is _blank_ if the entityType is:

                Player = player[] index
                Npc = npc[] index
                SceneObject = id
                GroundItem = ?
                */
                if (entityType == 0) {
                    hoveredEntities.add((Entity) Players.getAt(entityIdentifier));
                } else if (entityType == 1) {
                    hoveredEntities.add((Entity) Npcs.getAt(entityIdentifier));
                } else if (entityType == 2) {
                    Coordinate rbase = getBase();
                    Coordinate global =
                        new Coordinate(regionX + rbase.getX(), regionY + rbase.getY(),
                            getCurrentPlane()
                        );
                    hoveredEntities.add(
                        (Entity) GameObjects.getLoadedOn(global, entityIdentifier).first());
                } else if (entityType == 3) {
                    Coordinate rbase = getBase();
                    Coordinate global =
                        new Coordinate(regionX + rbase.getX(), regionY + rbase.getY(),
                            getCurrentPlane()
                        );
                    for (GroundItem stackEntry : GroundItems.getLoadedOn(global)) {
                        hoveredEntities.add((Entity) stackEntry);
                    }
                }
            }
        }
        return hoveredEntities;
    }
}
