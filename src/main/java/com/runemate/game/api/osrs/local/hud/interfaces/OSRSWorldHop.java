package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import lombok.extern.log4j.*;

@Log4j2
public class OSRSWorldHop {

    private static final int CONTAINER_INDEX = 69;

    public static boolean isOpen() {
        return ControlPanelTab.WORLD_HOP.isOpen();
    }

    public static boolean open() {
        return ControlPanelTab.WORLD_HOP.open();
    }

    public static boolean close() {
        final InterfaceComponent component = Interfaces.newQuery()
            .containers(69)
            .actions("Close")
            .sprites(535)
            .heights(23)
            .widths(26)
            .types(InterfaceComponent.Type.SPRITE)
            .results()
            .first();
        return component != null && component.interact("Close");
    }

    public static boolean to(int targetWorld) {
        log.info("Hopping to world {}", targetWorld);
        if (targetWorld < 0) {
            log.warn("Invalid request to hop to world {}", targetWorld);
            return false;
        }
        int initialWorld = Worlds.getCurrent();
        if (targetWorld == initialWorld) {
            log.info("The target world of {} is already loaded.", targetWorld);
            return true;
        }
        log.debug("Attempting to hop to world {} from {}", targetWorld, initialWorld);
        if (open()) {
            final InterfaceComponent wc = getWorldComponent(targetWorld);
            if (wc == null) {
                log.warn("Couldn't find world component");
                return false;
            }
            final InterfaceComponent cc = getContainerComponent();
            if (cc == null) {
                log.warn("Couldn't find container component");
                return false;
            }
            if (Interfaces.scrollTo(wc, cc)
                && wc.interact("Switch", Integer.toString(targetWorld))
                && Execution.delayUntil(() -> Worlds.getCurrent() == targetWorld || getHopWarningDialog() != null, 6000, 9000)) {
                ChatDialog.Option confirm = getHopWarningDialog();
                if (confirm != null && confirm.select()) {
                    Execution.delayUntil(() -> Worlds.getCurrent() == targetWorld, 6000, 9000);
                }
            }
        }
        return targetWorld == Worlds.getCurrent();
    }

    private static ChatDialog.Option getHopWarningDialog() {
        return ChatDialog.getOption("Yes. In future, only warn about dangerous worlds.",
            "Switch to the Deadman world - and your Deadman profile.",
            "Switch to the PvP world.",
            "Switch to the Access-All-Areas world - and temporary profile."
        );
    }

    private static InterfaceComponent getContainerComponent() {
        return Interfaces.newQuery()
            .containers(CONTAINER_INDEX)
            .types(InterfaceComponent.Type.CONTAINER)
            .grandchildren(false)
            .widths(174)
            .heights(193)
            .results()
            .first();
    }

    private static InterfaceComponent getWorldComponent(int worldId) {
        return Interfaces.newQuery()
            .containers(CONTAINER_INDEX)
            .types(InterfaceComponent.Type.SPRITE)
            .grandchildren(true)
            .names(Integer.toString(worldId))
            .visible()
            .results()
            .first();
    }
}