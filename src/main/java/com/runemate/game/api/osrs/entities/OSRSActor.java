package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.entities.status.*;
import com.runemate.game.api.osrs.region.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;
import org.jetbrains.annotations.*;

abstract class OSRSActor extends OSRSEntity implements Actor {

    OSRSActor(long uid) {
        super(uid);
        setBackupModel(BoundingModel.HUMANOID);
    }

    protected abstract OpenActor actor();


    @Override
    public final int getAnimationId() {
        return actor().getAnimationId();
    }


    @Override
    public final int getAnimationFrame() {
        return actor().getAnimationFrame();
    }


    @Override
    public int getStanceId() {
        return actor().getStanceId();
    }


    @Override
    public final int getStanceFrame() {
        return actor().getStanceFrame();
    }


    @Override
    public final int getHighPrecisionOrientation() {
        int orientation = actor().getOrientation();
        if (orientation == -1) {
            return 0;
        }
        return orientation;
    }


    @Override
    public final int getOrientationAsAngle() {
        int orientation = actor().getOrientation();
        if (orientation == -1) {
            return 0;
        }
        orientation = (orientation >> 8) * 45;
        return orientation;
    }

    @Override
    public boolean isFacing(Locatable locatable) {
        return RotatableCommons.isFacing(this, locatable);
    }


    @Override
    public final boolean isMoving() {
        return actor().getSpeed() > 0;
    }

    /**
     * The health gauge displayed during combat
     *
     * @return The health gauge if visible, else null
     */

    @Override
    @Nullable
    public final CombatGauge getHealthGauge() {
        final long barUid = actor().getHealthbar();
        return barUid == 0L ? null : new OSRSCombatGauge(barUid);
    }


    @Override
    @Nullable
    public final String getDialogue() {
        return actor().getDialogue();
    }


    @Override
    @Nullable
    public final Actor getTarget() {
        final int index = actor().getTargetIndex();
        if (index != -1) {
            if (index < 0x10000) {
                return OSRSNpcs.getByIndex(index);
            } else {
                return OSRSPlayers.getAt(index - 0x10000);
            }
        }
        return null;
    }


    @Override
    @NonNull
    public List<Integer> getSpotAnimationIds() {
        return actor().getSpotAnimations().stream().map(OpenSpotAnimationDefinition::getId).collect(Collectors.toList());
    }

    @Override
    @Nullable
    public final Coordinate getPosition(Coordinate regionBase) {
        final var hp = getHighPrecisionPosition(regionBase);
        return hp != null ? hp.reducePrecision(7).asCoordinate() : null;
    }


    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        if (regionBase == null) {
            regionBase = Region.getBase();
        }
        return new Coordinate.HighPrecision(
            (regionBase.getX() << 7) + actor().getRegionX(),
            (regionBase.getY() << 7) + actor().getRegionY(),
            regionBase.getPlane()
        );
    }


    @Override
    @NonNull
    public List<Hitsplat> getHitsplats() {
        int[] endCycles = actor().getHitsplatEndCycles();
        if (endCycles != null && endCycles.length > 0) {
            int[] typeIds = actor().getHitsplatTypeIds();
            if (typeIds != null && typeIds.length > 0) {
                int[] secondaryHitsplatIds = actor().getSecondaryHitsplatTypeIds();
                if (secondaryHitsplatIds != null && secondaryHitsplatIds.length > 0) {
                    int[] damages = actor().getHitsplatDamages();
                    if (damages != null && damages.length > 0) {
                        int[] secondaryDamages = actor().getSecondaryHitsplatDamages();
                        if (secondaryDamages != null && secondaryDamages.length > 0) {
                            int currentCycle = OpenClient.getGlobalCycle();
                            List<Hitsplat> hitsplats = new ArrayList<>(damages.length);
                            for (int i = 0; i < damages.length; ++i) {
                                if (endCycles[i] > currentCycle) {
                                    hitsplats.add(new Hitsplat(typeIds[i], damages[i],
                                        secondaryHitsplatIds[i], secondaryDamages[i],
                                        endCycles[i]
                                    ));
                                }
                            }
                            return hitsplats;
                        }
                    }
                }
            }
        }
        return Collections.emptyList();
    }

    @Override
    public Coordinate getServerPosition() {
        final var path = getPath();
        return path.isEmpty() ? getPosition() : path.get(0);
    }

    @NonNull
    @Override
    public List<Coordinate> getPath() {
        int pathLength = actor().getSpeed();
        if (pathLength > 0) {
            Coordinate base = OSRSRegion.getBase();
            List<Coordinate> path = new ArrayList<>(pathLength);
            int[] pathX = actor().getPathX();
            int[] pathY = actor().getPathY();
            for (int i = 0; i < pathLength; ++i) {
                path.add(base.derive(pathX[i], pathY[i]));
            }
            return path;
        }
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        String wip = getName() + '(';
        if (this instanceof Npc) {
            wip += "level: " + ((Npc) this).getLevel();
        } else if (this instanceof Player) {
            wip += "level: " + ((Player) this).getCombatLevel();
        }
        final Coordinate p = getPosition();
        if (p != null) {
            wip += ", position: " + p.getX() + ", " + p.getY() + ", " + p.getPlane();
        }
        wip += ')';
        return wip;
    }
}
