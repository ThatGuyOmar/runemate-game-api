package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.script.annotations.*;
import com.runemate.game.api.script.exceptions.*;
import java.util.*;

public final class OSRSProjectile extends OSRSCacheModelEntity implements Projectile {

    private OpenProjectile projectile;

    public OSRSProjectile(long uid) {
        super(uid);
    }

    private OpenProjectile rmi() {
        if (projectile == null) {
            projectile = OpenProjectile.create(uid);
        }
        return projectile;
    }

    @Override
    public int getId() {
        return getSpotAnimationId();
    }

    @Override
    public int getHighPrecisionOrientation() {
        int orientation = rmi().getOrientation();
        if (orientation == -1) {
            return 0;
        }
        return orientation;
    }

    @Override
    public int getOrientationAsAngle() {
        int orientation = rmi().getOrientation();
        if (orientation == -1) {
            return 0;
        }
        orientation = (orientation >> 8) * 45;
        return orientation;
    }

    @RS3Only
    @Override
    public Actor getSource() {
        throw new OSRSException("Projectile", "getSource");
    }

    @Override
    public Actor getTarget() {
        int index = rmi().getTargetIndex();
        if (index > 0) {
            return OSRSNpcs.getByIndex(index - 1);
        }
        if (index < 0) {
            int pindex = -index - 1;
            if (OpenPlayer.getLocalIndex() == pindex) {
                return OSRSPlayers.getLocal();
            }
            return OSRSPlayers.getAt(pindex);
        }
        return null;
    }

    @Override
    public int getSpotAnimationId() {
        return rmi().getSpotAnimationId();
    }

    public SpotAnimationDefinition getDefinition() {
        return SpotAnimationDefinitions.load(getSpotAnimationId());
    }

    @OSRSOnly
    @Override
    public int getLaunchCycle() {
        return rmi().getLaunchCycle();
    }

    @OSRSOnly
    @Override
    public int getImpactCycle() {
        return rmi().getImpactCycle();
    }

    @OSRSOnly
    @Override
    public Coordinate getLaunchPosition() {
        Coordinate regionBase = Region.getBase();
        return new Coordinate(regionBase.getX() + (rmi().getStartRegionX() >> 7),
            regionBase.getY() + (rmi().getStartRegionY() >> 7), regionBase.getPlane()
        );
    }

    @Override
    public boolean hasLaunched() {
        return rmi().isLaunched();
    }

    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        if (cacheModel != null && cacheModel.isValid()) {
            return cacheModel;
        }
        SpotAnimationDefinition definition = getDefinition();
        if (definition != null) {
            CacheModel model = CacheModel.load(definition.getModelId());
            if (model != null) {
                return cacheModel = new CompositeCacheModel(this, Collections.singletonList(model));
            }
        }
        return backupModel;
    }

    @Override
    public Coordinate getPosition(Coordinate regionBase) {
        if (regionBase == null) {
            regionBase = Region.getBase();
        }
        return new Coordinate(
            regionBase.getX() + (getRegionX() >> 7),
            regionBase.getY() + (getRegionY() >> 7),
            regionBase.getPlane()
        );
    }

    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition(Coordinate regionBase) {
        if (regionBase == null) {
            regionBase = Region.getBase();
        }
        return new Coordinate.HighPrecision(
            (regionBase.getX() << 7) + getRegionX(),
            (regionBase.getY() << 7) + getRegionY(),
            regionBase.getPlane()
        );
    }

    private int getRegionX() {
        return (int) rmi().getRegionX();
    }

    private int getRegionY() {
        return (int) rmi().getRegionY();
    }

    @Override
    public boolean isValid() {
        return super.isValid()
            && !Projectiles.newQuery().ids(getSpotAnimationId()).on(getPosition()).target(getTarget()).results().isEmpty();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "Projectile(", ")")
            .add("id=" + getSpotAnimationId())
            .add("position=" + getPosition())
            .toString();
    }

    @Override
    public int getAnimationId() {
        SpotAnimationDefinition definition = getDefinition();
        return definition != null ? definition.getAnimationId() : -1;
    }
}
