package com.runemate.ui.control;

import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.binding.*;
import com.runemate.ui.control.setting.*;
import java.net.*;
import java.util.*;
import javafx.application.*;
import javafx.fxml.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import lombok.*;

@InternalAPI
public class ControlPanel extends VBox implements Initializable {

    private final DefaultUI parent;
    @Getter
    private final LootPane lootPane;

    @Getter
    @FXML
    private VBox contentContainer;

    @FXML
    private Text runtimeText;

    @FXML
    private Text statusText;

    public ControlPanel(final DefaultUI ui) {
        this.parent = ui;
        this.lootPane = new LootPane(ui);
        FXUtil.loadFxml(this, "/fxml/control_panel.fxml");
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        setFillWidth(true);
        setMaxWidth(Double.MAX_VALUE);

        runtimeText.textProperty().bind(new TimestampBinding(parent.runtimeProperty()));

        final var settings = parent.bot().getSettingsManager();
        if (settings.hasSettings()) {
            contentContainer.getChildren().add(new SettingsPane(parent, settings));
            setStatusText("Waiting for settings...");
        } else {
            setStatusText("Starting...");
        }

        contentContainer.getChildren().add(new SkillsPane(parent));
        contentContainer.getChildren().add(lootPane);
    }

    public void setStatusText(@NonNull String status) {
        Platform.runLater(() -> statusText.setText(status));
    }

}
