package com.runemate.ui.control.setting;

import com.runemate.ui.*;
import com.runemate.ui.setting.open.*;
import java.util.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;

public class SettingsPane extends TitledPane {

    public SettingsPane(@NonNull DefaultUI ui, @NonNull SettingsManager manager) {

        final var parent = new VBox();
        parent.setSpacing(12);
        FlowPane container = new FlowPane();
        container.setHgap(12);
        container.setVgap(12);
        container.setRowValignment(VPos.TOP);
        parent.setPadding(new Insets(12, 4, 4, 4));
        parent.getChildren().add(container);

        final var startButton = new Button("Start");
        startButton.managedProperty().bind(visibleProperty());
        startButton.visibleProperty().bind(manager.lockedProperty().not());
        startButton.setPrefHeight(24);
        startButton.setOnAction(e -> {
            manager.lock();
            startButton.setText("Update");
        });
        parent.getChildren().add(startButton);

        setText("Settings");
        setContent(parent);

        var settings = new Settings(ui.bot());
        settings.init();

        var sections = new ArrayList<SectionPane>();
        settings.getSettings().keySet().stream()
            .map(Settings.Setting::section)
            .distinct()
            .forEach(section -> sections.add(new SectionPane(settings, section, false)));
        sections.sort(Comparator.comparingInt(SectionPane::getOrder));
        container.getChildren().addAll(sections);

        final var firstVisible = (SectionPane) container.getChildren().stream().filter(Node::isVisible).findFirst().orElse(null);
        if (firstVisible != null) {
            startButton.prefWidthProperty().bind(firstVisible.widthProperty());
        } else {
            startButton.setPrefWidth(250);
        }
    }
}
