package com.runemate.ui.control.setting;

import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.annotation.open.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import com.google.common.collect.*;
import java.util.Objects;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.scene.control.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2(topic = "Settings")
public class Settings implements SettingsListener {

    private final AbstractBot bot;
    private final SettingsManager manager;

    public Settings(final AbstractBot bot) {
        this.bot = bot;
        this.manager = bot.getSettingsManager();
    }

    @Getter
    private final Map<Setting, Control> settings = new HashMap<>();

    public void init() {

        var groups = new ArrayList<>(manager.getSettingsDescriptors());
        Collections.reverse(groups);
        var sections = groups.stream().flatMap(group -> group.sections().stream()).collect(Collectors.toSet());
        for (var group : groups) {
            for (var setting : group.settings()) {
                var section = sections.stream()
                    .filter(s -> s.key().equals(setting.setting().section()))
                    .findFirst()
                    .orElse(null);

                var desc = new Setting(group, section, setting);

                var control = Controls.build(bot, manager, desc);
                if (control == null) {
                    continue;
                }

                if (!Strings.isNullOrEmpty(setting.setting().description())) {
                    control.setTooltip(new Tooltip(setting.setting().description()));
                }

                settings.put(desc, control);
            }
        }

        settings.keySet().forEach(setting -> {
            var previous = manager.get(setting.group(), setting.setting());
            if (previous != null) {
                setting.settingProperty().set(previous);
            }
        });
        bot.getEventDispatcher().addListener(this);
    }

    public Map<Setting, Control> bySection(SettingsSectionDescriptor section) {
        return Maps.filterKeys(settings, setting -> setting.belongsTo(section));
    }

    public static Comparator<Map.Entry<Setting, Control>> ordered() {
        return (a, b) -> ComparisonChain.start()
            .compare(a.getKey().settingOrder(), b.getKey().settingOrder())
            .compare(a.getKey().setting().title(), b.getKey().setting().title())
            .result();
    }

    @Override
    public void onSettingChanged(SettingChangedEvent event) {
        Platform.runLater(() -> {
            //Synchronize UI elements
            settings.keySet().stream()
                .filter(setting -> setting.accepts(event))
                .findFirst()
                .ifPresent(entry -> entry.settingProperty().set((String) event.getValue()));

            //Check dependencies
            settings.forEach((setting, control) -> control.setVisible(setting.shouldBeShown()));
        });
    }

    @Override
    public void onSettingsConfirmed() {
        //noop
    }

    @Getter
    @Accessors(fluent = true)
    public class Setting {

        private final SettingsDescriptor group;
        private final SettingsSectionDescriptor section;
        private final SettingDescriptor setting;
        private final StringProperty settingProperty;

        private final Collection<Dependency> dependencies;

        public Setting(SettingsDescriptor group, SettingsSectionDescriptor section, SettingDescriptor setting) {
            this.group = group;
            this.section = section;
            this.setting = setting;
            this.settingProperty = new SimpleStringProperty();

            dependencies = new ArrayList<>();
            for (var dependency : setting.dependencies()) {
                dependencies.add(new Dependency(dependency));
            }
        }

        public void setSettingValue(String value) {
            if (Strings.isNullOrEmpty(value)) {
                manager.remove(group, setting);
            } else {
                manager.set(group, setting, value);
            }
        }

        public int settingOrder() {
            return setting.order();
        }

        public boolean accepts(SettingChangedEvent event) {
            return Objects.equals(group.group().group(), event.getGroup()) && Objects.equals(setting.key(), event.getKey());
        }

        public boolean belongsTo(SettingsSectionDescriptor section) {
            if (this.section == null) {
                return section == null;
            }

            return Objects.equals(section, this.section);
        }

        public boolean shouldBeShown() {
            if (setting.hidden()) {
                return false;
            }

            //True if empty
            return dependencies.stream().allMatch(Dependency::validate);
        }

        private class Dependency {

            private final String group, key;
            private final Pattern value;

            public Dependency(DependsOn d) {
                group = d.group();
                key = d.key();
                if (Strings.isNullOrEmpty(d.value())) {
                    value = null;
                } else {
                    value = Pattern.compile(d.value());
                }
            }

            public boolean accepts(String string) {
                if (Strings.isNullOrEmpty(string)) {
                    return value == null;
                }

                return value.matcher(string).find();
            }

            public boolean validate() {
                return accepts(manager.get(group, key));
            }
        }
    }
}
