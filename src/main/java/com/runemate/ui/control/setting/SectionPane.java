package com.runemate.ui.control.setting;

import com.runemate.ui.setting.descriptor.open.*;
import java.util.stream.*;
import javafx.beans.binding.*;
import javafx.beans.property.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;

public class SectionPane extends TitledPane {

    @Getter
    private final int order;

    public SectionPane(Settings settings, SettingsSectionDescriptor section, boolean collapsible) {
        setMaxHeight(Integer.MAX_VALUE);
        setCollapsible(collapsible);
        setPadding(new Insets(4));
        getStyleClass().add("settings-section");
        managedProperty().bind(visibleProperty());

        if (section == null) { //"General" section
            setText("General");
            setExpanded(true);
            order = Integer.MIN_VALUE;
        } else { //Named section
            setText(section.title());
            setExpanded(!section.section().collapsed());
            order = section.order();
        }

        var grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setGridLinesVisible(false);
        grid.setPadding(new Insets(4, 4, 4, 24));
        grid.setVgap(8);
        grid.setHgap(8);

        var textConstraints = new ColumnConstraints();
        textConstraints.setHalignment(HPos.RIGHT);
        textConstraints.setFillWidth(true);

        var valueConstraints = new ColumnConstraints();
        valueConstraints.setHalignment(HPos.LEFT);
        valueConstraints.setFillWidth(true);

        grid.getColumnConstraints().addAll(textConstraints, valueConstraints);
        setContent(grid);

        var values = settings.bySection(section);
        var hide = BooleanBinding.booleanExpression(new SimpleBooleanProperty(true));

        var entries = values.entrySet().stream().sorted(Settings.ordered()).collect(Collectors.toList());
        for (int row = 0; row < entries.size(); row++) {
            var entry = entries.get(row);
            var setting = entry.getKey();
            var control = entry.getValue();

            var label = new Label(setting.setting().title());
            label.visibleProperty().bind(control.visibleProperty());
            label.managedProperty().bind(label.visibleProperty());

            hide = Bindings.and(hide, control.visibleProperty().not());

            grid.add(label, 0, row);
            grid.add(control, 1, row);
        }

        visibleProperty().bind(hide.not());
    }
}
