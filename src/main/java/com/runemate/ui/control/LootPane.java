package com.runemate.ui.control;

import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.tracker.*;
import java.util.*;
import javafx.animation.*;
import javafx.application.*;
import javafx.collections.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2
@InternalAPI
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class LootPane extends TitledPane {

    DefaultUI ui;
    VBox container;
    final InventoryTracker tracker;

    public LootPane(@NonNull final DefaultUI ui) {
        this.ui = ui;
        tracker = new InventoryTracker(ui.bot());
        tracker.getItems().addListener((MapChangeListener<Integer, InventoryTracker.ItemTracker>) change -> {
            if (change.wasAdded()) {
                addTracker(change.getValueAdded());
            }
        });

        var parentContainer = new VBox(12);
        parentContainer.setFillWidth(true);
        parentContainer.setPadding(new Insets(12));

        container = new VBox(12);
        container.setFillWidth(true);

        setText("Loot");
        setContent(parentContainer);
        setExpanded(false);

        ui.addRepeatTask(() -> Platform.runLater(() -> FXCollections.sort(
            container.getChildren(),
            Comparator.comparingInt(node -> ((ItemControl) node).totalValueProperty().intValue()).reversed()
        )));
        parentContainer.getChildren().add(new TotalLootControl(ui, tracker));
        parentContainer.getChildren().add(container);
    }

    public void setItemEventFilter(final ItemEventFilter filter) {
        tracker.setItemEventFilter(filter);
    }

    private void addTracker(@NonNull InventoryTracker.ItemTracker tracker) {
        synchronized (container) {
            Platform.runLater(() -> {
                log.info("Adding tracker for " + tracker.getName());
                container.getChildren().add(new ItemControl(ui, tracker));
            });
        }
    }
}
