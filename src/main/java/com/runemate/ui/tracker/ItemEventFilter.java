package com.runemate.ui.tracker;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.function.*;

@FunctionalInterface
public interface ItemEventFilter extends Predicate<ItemEvent> {
}
