package com.runemate.bot.test;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.region.*;
import java.awt.*;
import java.util.concurrent.*;
import javax.swing.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public class Overlay extends JComponent {

    private final JFrame frame;
    private final FeatureTestBot bot;
    private final Timer timer;

    public Overlay(final FeatureTestBot bot) {
        this.bot = bot;

        frame = new JFrame("Model viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);
        frame.add(this);
        frame.setUndecorated(true);
        frame.setBackground(new Color(0, 0, 0, 0));

        frame.pack();
        frame.setVisible(true);

        timer = new Timer(100, e -> {
            // Code to update the overlay every 50 ms goes here
            try {
                bot.getPlatform().invokeAndWait(() -> {
                    var loc = Screen.getLocation();
                    if (loc != null) {
                        frame.setLocation(loc);
                    }

                    var bounds = Screen.getBounds();
                    if (bounds != null) {
                        frame.setSize(bounds.getSize());
                    }
                });
            } catch (ExecutionException | InterruptedException ignored) {

            }
            repaint();
        });
        timer.start(); // Start the timer
    }

    @SneakyThrows
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Code to render the overlay goes here
        var graphics = (Graphics2D) g;
        graphics.setPaint(Color.RED);
        graphics.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        bot.getPlatform().invokeAndWait(() -> {

            final var player = Players.getLocal();
            if (player != null) {
                player.getServerPosition().render(graphics);
            }

            final var object = GameObjects.newQuery().types(GameObject.Type.PRIMARY).results().nearest();
            if (object != null) {
                graphics.setPaint(Color.BLUE);
                var clickbox = OpenHull.lookup(((Entity) object).uid);
                if (clickbox != null) {
                    graphics.draw(clickbox);
                }

                var model = object.getModel();
                if (model != null) {
                    graphics.setPaint(Color.RED);
                    model.render(graphics);
                }
            }

            final var mouse = Mouse.getPosition();
            graphics.drawRect((int) mouse.getX(), (int) mouse.getY(), 2, 2);
        });
    }


}
