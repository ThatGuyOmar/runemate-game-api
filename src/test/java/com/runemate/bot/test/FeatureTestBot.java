package com.runemate.bot.test;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.runemate.ui.setting.annotation.open.*;
import java.util.regex.*;
import lombok.extern.log4j.*;

/**
 * This bot can be used to test new features that you're working on.Recommend that you use this to make it obvious to the reviewer
 * how exactly the code change was tested.
 * <p>
 * The "testJar" gradle task will produce a jar containing this bot which you can place in a bot directory.
 * <p>
 * The following gradle command will build this bot and then launch the client:
 * testJar launch --args="--dev"
 */
@Log4j2
public class FeatureTestBot extends LoopingBot implements SettingsListener {

    @SettingsProvider(updatable = true)
    private ExampleSettings settings;

    public static final Pattern WIELD = Pattern.compile("^(?:Wield|Wear|Equip)$");
    private static final Coordinate GRAND_EXCHANGE = new Coordinate(3165, 3476, 0);

    private Path path;

    @Override
    public void onStart(final String... arguments) {
        getEventDispatcher().addListener(this);
        GameEvents.Universal.LOGIN_HANDLER.disable(this);
        setLoopDelay(3000);
    }

    @Override
    public void onLoop() {

        log.info(Quest.OSRS.SECRETS_OF_THE_NORTH.getStatus());
        log.info(Quest.OSRS.DESERT_TREASURE_II.getStatus());

        log.info("---");
    }

    private void withdraw(String name, int quantity) {
        var item = Bank.getItems(name).first();
        if (item != null) {
            log.info("Withdrawing {} x{}", name, quantity);
            var action = MenuAction.forBankWithdrawal(item, quantity);
            if (action != null) {
                log.info(action);
                DirectInput.send(action);
            }
        }
    }

    public boolean wear(SpriteItem item) {
        var ma = MenuAction.forSpriteItem(item, WIELD);
        if (ma == null) {
            return false;
        }

        DirectInput.send(ma);
        return true;
    }

    public boolean wear(EquipmentLoadout loadout) {
        var mismatches = loadout.getMissingSlots();
        if (mismatches.isEmpty()) {
            return true;
        }

        var switched = 0;

        final var items = Inventory.getItems();
        for (var slot : mismatches) {
            final var pattern = loadout.get(slot);
            if (pattern == null) {
                continue;
            }

            final var item = Inventory.newQuery().provider(items::asList).names(pattern).results().first();
            if (item == null) {
                return false;
            }

            if (wear(item)) {
                switched++;
            } else {
                return false;
            }
        }

        log.debug("Equipped {} items", switched);

        return true;
    }

    @Override
    public void onSettingChanged(final SettingChangedEvent e) {
//        log.info("{}.{} -> {}", e.getGroup(), e.getKey(), e.getValue());
    }

    @Override
    public void onSettingsConfirmed() {

    }
}
